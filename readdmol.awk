#*******************************************************************************
#                                                                              *
#                                   Viewmol                                    *
#                                                                              *
#                           R E A D D M O L . A W K                            *
#                                                                              *
#                 Copyright (c) Joerg-R. Hill, October 2003                    *
#                                                                              *
#*******************************************************************************
#
# $Id: readdmol.awk,v 1.1 2003/11/07 12:56:18 jrh Exp $
# $Log: readdmol.awk,v $
# Revision 1.1  2003/11/07 12:56:18  jrh
# Initial revision
#
#
function vectorlength(x, y, z)
{
  return(sqrt(x*x+y*y+z*z));
}
function angle(x1, y1, z1, x2, y2, z2)
{
  x=(x1*x2+y1*y2+z1*z2)/(vectorlength(x1, y1, z1)*vectorlength(x2, y2, z2));
  y=sqrt(1-x*x);
  return(57.29577951308232087721*atan2(y, x));
}
function printCell(label, cell)
{
  if (useVectors == "n")
  {
    a=vectorlength(cell[1,1], cell[1,2], cell[1,3]);
    b=vectorlength(cell[2,1], cell[2,2], cell[2,3]);
    c=vectorlength(cell[3,1], cell[3,2], cell[3,3]);
    alpha=angle(cell[2,1], cell[2,2], cell[2,3], cell[3,1], cell[3,2], cell[3,3]);
    beta=angle(cell[1,1], cell[1,2], cell[1,3], cell[3,1], cell[3,2], cell[3,3]);
    gamma=angle(cell[1,1], cell[1,2], cell[1,3], cell[2,1], cell[2,2], cell[2,3]);
    printf("%s %f %f %f %f %f %f\n", label, a, b, c, alpha, beta, gamma);
  }
  else
  {
    printf("%s vectors\n", label);
    printf("%22.14f%22.14f%22.14f\n", cell[1,1], cell[1,2], cell[1,3]);
    printf("%22.14f%22.14f%22.14f\n", cell[2,1], cell[2,2], cell[2,3]);
    printf("%22.14f%22.14f%22.14f\n", cell[3,1], cell[3,2], cell[3,3]);
  }
}
BEGIN {cycle=0;
       fixed=0;
       readfrq=0;
       readmodes=0;
       readorbital=0;
       readgrid=0;
       readStartCoord=0;
       readEnergy=0;
       nfreq=0;
       ngridfiles=0;
       unitcell=0;
       cellvectors=0;
       transx=0.0;
       transy=0.0;
       transz=0.0;
       title=1;
       version=3;
       opt=0;
       cosmo=0;
      }
/^  DDDDDDDD    MM       MM    OOOOOO    LL/ {version=2;}
/^# Title/ {if (title == 1)
	    {
	      str=substr($0, 11, length($0)-10);
	      sub("<--", "", str);
	      printf("$title\n%s\n", str);
	      title=0;
	    }
	   }
/^Symmetry group of the molecule/ {printf("$symmetry %s\n", $6);}
/^translated by/ {transx=$3;
		  transy=$4;
		  transz=$5;
		 }
/Unit Cell Lattice Vectors/ {cellvectors=1;}
/^a/ {if (cellvectors == 1)
      {
        cell[1,1]=$2;
        cell[1,2]=$3;
	cell[1,3]=$4;
      }
     }
/^b/ {if (cellvectors == 1)
      {
        cell[2,1]=$2;
        cell[2,2]=$3;
	cell[2,3]=$4;
      }
     }
/^c/ {if (cellvectors == 1)
      {
        cell[3,1]=$2;
        cell[3,2]=$3;
	cell[3,3]=$4;
	cellvectors=0;
	unitcell=1;
	useVectors="y";
      }
     }
/^  a1/ {cell[1,1]=$2;
	 cell[1,2]=$3;
	 cell[1,3]=$4;
	}
/^ translation vector \[a0\]    1/ {cell[1,1]=$5;
				    cell[1,2]=$6;
				    cell[1,3]=$7;
				   }
/^  a2/ {cell[2,1]=$2;
	 cell[2,2]=$3;
	 cell[2,3]=$4;
	}
/^ translation vector \[a0\]    2/ {cell[2,1]=$5;
				    cell[2,2]=$6;
				    cell[2,3]=$7;
				   }
/^  a3/ {cell[3,1]=$2;
	 cell[3,2]=$3;
	 cell[3,3]=$4;
	 unitcell=1;
	}
/^ translation vector \[a0\]    3/ {cell[3,1]=$5;
				    cell[3,2]=$6;
				    cell[3,3]=$7;
	 unitcell=1;
	}
/COSMO input/ {cosmo=1;}
/Atoms with "Fixed" Coordinates/ {fixed=1;}
/Entering Optimization Section/ {opt=1;}
/Entering MD\/SimAnn Stage Number/ {opt=1;}
/Entering Vibrations Section/ {opt=0;}
/^\$coordinates/ {readStartCoord=1;
		  i=0;}
/^\$end/ {readStartCoord=0;}
/^df   [ A-Z][A-Za-z]/ {if ($2 != "self")
                        {
			  coord[i,1]=$3+transx;
		          coord[i,2]=$4+transy;
		          coord[i,3]=$5+transz;
		          coord[i,4]=$2;
		          grad[i,1]=$6;
		          grad[i,2]=$7;
		          grad[i++,3]=$8;
		          if (index(fix[i], "x") == 0)
		            gnorm+=$6*$6;
		          if (index(fix[i], "y") == 0)
		            gnorm+=$7*$7;
		          if (index(fix[i], "z") == 0)
		            gnorm+=$8*$8;
		          readEnergy=1;
			}
		       }
/^[A-Z][ a-z] / {if (readStartCoord == 1)
		 {
		   coord[i,1]=$2;
		   coord[i,2]=$3;
		   coord[i,3]=$4;
		   coord[i++,4]=$1;
		 }
		}
/^df              ATOMIC  COORDINATES/ {opt=1;
					cycle++;
					i=0;
					gnorm=0.0;
					readorbital=0;
				       }
/^df   self consistent total energy/ {energy=$6;
				      sub("Ha", "", energy);
				     }
/^df total energy/    {if (opt == 1)
		       {
			 energy=$4;
			 if (cycle == 1) printf("$grad 0.52917706\n");
			 printf("  cycle =    %d  energy =%18.10f |dE/dxyz| =%10.6f\n", cycle, energy, sqrt(gnorm));
			 if (unitcell == 1) printCell("  unitcell", cell);
			 for (j=0; j<i; j++)
			 {
			   printf("%22.14f%22.14f%22.14f  %s\n", coord[j,1], coord[j,2], coord[j,3], coord[j,4]);
			 }
			 for (j=0; j<i; j++)
			 {
			   printf("%22.14f%22.14f%22.14f\n", grad[j,1], grad[j,2], grad[j,3]);
			 }
		       }
		      }
/^df  binding energy/ {if (opt == 1 && cosmo == 0)
		       {
			readEnergy=0;
			energy=$4;
			if (cycle == 1) printf("$grad 0.52917706\n");
			printf("  cycle =    %d  energy =%18.10f |dE/dxyz| =%10.6f\n", cycle, energy, sqrt(gnorm));
			if (unitcell == 1) printCell("  unitcell", cell);
			for (j=0; j<i; j++)
			{
			  printf("%22.14f%22.14f%22.14f  %s\n", coord[j,1], coord[j,2], coord[j,3], coord[j,4]);
			}
			for (j=0; j<i; j++)
			{
			  printf("%22.14f%22.14f%22.14f\n", grad[j,1], grad[j,2], grad[j,3]);
			}
		      }
		     }
/\+ Non-Electrostatic Energy/ {cosmo++;
			       if (cosmo == 3)
			       {
				 cosmo=1;
				 energy=$5;
				 if (cycle == 0 || cycle == 1) printf("$grad 0.52917706\n");
				 printf("  cycle =    %d  energy =%18.10f |dE/dxyz| =%10.6f\n", cycle, energy, sqrt(gnorm));
				 if (unitcell == 1) printCell("  unitcell", cell);
				 for (j=0; j<i; j++)
				 {
				   printf("%22.14f%22.14f%22.14f  %s\n", coord[j,1], coord[j,2], coord[j,3], coord[j,4]);
				 }
				 for (j=0; j<i; j++)
				 {
				   printf("%22.14f%22.14f%22.14f\n", grad[j,1], grad[j,2], grad[j,3]);
				 }
			       }
			      }
/^df \*\*\*\*\* magnitude of the gradient:/ {if (opt == 1)
					     {
					       gnorm=$7;
					       if (cycle == 1) printf("$grad 0.52917706\n");
					       printf("  cycle =    %d  energy =%18.10f |dE/dxyz| =%10.6f\n", cycle, energy, gnorm);
					       if (unitcell == 1) printCell("  unitcell", cell);
					       for (j=0; j<i; j++)
					       {
						 printf("%22.14f%22.14f%22.14f  %s\n", coord[j,1], coord[j,2], coord[j,3], coord[j,4]);
					       }
					       for (j=0; j<i; j++)
					       {
						 printf("%22.14f%22.14f%22.14f\n", grad[j,1], grad[j,2], grad[j,3]);
					       }
					     }
					    }
/^     vibrational frequencies, intensities/ {readfrq=1;
					      printf("$vibrational spectrum\n");
					     }
/^  Frequencies \(cm-1\) and normal modes/ {readmodes=1;
					    readfrq=0;
					    total=0;
					    mode=0;
					   }
/^  Eigenvalues and occupations:/ {readorbital=1;
				   norbital=0;}
/^    state/                      {readorbital=1;
				   norbital=0;}
/^   Searching for/               {readorbital=0;}
/^ Molecular orbital spectrum/    {readorbital=0;}
/^  Beta orbitals, symmetry block/ {readorbital=0;}
/^  Orbital occupation is/        {readorbital=0;}
/^Ef/                             {readorbital=0;
				     if (readEnergy == 1)
					       {
				     if (opt == 1)
				     {
				       energy=$2;
				       if (cycle == 1) printf("$grad 0.52917706\n");
				       printf("  cycle =    %d  energy =%18.10f |dE/dxyz| =%10.6f\n", cycle, energy, sqrt(gnorm));
				       if (unitcell == 1) printCell("  unitcell", cell);
				       for (j=0; j<i; j++)
				       {
					 printf("%22.14f%22.14f%22.14f  %s\n", coord[j,1], coord[j,2], coord[j,3], coord[j,4]);
				       }
				       for (j=0; j<i; j++)
				       {
					 printf("%22.14f%22.14f%22.14f\n", grad[j,1], grad[j,2], grad[j,3]);
				       }
				     }
						   readEnergy=0;
					       }
				    }
/^  orbital/                      {if (system("ls " dir "/" $NF " > /dev/null 2> /dev/null") == 0)
				   {
				     gridfile[ngridfiles++]=dir "/" $NF;
				   }
				   else
				   {
				     printf("$error noFile 0 %s\n", dir "/" $NF);
				   }
				  }
/^ grid specifications: I_dim/    {readgrid=1;}
/Distance:/                       {fix[$2-1]="xyz";
				   fix[$3-1]="xyz";
				  }
/Hessian modes/ {fixed=0;}
/^ *[0-9]/ {if (readfrq == 1)
	    {
	      printf("A1 %10.1f %10.5f 1.0\n", $3, $4/979.5936733887993);
	      nfreq++;
	    }
	    if (fixed == 1)
	    {
	      s=$2 $3 $4;
	      fix[$1-1]=tolower(s);
	    }
	    if (readgrid == 1)
	    {
	      if ($1 == "3")
	      {
		gridx[0]=$2;
		gridy[0]=$3;
		gridz[0]=$4;
		gridn[0]=$5;
		gridx[3]=$6;
		gridy[3]=$7;
		gridz[3]=$8;
	      }
	      else
	      {
		gridn[2]=$1;
		gridx[2]=$2;
		gridy[2]=$3;
		gridz[2]=$4;
		gridn[1]=$5;
		gridx[1]=$6;
		gridy[1]=$7;
		gridz[1]=$8;
		readgrid=0;
	      }
	    }
	   }
/^ *[-.0-9]*[DE][+-][0-9][0-9]/ {if (version == 2 && readorbital == 1)
				{
				  for (j=1; j<=NF; j++)
				  {
				    norbital++;
				    orbital[norbital,1]=symmetry;
				    orbital[norbital,2]=$j;
				    sub("D", "E", orbital[norbital,2]);
				  }
				}
			       }
/[0-9][0-9]*[ ]*+[ ]*[0-9][0-9]*/ {if (version > 2 && readorbital == 1)
				   {
				     norbital++;
				     orbital[norbital,1]=$4;
				     orbital[norbital,2]=$5;
				   }
				  }
/^ [A-z][ A-z]* [xyz]/ {if (readmodes == 1)
			{
			  for (j=3; j<=NF; j++)
			  {
			    nm[mode+j-3,total]=$j;
			  }
			  total++;
			}
		       }
/^ *[xyz]/ {if (readmodes == 1)
	    {
	      for (j=2; j<=NF; j++)
	      {
		nm[mode+j-2,total]=$j;
	      }
	      total++;
	      if (total == 3*i)
	      {
		mode+=NF-1;
		total=0;
	      }
	    }
	   }
/^  \*\*\*\*\*\*\*\*/ {readfrq=0;}
/^ Zero point vibrational energy/ {readmodes=0;
				   printf("$vibrational normal modes\n");
				   for (j=0; j<3*i; j++)
				   {
				     for (k=0; k<nfreq; k++)
				     {
				       if (k % 5 == 0)
				       {
					 if (k != 0) printf("\n");
					 printf("%d %d", j, j);
				       }
				       printf(" %10.6f", nm[k,j]);
				     }
				     printf("\n");
				   }
				  }
/^  Alpha orbitals,/ {j=index($6, ".");
		    if (j != 0) symmetry=substr($6, 1, j-1);
		    else symmetry=$6;
		   }
END {printf("$coord 0.52917706\n");
     for (j=0; j<i; j++)
     {
       printf("%22.14f%22.14f%22.14f  %s %s\n", coord[j,1], coord[j,2], coord[j,3], coord[j,4], fix[j]);
     }
     if (unitcell == 1) printCell("$unitcell", cell);
     if (norbital > 0)
     {
       printf("$scfmo\n");
       for (j=1; j<=norbital; j++)
       {
	 printf("%d %s  eigenvalue= %20.15e  nsaos=0\n", j, orbital[j,1], orbital[j,2]);
       }
     }
     if (ngridfiles > 0)
     {
       for (j=0; j<ngridfiles; j++)
       {
	 printf("$grid #%d\n", j+1);
	 printf("  origin  0.0 0.0 0.0\n");
	 for (k=1; k<=3; k++)
	 {
	   x=gridx[k]-gridx[0];
	   y=gridy[k]-gridy[0];
	   z=gridz[k]-gridz[0];
	   l[k]=sqrt(x*x+y*y+z*z);
	   printf("  vector%d %f %f %f\n", k, x/l[k], y/l[k], z/l[k]);
	 }
	 printf(" grid1 start %f delta %f points %d\n", gridx[0], l[1]/gridn[0], gridn[0]+1);
	 printf(" grid2 start %f delta %f points %d\n", gridy[0], l[2]/gridn[1], gridn[1]+1);
	 printf(" grid3 start %f delta %f points %d\n", gridz[0], l[3]/gridn[2], gridn[2]+1);
	 printf(" type mo \n");
	 getline < gridfile[j];
	 str=$0;
	 sub("^[ ]*", "", str);
	 sub("[ ]*$", "", str);
	 printf(" title for this grid\n%s\n", str);
	 getline < gridfile[j];
	 getline < gridfile[j];
	 getline < gridfile[j];
	 getline < gridfile[j];
	 printf(" plotdata\n");
	 k=1;
	 while (getline < gridfile[j])
	 {
	   printf("%16s", $0);
	   if (k % 5 == 0)
	   {
	     printf("\n");
	   }
	   k++;
	 }
	 if (k % 5 != 0)
	 {
	   printf("\n");
	 }
       }
     }
     printf("$end\n");}
