#!/usr/bin/env python
#*******************************************************************************
#                                                                              *
#                                   Viewmol                                    *
#                                                                              *
#                          W R I T E G A U S S . P Y                           *
#                                                                              *
#                     Copyright (c) Stephan Schenk, 2002                       *
#                                                                              *
#*******************************************************************************
#
# $Id: writegauss.py,v 1.1 2003/11/07 13:01:01 jrh Exp $
# $Log: writegauss.py,v $
# Revision 1.1  2003/11/07 13:01:01  jrh
# Initial revision
#
#
############################################################################
#
#                           W R I T E G A U S S
#
############################################################################
#
# Writegauss is a script to generate an input file for Gaussian98
# Copyright � 2002 by Stephan Schenk (stephan.schenk@uni-jena.de). All
# rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
# History
#
# 2003/01/31	Removed syntax error in class Application
#		update_solventsolvent
#
# 2003/01/29	Removed syntax error in class Application
#		self.checkbuttonNoRaman.grid()
#
# 2003/01/13	Removed $RunGauss
#		Remove SCF=(Direct,Tight) since this is the default from G98.A11
#		onwards
#
# 2002/07/31	Added support for Opt=NoEigenTest,GDIIS,Big
#
# 2002/06/12	Added support for Freq=Hindered and Freq=ReadFC
#		Added checkpoint filename guess ability
#
############################################################################
#
# import necessary modules
#
from string import *
from Tkinter import *
import sys
import tkSimpleDialog

#
# check for filename given as command line parameter
#
filename = ""
coordinates = []

TESTING = 0
#TESTING = 1
if not TESTING :
	if len( sys.argv) > 1 :
		filename = sys.argv[1]

	if not len( filename) :
		sys.exit( -1)

	#
	# filename was given, now read coordinates from stdin
	#
	lines = sys.stdin.readlines()

	#
	# bring line into appropriate syntax
	#
	dowrite = 0
	for line in lines :
		start = 0
		if find( line, "coord") != -1 :
			dowrite = 1
			start = 1
		if find( line, "bonds") != -1 or find( line, "end") != -1 :
			dowrite = 0;

		if dowrite == 1 and start == 0:
			tokens = split( line)
			# this is something tricky because we get coordinates in atomic units(bohr)
			# but need angstroms
			faktor = 0.529177249
			s = tokens[3] + " "
			for i in range(0,3) :
				s = s + str( float( tokens[i]) * faktor) + " "
			coordinates.append( s)

#
# definition of various variables defaults
#
variableMethod = "B3LYP"
variableBasisset = "Gen"
variableCharge = "0"
variableMultiplicity = "1"
variableCheckpointfile = ""
variableMemory = ""
variableProcessors = ""
variableVerbose = 0
variableViewmol = 0
variableTitle = ""
variableOptimization = 1
variableTS = 0
variableNoEigenTest = 0
variableCalcFC = 0
variableReadFC = 0
variableOptAlgorithm = "GDIIS"
variablePopulation = "None"
variableSolventmethod="None"
variableSolventsolvent="Acetonitrile"
variableFrequency = 0
variableNoRaman = 0
variableHindered = 0
variableFreqReadFC = 0
variableNMR = 0
variableGFInput = 0
variableGuess = 0
variableGeometry = 0
variablePseudo = 0
variableGuessCheck = 1

#
# definition of method dialog
#
class MethodDialog( tkSimpleDialog.Dialog) :

	def __init__( self, parent, title, method) :
		self.method = method
		tkSimpleDialog.Dialog.__init__( self, parent, title)

	def body( self, master) :
		self.var = StringVar()
		self.radioMethods = []
		self.METHODS = ( "B3LYP", "B3PW91", "BP86", "BPW91", "BLYP", "SVWN",
		                 "HF", "MP2", "MP4", "CCSD(T)")

		for i in range( len( self.METHODS)) :
			self.radioMethods.append( Radiobutton( master, text=self.METHODS[i], \
												value=self.METHODS[i], variable=self.var))
			self.radioMethods[i].grid( row=i, column=0, sticky=W)
			if self.METHODS[i] == self.method :
				self.radioMethods[i].select()

	def apply( self) :
		self.method = self.var.get()
		self.result = 1


#
# definition of solvatation model dialog
#
class SolventMethodDialog( tkSimpleDialog.Dialog) :

	def __init__( self, parent, title, method) :
		self.method = method
		tkSimpleDialog.Dialog.__init__( self, parent, title)

	def body( self, master) :
		self.var = StringVar()
		self.radioMethods = []
		self.labelMethods = []
		self.METHODS = ( ("None", ""),
		                 ("PCM", "(Tomasi)"),
							  ("CPCM", "(Cosmo)"),
							  ("IEFPCM", ""),
							  ("IPCM", "(Isodensity)"),
							  ("SCIPCM", "")
							  )

		for i in range( len( self.METHODS)) :
			self.radioMethods.append( Radiobutton( master, text=self.METHODS[i][0],
			                          value=self.METHODS[i][0], variable=self.var))
			self.radioMethods[i].grid( row=i, column=0, sticky=W)
			self.labelMethods.append( Label( master, text=self.METHODS[i][1]))
			self.labelMethods[i].grid( row=i, column=1, sticky=W)
			if self.METHODS[i][0] == self.method :
				self.radioMethods[i].select()

	def apply( self) :
		self.method = self.var.get()
		self.result = 1


#
# definition of solvent dialog
#
class SolventDialog( tkSimpleDialog.Dialog) :

	def __init__( self, parent, title, solvent) :
		self.solvent = solvent
		tkSimpleDialog.Dialog.__init__( self, parent, title)

	def body( self, master) :
		self.var = StringVar()
		self.radioSolvents = []
		self.labelEpsilons = []
		self.SOLVENTS = ( ("Acetone", "20.7"),
		                  ("Acetonitrile", "36.64"),
								("Aniline", "6.89"),
								("Benzene", "2.247"),
								("Carbontetrachloride", "2.228"),
								("Chlorobenzene", "5.621"),
								("Chloroform", "4.9"),
								("Cyclohexane", "2.023"),
								("Dichloroethane", "10.36"),
								("Dichloromethane", "8.93"),
								("Diethylether", "4.335"),
								("Dimethylsulfoxide", "46.7"),
								("Ethanol", "24.55"),
								("Heptane", "1.92"),
								("Methanol", "32.63"),
								("Nitromethane", "38.2"),
								("THF", "7.58"),
								("Toluene", "2.379"),
								("Water", "78.39"),
							  )

		userow = 0

		self.labelColumn1 = Label( master, text="Solvent")
		self.labelColumn1.grid( row=userow, column=0)
		self.labelColumn2 = Label( master, text="Epsilon")
		self.labelColumn2.grid( row=userow, column=1, padx=15)

		userow = userow + 1

		self.frame = Frame( master, height=15)
		self.frame.grid( row=userow, column=0)

		userow = userow + 1

		for i in range( len(self.SOLVENTS)) :
			self.radioSolvents.append( Radiobutton( master, text=self.SOLVENTS[i][0],
			                          value=self.SOLVENTS[i][0], variable=self.var))
			self.radioSolvents[i].grid( row=userow, column=0, sticky=W)
			self.labelEpsilons.append( Label( master, text=self.SOLVENTS[i][1],
			anchor="w"))
			self.labelEpsilons[i].grid( row=userow, column=1, sticky=W, padx=15)
			if self.SOLVENTS[i][0] == self.solvent :
				self.radioSolvents[i].select()
			userow = userow + 1

	def apply( self) :
		self.solvent = self.var.get()
		self.result = 1


#
# definition of basisset dialog
#
class BasissetDialog( tkSimpleDialog.Dialog) :

	def __init__( self, parent, title, basisset) :
		self.basisset = basisset
		tkSimpleDialog.Dialog.__init__( self, parent, title)

	def body( self, master) :
		self.SETS = ( "STO-3G", "3-21G", "6-31G", "6-311G", "LanL2DZ",
		              "Gen","ChkBas")
		self.SETS_WITH_MODIFIERS = ( "3-21G", "6-31G", "6-311G")
		self.DIFFUSE = ( "None", "+", "++")
		self.HEAVYPOLAR = ( "None", "d", "2d", "3d", "df", "2df","3df")
		self.HYDROGENPOLAR = ( "None", "p", "2p", "3p", "pd", "2pd", "3pd")

		self.frame0 = Frame( master, height=5)
		self.frame0.grid( row=1, column=0)
		self.frame1 = Frame( master, height=10)
		self.frame1.grid( row=90, column=0)
		self.frame2 = Frame( master, height=5)
		self.frame2.grid( row=96, column=0)

		self.labelSet = Label( master, text="Basisset", width=20)
		self.labelSet.grid( row=0, column=0, sticky=W)
		self.varSet = StringVar()
		self.radioSets = []
		for i in range( len( self.SETS)) :
			self.radioSets.append( Radiobutton( master, text=self.SETS[i], \
												value=self.SETS[i], variable=self.varSet))
			self.radioSets[i].grid( row=i+2, column=0, sticky=W, padx=25)

		self.labelDiffuse = Label( master, text="Diffuse", width=20)
		self.labelDiffuse.grid( row=0, column=1, sticky=W)
		self.varDiffuse = StringVar()
		self.radioDiffuse = []
		for i in range( len( self.DIFFUSE)) :
			self.radioDiffuse.append( Radiobutton( master, text=self.DIFFUSE[i], \
												value=self.DIFFUSE[i], variable=self.varDiffuse))
			self.radioDiffuse[i].grid( row=i+2, column=1, sticky=W, padx=40)

		self.labelHeavypolar = Label( master, text="Heavy atom polar", width=20)
		self.labelHeavypolar.grid( row=95, column=0, sticky=W)
		self.varHeavypolar = StringVar()
		self.radioHeavypolar = []
		for i in range( len( self.HEAVYPOLAR)) :
			self.radioHeavypolar.append( Radiobutton( master, text=self.HEAVYPOLAR[i], \
									value = self.HEAVYPOLAR[i], variable=self.varHeavypolar))
			self.radioHeavypolar[i].grid( row=100+i, column=0, sticky=W, padx=25)

		self.labelHydrogenpolar = Label( master, text="Hydrogen polar", width=20)
		self.labelHydrogenpolar.grid( row=95, column=1, sticky=W)
		self.varHydrogenpolar = StringVar()
		self.radioHydrogenpolar = []
		for i in range( len( self.HYDROGENPOLAR)) :
			self.radioHydrogenpolar.append( Radiobutton( master, text=self.HYDROGENPOLAR[i], \
									value = self.HYDROGENPOLAR[i], variable=self.varHydrogenpolar))
			self.radioHydrogenpolar[i].grid( row=100+i, column=1, sticky=W, padx=40)

		self.radioSets[3].select()
		self.radioDiffuse[2].select()
		self.radioHeavypolar[1].select()
		self.radioHydrogenpolar[1].select()

	def apply( self) :
		extension = 0
		for i in  self.SETS_WITH_MODIFIERS :
			if self.varSet.get() == i :
				extension = 1

#WARNING: currently only Pople basis sets are handled correctly here
		if extension == 0 :
			self.basisset = self.varSet.get()
		else :
			if self.varDiffuse.get() == "None" :
				self.basisset = self.varSet.get()
			else :
				s = self.varSet.get()
				self.basisset = s[:len(s)-1]
				self.basisset = self.basisset + self.varDiffuse.get() + "G"
			if self.varHeavypolar.get() != "None" :
				self.basisset = self.basisset + "(" + self.varHeavypolar.get()
				if self.varHydrogenpolar.get() != "None" :
					self.basisset = self.basisset + "," + self.varHydrogenpolar.get()
				self.basisset = self.basisset + ")"

		self.result = 1


#
# definition of final dialog
#
class FinalDialog( tkSimpleDialog.Dialog) :
	def __init__( self, parent, title, header) :
		self.header = header
		tkSimpleDialog.Dialog.__init__( self, parent, title)

	def body( self, master) :
		self.label = Label( master, text="Final output will be:")
		self.label.grid( row=0, column=0, columnspan=20, sticky=W)
		self.text = Text( master)
		self.text.insert( END, join( self.header, "\n"))
		self.text.grid( row=1, column=0)
		return self.text

	def apply( self) :
		self.result = 1
		start = self.text.index( "1.0")
		end = self.text.index( END)
		self.textfield = self.text.get( start, end )


#
# definition of main window
#

class Application :

	def __init__( self, master) :

		self.mainframe = Frame( master)
		self.mainframe.pack()

		userow = 0

		self.labelMethod = Label( self.mainframe, text="Method:")
		self.labelMethod.grid( row=userow, column=0, sticky=W)
		self.varMethod = StringVar()
		self.varMethod.set( variableMethod)
		self.entryMethod = Entry( self.mainframe)
		self.entryMethod.grid( row=userow, column=1, sticky=W)
		self.entryMethod.configure( textvariable=self.varMethod)
		self.buttonMethod = Button( self.mainframe, text="Select", command=self.select_method)
		self.buttonMethod.grid( row=userow, column=2)

		self.frame0 = Frame( self.mainframe, width=30)
		self.frame0.grid( row=userow, column=3)
		self.labelCharge = Label( self.mainframe, text="Charge:", width=10, anchor="w")
		self.labelCharge.grid( row=userow, column=4, sticky=W)
		self.varCharge = StringVar()
		self.varCharge.set( variableCharge)
		self.entryCharge = Entry( self.mainframe, width=4)
		self.entryCharge.grid( row=userow, column=5, sticky=W)
		self.entryCharge.configure( textvariable=self.varCharge)

		userow = userow + 1

		self.labelBasisset = Label( self.mainframe, text="Basisset:")
		self.labelBasisset.grid( row=userow, column=0, sticky=W)
		self.varBasisset = StringVar()
		self.varBasisset.set( variableBasisset)
		self.entryBasisset = Entry( self.mainframe)
		self.entryBasisset.grid( row=userow, column=1, sticky=W)
		self.entryBasisset.configure( textvariable=self.varBasisset)
		self.buttonBasisset = Button( self.mainframe, text="Select", command=self.select_basisset)
		self.buttonBasisset.grid( row=userow, column=2)

		self.labelMultiplicity = Label( self.mainframe, text="Multiplicity:", width=10, anchor="w")
		self.labelMultiplicity.grid( row=userow, column=4, sticky=W)
		self.varMultiplicity = StringVar()
		self.varMultiplicity.set( variableMultiplicity)
		self.entryMultiplicity = Entry( self.mainframe, width=4)
		self.entryMultiplicity.grid( row=userow, column=5, sticky=W)
		self.entryMultiplicity.configure( textvariable=self.varMultiplicity)

		userow = userow + 1

		self.frame1 = Frame( self.mainframe, height=15)
		self.frame1.grid( row = userow, column=0)

		userow = userow + 1

		self.labelCheckpointfile = Label( self.mainframe, text="Checkpointfile:",
		                                  width=12, anchor="w")
		self.labelCheckpointfile.grid( row=userow, column=0, sticky=W)

		self.varCheckpointfile = StringVar()
		self.varCheckpointfile.set( variableCheckpointfile)
		if variableGuessCheck == 1:
			check = filename
			#strip possible directory
			index = rfind( check, "/")
			if index >= 0 and index < len( check):
				check = check[index+1:]
			#strip possible extension
			index = rfind( check, ".")
			if index >= 0 :
				check = check[0:index]
			#append .chk and set dialog value
			check = check + ".chk"
			self.varCheckpointfile.set( check)
		self.entryCheckpointfile = Entry( self.mainframe, width=45)
		self.entryCheckpointfile.configure( textvariable=self.varCheckpointfile)
		self.entryCheckpointfile.grid( row=userow, column=1, columnspan=20, padx=15)

		userow = userow + 1

		self.frame2 = Frame( self.mainframe, height=10)
		self.frame2.grid( row = userow, column=0)

		userow = userow + 1

		self.labelMemory = Label( self.mainframe, text="Memory (MB):")
		self.labelMemory.grid( row=userow, column=0, sticky=W)
		self.varMemory = StringVar()
		self.varMemory.set( variableMemory)
		self.entryMemory = Entry( self.mainframe, width=5)
		self.entryMemory.configure( textvariable=self.varMemory)
		self.entryMemory.grid( row=userow, column=1, sticky=W, padx=20)

		self.labelProcessor = Label( self.mainframe, text="Processors:", width=12,
		                             anchor="e")
		self.labelProcessor.grid( row=userow, column=2, sticky=E, columnspan=2)
		self.varProcessors = StringVar()
		self.varProcessors.set( variableProcessors)
		self.entryProcessors = Entry( self.mainframe, width=3)
		self.entryProcessors.configure( textvariable=self.varProcessors)
		self.entryProcessors.grid( row=userow, column=4, sticky=W, padx=15)

		userow = userow + 1

		self.frame3 = Frame( self.mainframe, height=10)
		self.frame3.grid( row = userow, column=0)

		userow = userow + 1

		self.labelTitle = Label( self.mainframe, text="Title:")
		self.labelTitle.grid( row=userow, column=0, sticky=W)
		self.varTitle = StringVar()
		self.varTitle.set( variableTitle)
		self.entryTitle = Entry( self.mainframe, width=45)
		self.entryTitle.configure( textvariable=self.varTitle)
		self.entryTitle.grid( row=userow, column=1, columnspan=20)

		userow = userow + 1

		self.frame4 = Frame( self.mainframe, height=15)
		self.frame4.grid( row=userow, column=0)

		userow = userow + 1

		self.labelGeometry = Label( self.mainframe, text="Optimization:")
		self.labelGeometry.grid( row=userow, column=0, sticky=W)

		self.varOptimization = IntVar()
		self.varOptimization.set( variableOptimization)
		self.checkbuttonOptimization = Checkbutton( self.mainframe, text="Optimize",
		                                            variable=self.varOptimization)
		self.checkbuttonOptimization.grid( row=userow, column=1, sticky=W)

		self.varTS = IntVar()
		self.varTS.set( variableTS)
		self.checkbuttonTS = Checkbutton( self.mainframe, text="TS",
		                                  variable=self.varTS)
		self.checkbuttonTS.grid( row=userow , column=1, sticky=E)

		self.varCalcFC = IntVar()
		self.varCalcFC.set( variableCalcFC)
		self.checkbuttonCalcFC = Checkbutton( self.mainframe, text="CalcFC",
		                                      variable=self.varCalcFC)
		self.checkbuttonCalcFC.grid( row=userow, column=2, sticky=W, padx=15,
		                             columnspan=2)

		self.varReadFC = IntVar()
		self.varReadFC.set( variableReadFC)
		self.checkbuttonReadFC = Checkbutton( self.mainframe, text="ReadFC",
		                                      variable=self.varReadFC)
		self.checkbuttonReadFC.grid( row=userow, column=4, sticky=W,
		                             columnspan=2)

		userow = userow +1

		self.varNoEigenTest = IntVar()
		self.varNoEigenTest.set( variableNoEigenTest)
		self.checkbuttonNoEigenTest = Checkbutton( self.mainframe, text="NoEigenTest",
		                                           variable=self.varNoEigenTest)
		self.checkbuttonNoEigenTest.grid( row=userow, column=1, sticky=W)

		userow = userow +1

		self.frame4a = Frame( self.mainframe, height=5)
		self.frame4a.grid( row=userow, column=0)

		userow = userow +1

		self.labelOptAlgorithm = Label( self.mainframe, text="Opt. Algorithm:")
		self.labelOptAlgorithm.grid( row=userow, column=0, sticky=W)

		self.OPTALGORITHM = ( "Berny", "GDIIS", "Big")
		self.radioOptAlgorithm = []
		self.varOptAlgorithm = StringVar()
		self.varOptAlgorithm.set( variableOptAlgorithm)
		for i in range( len( self.OPTALGORITHM)) :
			self.radioOptAlgorithm.append( Radiobutton( self.mainframe, text=self.OPTALGORITHM[i],\
			                               value=self.OPTALGORITHM[i], variable=self.varOptAlgorithm))
			a =  0
			if i == 2 :
				a = 1
			self.radioOptAlgorithm[i].grid( row=userow, column=i+1+a, sticky=W)

		userow = userow + 1

		self.frame5 = Frame( self.mainframe, height=15)
		self.frame5.grid( row=userow, column=0)

		userow = userow + 1

		self.labelPopulation = Label( self.mainframe, text="Population analysis:")
		self.labelPopulation.grid( row=userow, column=0, sticky=W)

		self.POPULATION = ( "None", "NPA", "NBO")
		self.radioPopulation = []
		self.varPopulation = StringVar()
		self.varPopulation.set( variablePopulation)
		for i in range( len( self.POPULATION)) :
			self.radioPopulation.append( Radiobutton( self.mainframe, text=self.POPULATION[i],\
							value=self.POPULATION[i], variable=self.varPopulation))
			a = 0
			if i == 2 :
				a = 1
			self.radioPopulation[i].grid( row=userow, column=i+1+a, sticky=W)

		userow = userow + 1

		self.frame5a = Frame( self.mainframe, height=15)
		self.frame5a.grid( row = userow, column=0)

		userow = userow + 1

		self.labelSolventmethod = Label( self.mainframe, text="Solvatation model:")
		self.labelSolventmethod.grid( row=userow, column=0, sticky=W)
		self.varSolventmethod = StringVar()
		self.varSolventmethod.set( variableSolventmethod)
		self.entrySolventmethod = Entry( self.mainframe, width=35)
		self.entrySolventmethod.configure( textvariable=self.varSolventmethod)
		self.entrySolventmethod.grid( row=userow, column=1, sticky=W, columnspan=4)
		self.buttonSolventmethod = Button( self.mainframe, text="Select",
		                                   command=self.select_solventmethod)
		self.buttonSolventmethod.grid( row=userow, column=4, sticky=W)

		userow = userow + 1

		self.labelSolventsolvent = Label( self.mainframe, text="Solvent:")
		self.labelSolventsolvent.grid( row=userow, column=0, sticky=W)
		self.varSolventsolvent = StringVar()
		self.varSolventsolvent.set( variableSolventsolvent)
		self.entrySolventsolvent = Entry( self.mainframe, width=35)
		self.entrySolventsolvent.configure( textvariable=self.varSolventsolvent)
		self.entrySolventsolvent.grid( row=userow, column=1, sticky=W, columnspan=4)
		self.buttonSolventsolvent = Button( self.mainframe, text="Select",
		                                    command=self.select_solventsolvent)
		self.buttonSolventsolvent.grid( row=userow, column=4, sticky=W)


		userow = userow + 1

		self.frame6 = Frame( self.mainframe, height=15)
		self.frame6.grid( row = userow, column=0)

		userow = userow + 1

		self.labelFrequency = Label( self.mainframe, text="Frequencies:")
		self.labelFrequency.grid( row=userow, column=0, sticky=W)
		self.varFrequency = IntVar()
		self.varFrequency.set( variableFrequency)
		self.checkbuttonFrequency = Checkbutton( self.mainframe, text="Calculate",
		                                         variable=self.varFrequency)
		self.checkbuttonFrequency.grid( row=userow, column=1, sticky=W)

		self.varNoRaman = IntVar()
		self.varNoRaman.set( variableNoRaman)
		self.checkbuttonNoRaman = Checkbutton( self.mainframe, text="NoRaman",
		                                       variable=self.varNoRaman)
		self.checkbuttonNoRaman.grid( row=userow, column=2, columnspan=2, sticky=W)

		userow = userow + 1

		self.varHindered = IntVar()
		self.varHindered.set( variableHindered)
		self.checkbuttonHindered = Checkbutton( self.mainframe, text="Hindered",
		                                        variable=self.varHindered)
		self.checkbuttonHindered.grid( row=userow, column=1, sticky=W)

		self.varFreqReadFC = IntVar()
		self.varFreqReadFC.set( variableFreqReadFC)
		self.checkbuttonFreqReadFC = Checkbutton( self.mainframe, text="ReadFC",
		                                          variable=self.varFreqReadFC)
		self.checkbuttonFreqReadFC.grid( row=userow, column=2, columnspan=2,sticky=W)

		userow = userow + 1

		self.frame7 = Frame( self.mainframe, height=15)
		self.frame7.grid( row=userow, column=0)

		userow = userow + 1

		self.labelMisc = Label( self.mainframe, text="Miscellanous:")
		self.labelMisc.grid( row=userow, column=0, sticky=W)

		self.varNMR = IntVar()
		self.varNMR.set( variableNMR)
		self.checkbuttonNMR = Checkbutton( self.mainframe, text="NMR shifts",
		                                   variable=self.varNMR)
		self.checkbuttonNMR.grid( row=userow, column=1, sticky=W)

		self.varGFInput = IntVar()
		self.varGFInput.set( variableGFInput)
		self.update_basisset( self.varBasisset.get())
		self.checkbuttonGFInput = Checkbutton( self.mainframe, text="GFInput",
		                                       variable=self.varGFInput)
		self.checkbuttonGFInput.grid( row=userow, column=2, sticky=W)

		userow = userow + 1

		self.varVerbose = IntVar()
		self.varVerbose.set( variableVerbose)
		self.checkbuttonVerbose = Checkbutton( self.mainframe, text="Verbose output",
		                                       variable=self.varVerbose)
		self.checkbuttonVerbose.grid( row=userow, column=1, sticky=W)

		self.varViewmol = IntVar()
		self.varViewmol.set( variableViewmol)
		self.checkbuttonViewmol = Checkbutton( self.mainframe, text="MO coeff.",
		                                       variable=self.varViewmol)
		self.checkbuttonViewmol.grid( row=userow, column=2, columnspan=20, sticky=W)

		userow = userow + 1

		self.varGuess = IntVar()
		self.varGuess.set( variableGuess)
		self.checkbuttonGuess = Checkbutton( self.mainframe, text="Guess=Read",
		                                     variable=self.varGuess)
		self.checkbuttonGuess.grid( row=userow, column=1, sticky=W)

		self.varGeometry = IntVar()
		self.varGeometry.set( variableGeometry)
		self.checkbuttonGeometry = Checkbutton( self.mainframe, text="Geom=AllCheck",
		                                        variable=self.varGeometry)
		self.checkbuttonGeometry.grid( row=userow, column=2, sticky=W,
		                               columnspan=4)

		userow = userow + 1

		self.varPseudo = IntVar()
		self.varPseudo.set( variablePseudo)
		self.checkbuttonPseudo = Checkbutton( self.mainframe, text="Pseudo=Read",
		                                      variable=self.varPseudo)
		self.checkbuttonPseudo.grid( row=userow, column=1, sticky=W)

		userow = userow + 1

		self.frame9 = Frame( self.mainframe, height=25)
		self.frame9.grid( row=userow, column=0)

		userow = userow + 1

		self.button = Button( self.mainframe, text="Finish", fg="red", command=self.show_final_dialog)
		self.button.grid( row=userow, columnspan=100)

		userow = userow + 1

		self.frame10 = Frame( self.mainframe, height=10)
		self.frame10.grid( row=userow, column=0)

		userow = userow + 1 
		self.labelCopyright = Label( self.mainframe, text="Copyright � 2002 Stephan Schenk <stephan.schenk@uni-jena.de>", font=("Times", 8))
		self.labelCopyright.grid( row=userow, column=0, columnspan=4, sticky=W)



	def select_method( self) :
		d = MethodDialog( root, "Method", self.varMethod.get())
		if d.result == 1 :
			self.update_method( d.method)

	def update_method( self, text) :
		self.varMethod.set( text)
		variableMethod = text

	def select_basisset( self) :
		d = BasissetDialog( root, "Basisset", self.varBasisset.get())
		if d.result == 1 :
			self.update_basisset( d.basisset)

	def update_basisset( self, text) :
		self.varBasisset.set( text)
		variableBasisset = text
		if text == "Gen" or text == "ChkBas" :
			self.varGFInput.set( 1)

	def select_solventmethod( self) :
		d = SolventMethodDialog( root, "Solvatation Model",
		                         self.varSolventmethod.get())
		if d.result == 1:
			self.update_solventmethod( d.method)

	def update_solventmethod( self, text) :
		self.varSolventmethod.set( text)
		variableSolventmethod = text

	def select_solventsolvent( self) :
		d = SolventDialog( root, "Solvent", self.varSolventsolvent.get())
		if d.result == 1:
			self.update_solventsolvent( d.solvent)

	def update_solventsolvent( self, text) :
		self.varSolventsolvent.set( text)
		variableSolventsolvent = text

	def show_final_dialog( self) :

		header = []
#		header.append( "$RunGauss")
#%-section
		if self.varCheckpointfile.get() != "" :
			header.append( "%chk=" + self.varCheckpointfile.get())
		if self.varMemory.get() != "" :
			header.append( "%mem=" + self.varMemory.get() + "MB")
		if self.varProcessors.get() != "":
			header.append( "%nproc=" + self.varProcessors.get())

#route-section
		s = "#"
		if self.varVerbose.get() == 1:
			s = s + "P "
		s = s + self.varMethod.get() + "/" + self.varBasisset.get()
		header.append( s)

		if self.varOptimization.get() == 1 :
			s = "Opt"
			if self.varTS.get() == 1\
				or self.varNoEigenTest.get() == 1\
				or self.varCalcFC.get() == 1\
				or self.varReadFC.get() == 1\
				or self.varOptAlgorithm.get() == "GDIIS"\
				or self.varOptAlgorithm.get() == "Big" :
				s = s + "=("
				if self.varTS.get() == 1 :
					s = s + "TS"
					if self.varNoEigenTest.get() == 1\
						or self.varCalcFC.get()== 1\
						or self.varReadFC.get() == 1\
						or self.varOptAlgorithm.get() == "GDIIS"\
						or self.varOptAlgorithm.get() == "Big" :
						s = s + ","
				if self.varNoEigenTest.get() == 1 :
					s = s + "NoEigenTest"
					if self.varCalcFC.get() == 1\
						or self.varReadFC.get() == 1\
						or self.varOptAlgorithm.get() == "GDIIS"\
						or self.varOptAlgorithm.get() == "Big" :
						s = s + ","
				if self.varCalcFC.get() == 1 :
					s = s + "CalcFC"
					if self.varReadFC.get() == 1\
						or self.varOptAlgorithm.get() == "GDIIS"\
						or self.varOptAlgorithm.get() == "Big" :
						s = s + ","
				if self.varReadFC.get() == 1 :
					s = s + "ReadFC"
					if self.varOptAlgorithm.get() == "GDIIS"\
						or self.varOptAlgorithm.get() == "Big" :
						s = s + ","
				if self.varOptAlgorithm.get() == "GDIIS" :
					s = s + "GDIIS"
				elif self.varOptAlgorithm.get() == "Big" :
					s = s + "Big"

				s = s + ")"
			header.append( s)

		if self.varFrequency.get() == 1 :
			s = "Freq"
			if self.varNoRaman.get() == 1\
				or self.varHindered.get() == 1\
				or self.varFreqReadFC.get() == 1:
				s = s + "=("
				if self.varFreqReadFC.get() == 1:
					s = s + "ReadFC"
				if self.varNoRaman.get() == 1:
					if self.varFreqReadFC.get() == 1:
						s = s + ","
					s = s + "NoRaman"
				if self.varHindered.get() == 1:
					if self.varFreqReadFC.get() == 1\
						or self.varNoRaman.get() == 1:
						s = s + ","
					s = s + "Hindered"
				s = s + ")"
			header.append( s)

		if self.varNMR.get() == 1 :
			s = "NMR"
			header.append( s)

		s = "Pop="
		if self.varPopulation.get() == "NBO" :
			s = s + "NBORead IOP(3/59=10)"
		else :
			s = s + self.varPopulation.get()
		header.append( s)

		if self.varViewmol.get() == 1 :
			s = "GFPrint Iop(5/33=2)"
			header.append( s)

		if self.varGFInput.get() == 1 :
			s = "GFInput"
			header.append( s)

#		s = "SCF=(Direct,Tight)"
#		header.append( s)

		if lower( self.varSolventmethod.get()) != "none" :
			s = "SCRF=(" + self.varSolventmethod.get() + ","
			s = s + "Solvent=" + self.varSolventsolvent.get() + ")"
			header.append( s)

		if self.varGuess.get() == 1:
			s = "Guess=Read"
			header.append( s)

		if self.varGeometry.get() == 1:
			s = "Geom=AllCheck"
			header.append( s)

		if self.varPseudo.get() == 1:
			s = "Pseudo=Read"
			header.append( s)

		header.append( "")#trailing blank line

#title section
		if len( self.varTitle.get()) > 0 :
			header.append( self.varTitle.get())
		header.append( "")

#charge, multiplicity
		if self.varGeometry.get() != 1:
			header.append( self.varCharge.get() + " " + self.varMultiplicity.get())

#cartesian coordinates
		if self.varGeometry.get() != 1:
			for line in coordinates :
				header.append( line)
		header.append( "")

#nbo section
		if self.varPopulation.get() == "NBO" :
			header.append( "$nbo reson bndidx $end")
			header.append( "")

#end of input file generation

		d = FinalDialog( root, "Output", header)
		if d.result == 1 :
			file = open( filename, "w")
			file.write( d.textfield)
			file.close()
			self.mainframe.quit()



#
# main program
#
root = Tk()
root.tk_strictMotif( 1)
app = Application( root)
root.title('Input for Gaussian 98')
root.mainloop()
