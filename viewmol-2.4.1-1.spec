Summary: Molecule viewer and editor
Name: viewmol
Version: 2.4.1
Release: 1
Copyright: GPL
Group: Application/Graphics
Source: http://prdownloads.sourceforge.net/viewmol/viewmol-2.4.1.src.tgz
%description
Viewmol is a graphical front end for computational chemistry programs.
It is able to graphically aid in the generation of molecular structures
for computations and to visualize their results. At present Viewmol
includes input filters for Discover, DMol3, Gamess, Gaussian 9x/03, Gulp,
Mopac, PQS, Turbomole, and Vamp outputs as well as for PDB files. Structures
can be saved as Accelrys' car-files, MDL files, and Turbomole coordinate
files. Viewmol can generate input files for Gaussian 9x/03. Viewmol's file
format has been added to OpenBabel so that OpenBabel can serve as an
input as well as an output filter for coordinates.

%prep
%setup

%build
cd source; make

%install
cd source
./install /usr/local
cd ../kde
./install

%post
/usr/local/lib/viewmol/kde/uninstall magic

%define _unpackaged_files_terminate_build 0
%define _missing_doc_files_terminate_build 0

%files
/usr/local/bin/viewmol
/usr/local/lib/viewmol/Linux/bio
/usr/local/lib/viewmol/Linux/readgamess
/usr/local/lib/viewmol/Linux/readgauss
/usr/local/lib/viewmol/Linux/readmopac
/usr/local/lib/viewmol/Linux/readpdb
/usr/local/lib/viewmol/Linux/tm
%dir /usr/local/lib/viewmol/Linux
/usr/local/lib/viewmol/locale/de/Viewmol
/usr/local/lib/viewmol/locale/de/LC_MESSAGES/Viewmol.mo
%dir /usr/local/lib/viewmol/locale/de/LC_MESSAGES
%dir /usr/local/lib/viewmol/locale/de
/usr/local/lib/viewmol/locale/en_US/Viewmol
/usr/local/lib/viewmol/locale/en_US/LC_MESSAGES/Viewmol.mo
%dir /usr/local/lib/viewmol/locale/en_US/LC_MESSAGES
%dir /usr/local/lib/viewmol/locale/en_US
/usr/local/lib/viewmol/locale/es/Viewmol
/usr/local/lib/viewmol/locale/es/LC_MESSAGES/Viewmol.mo
%dir /usr/local/lib/viewmol/locale/es/LC_MESSAGES
%dir /usr/local/lib/viewmol/locale/es
/usr/local/lib/viewmol/locale/fr/Viewmol
/usr/local/lib/viewmol/locale/fr/LC_MESSAGES/Viewmol.mo
%dir /usr/local/lib/viewmol/locale/fr/LC_MESSAGES
%dir /usr/local/lib/viewmol/locale/fr
/usr/local/lib/viewmol/locale/hu/Viewmol
/usr/local/lib/viewmol/locale/hu/LC_MESSAGES/Viewmol.mo
%dir /usr/local/lib/viewmol/locale/hu/LC_MESSAGES
%dir /usr/local/lib/viewmol/locale/hu
/usr/local/lib/viewmol/locale/pl/Viewmol
/usr/local/lib/viewmol/locale/pl/LC_MESSAGES/Viewmol.mo
%dir /usr/local/lib/viewmol/locale/pl/LC_MESSAGES
%dir /usr/local/lib/viewmol/locale/pl
/usr/local/lib/viewmol/locale/ru/Viewmol
/usr/local/lib/viewmol/locale/ru/LC_MESSAGES/Viewmol.mo
%dir /usr/local/lib/viewmol/locale/ru/LC_MESSAGES
%dir /usr/local/lib/viewmol/locale/ru
/usr/local/lib/viewmol/doc/viewmol.html
/usr/local/lib/viewmol/doc/viewmol.pdf
/usr/local/lib/viewmol/doc/html/blueball.png
/usr/local/lib/viewmol/doc/html/bondform.png
/usr/local/lib/viewmol/doc/html/cellform.png
/usr/local/lib/viewmol/doc/html/coloreditor.png
/usr/local/lib/viewmol/doc/html/configurationform.png
/usr/local/lib/viewmol/doc/html/content.html
/usr/local/lib/viewmol/doc/html/crossref.png
/usr/local/lib/viewmol/doc/html/cyclohexane.png
/usr/local/lib/viewmol/doc/html/downarrow.png
/usr/local/lib/viewmol/doc/html/error.png
/usr/local/lib/viewmol/doc/html/historyform.png
/usr/local/lib/viewmol/doc/html/img38.png
/usr/local/lib/viewmol/doc/html/img39.png
/usr/local/lib/viewmol/doc/html/img40.png
/usr/local/lib/viewmol/doc/html/img41.png
/usr/local/lib/viewmol/doc/html/img42.png
/usr/local/lib/viewmol/doc/html/img43.png
/usr/local/lib/viewmol/doc/html/img44.png
/usr/local/lib/viewmol/doc/html/img45.png
/usr/local/lib/viewmol/doc/html/img46.png
/usr/local/lib/viewmol/doc/html/img47.png
/usr/local/lib/viewmol/doc/html/img48.png
/usr/local/lib/viewmol/doc/html/img49.png
/usr/local/lib/viewmol/doc/html/img50.png
/usr/local/lib/viewmol/doc/html/img51.png
/usr/local/lib/viewmol/doc/html/img52.png
/usr/local/lib/viewmol/doc/html/img53.png
/usr/local/lib/viewmol/doc/html/img54.png
/usr/local/lib/viewmol/doc/html/img55.png
/usr/local/lib/viewmol/doc/html/img56.png
/usr/local/lib/viewmol/doc/html/img57.png
/usr/local/lib/viewmol/doc/html/img58.png
/usr/local/lib/viewmol/doc/html/img59.png
/usr/local/lib/viewmol/doc/html/img60.png
/usr/local/lib/viewmol/doc/html/img61.png
/usr/local/lib/viewmol/doc/html/img62.png
/usr/local/lib/viewmol/doc/html/img63.png
/usr/local/lib/viewmol/doc/html/img64.png
/usr/local/lib/viewmol/doc/html/img65.png
/usr/local/lib/viewmol/doc/html/leftarrow.png
/usr/local/lib/viewmol/doc/html/modesform.png
/usr/local/lib/viewmol/doc/html/moform.png
/usr/local/lib/viewmol/doc/html/mouse.png
/usr/local/lib/viewmol/doc/html/newman.png
/usr/local/lib/viewmol/doc/html/node0.html
/usr/local/lib/viewmol/doc/html/node10.html
/usr/local/lib/viewmol/doc/html/node11.html
/usr/local/lib/viewmol/doc/html/node12.html
/usr/local/lib/viewmol/doc/html/node13.html
/usr/local/lib/viewmol/doc/html/node14.html
/usr/local/lib/viewmol/doc/html/node15.html
/usr/local/lib/viewmol/doc/html/node16.html
/usr/local/lib/viewmol/doc/html/node17.html
/usr/local/lib/viewmol/doc/html/node18.html
/usr/local/lib/viewmol/doc/html/node19.html
/usr/local/lib/viewmol/doc/html/node1.html
/usr/local/lib/viewmol/doc/html/node20.html
/usr/local/lib/viewmol/doc/html/node21.html
/usr/local/lib/viewmol/doc/html/node22.html
/usr/local/lib/viewmol/doc/html/node23.html
/usr/local/lib/viewmol/doc/html/node24.html
/usr/local/lib/viewmol/doc/html/node25.html
/usr/local/lib/viewmol/doc/html/node26.html
/usr/local/lib/viewmol/doc/html/node27.html
/usr/local/lib/viewmol/doc/html/node28.html
/usr/local/lib/viewmol/doc/html/node29.html
/usr/local/lib/viewmol/doc/html/node2.html
/usr/local/lib/viewmol/doc/html/node30.html
/usr/local/lib/viewmol/doc/html/node31.html
/usr/local/lib/viewmol/doc/html/node32.html
/usr/local/lib/viewmol/doc/html/node33.html
/usr/local/lib/viewmol/doc/html/node34.html
/usr/local/lib/viewmol/doc/html/node35.html
/usr/local/lib/viewmol/doc/html/node36.html
/usr/local/lib/viewmol/doc/html/node37.html
/usr/local/lib/viewmol/doc/html/node38.html
/usr/local/lib/viewmol/doc/html/node39.html
/usr/local/lib/viewmol/doc/html/node3.html
/usr/local/lib/viewmol/doc/html/node4.html
/usr/local/lib/viewmol/doc/html/node5.html
/usr/local/lib/viewmol/doc/html/node6.html
/usr/local/lib/viewmol/doc/html/node7.html
/usr/local/lib/viewmol/doc/html/node8.html
/usr/local/lib/viewmol/doc/html/node9.html
/usr/local/lib/viewmol/doc/html/orangeball.png
/usr/local/lib/viewmol/doc/html/printform.png
/usr/local/lib/viewmol/doc/html/pseform.png
/usr/local/lib/viewmol/doc/html/rightarrow.png
/usr/local/lib/viewmol/doc/html/setatomcolors.png
/usr/local/lib/viewmol/doc/html/spectrumform.png
/usr/local/lib/viewmol/doc/html/thermoform.png
/usr/local/lib/viewmol/doc/html/uparrow.png
/usr/local/lib/viewmol/doc/html/viewmol.css
/usr/local/lib/viewmol/doc/html/viewmol.png
/usr/local/lib/viewmol/doc/html/waveform1.png
/usr/local/lib/viewmol/doc/html/waveform2.png
/usr/local/lib/viewmol/doc/html/writegauss.png
%dir /usr/local/lib/viewmol/doc/html
%dir /usr/local/lib/viewmol/doc
/usr/local/lib/viewmol/examples/discover/benzene.cor
/usr/local/lib/viewmol/examples/discover/benzene.hessian
%dir /usr/local/lib/viewmol/examples/discover
/usr/local/lib/viewmol/examples/dmol/quartz_homo.grd
/usr/local/lib/viewmol/examples/dmol/quartz_lumo.grd
/usr/local/lib/viewmol/examples/dmol/quartz.outmol
%dir /usr/local/lib/viewmol/examples/dmol
/usr/local/lib/viewmol/examples/gamess/c10h8n2.out
%dir /usr/local/lib/viewmol/examples/gamess
/usr/local/lib/viewmol/examples/gaussian/h2o.log
/usr/local/lib/viewmol/examples/gaussian/methanol.log
/usr/local/lib/viewmol/examples/gaussian/mmdc1.log
%dir /usr/local/lib/viewmol/examples/gaussian
/usr/local/lib/viewmol/examples/gulp/urea.out
%dir /usr/local/lib/viewmol/examples/gulp
/usr/local/lib/viewmol/examples/mopac/adamantane.gpt
/usr/local/lib/viewmol/examples/mopac/adamantane.out
%dir /usr/local/lib/viewmol/examples/mopac
/usr/local/lib/viewmol/examples/pdb/fau1.pdb
/usr/local/lib/viewmol/examples/pdb/gpc.pdb
/usr/local/lib/viewmol/examples/pdb/octa.pdb
%dir /usr/local/lib/viewmol/examples/pdb
/usr/local/lib/viewmol/examples/pqs/cpcpm.out
%dir /usr/local/lib/viewmol/examples/pqs
/usr/local/lib/viewmol/examples/reaction/h2o.outmol
/usr/local/lib/viewmol/examples/reaction/hio3.outmol
/usr/local/lib/viewmol/examples/reaction/hno3.outmol
/usr/local/lib/viewmol/examples/reaction/i2.outmol
/usr/local/lib/viewmol/examples/reaction/no.outmol
%dir /usr/local/lib/viewmol/examples/reaction
/usr/local/lib/viewmol/examples/turbomole/basis
/usr/local/lib/viewmol/examples/turbomole/control
/usr/local/lib/viewmol/examples/turbomole/coord
/usr/local/lib/viewmol/examples/turbomole/gradient
/usr/local/lib/viewmol/examples/turbomole/mos
%dir /usr/local/lib/viewmol/examples/turbomole
%dir /usr/local/lib/viewmol/examples
/usr/local/lib/viewmol/scripts/demo.py
/usr/local/lib/viewmol/scripts/setAtomColors.py
/usr/local/lib/viewmol/scripts/template.py
/usr/local/lib/viewmol/scripts/uff.py
%dir /usr/local/lib/viewmol/scripts
/usr/local/lib/viewmol/tests/autotest.py
/usr/local/lib/viewmol/tests/importtest.py
%dir /usr/local/lib/viewmol/tests
/usr/local/lib/viewmol/readdmol
/usr/local/lib/viewmol/readdmol.awk
/usr/local/lib/viewmol/readgulp
/usr/local/lib/viewmol/readpqs
/usr/local/lib/viewmol/viewmolrc
/usr/local/lib/viewmol/writecar
/usr/local/lib/viewmol/writegauss.py
/usr/local/lib/viewmol/writemol
/usr/local/lib/viewmol/writetm
/usr/local/lib/viewmol/kde/uninstall
/usr/local/lib/viewmol/kde/mimelnk/magic
%dir /usr/local/lib/viewmol/kde/mimelnk
%dir /usr/local/lib/viewmol/kde
%dir /usr/local/lib/viewmol
/usr/share/mimelnk/chemical/x-dmol.desktop
/usr/share/mimelnk/chemical/x-gamess-output.desktop
/usr/share/mimelnk/chemical/x-gaussian-log.desktop
/usr/share/mimelnk/chemical/x-gulp.desktop
/usr/share/mimelnk/chemical/x-mopac.desktop
/usr/share/mimelnk/chemical/x-msi-car.desktop
/usr/share/mimelnk/chemical/x-msi-hessian.desktop
/usr/share/mimelnk/chemical/x-msi-mdf.desktop
/usr/share/mimelnk/chemical/x-msi-msi.desktop
/usr/share/mimelnk/chemical/x-pdb.desktop
/usr/share/mimelnk/chemical/x-pqs-output.desktop
/usr/share/mimelnk/chemical/x-turbomole-basis.desktop
/usr/share/mimelnk/chemical/x-turbomole-control.desktop
/usr/share/mimelnk/chemical/x-turbomole-coord.desktop
/usr/share/mimelnk/chemical/x-turbomole-grad.desktop
/usr/share/mimelnk/chemical/x-turbomole-scfmo.desktop
/usr/share/mimelnk/chemical/x-turbomole-vibrational.desktop
%dir /usr/share/mimelnk/chemical
/usr/share/icons/default.kde/128x128/apps/viewmol.png
/usr/share/icons/default.kde/64x64/apps/viewmol.png
/usr/share/icons/default.kde/48x48/apps/viewmol.png
/usr/share/icons/default.kde/32x32/apps/viewmol.png
/usr/share/icons/default.kde/128x128/mimetypes/molecule.png
/usr/share/icons/default.kde/64x64/mimetypes/molecule.png
/usr/share/icons/default.kde/48x48/mimetypes/molecule.png
/usr/share/icons/default.kde/32x32/mimetypes/molecule.png
/usr/share/icons/default.kde/128x128/mimetypes/qc.png
/usr/share/icons/default.kde/64x64/mimetypes/qc.png
/usr/share/icons/default.kde/48x48/mimetypes/qc.png
/usr/share/icons/default.kde/32x32/mimetypes/qc.png
/usr/share/icons/default.kde/128x128/mimetypes/solid.png
/usr/share/icons/default.kde/64x64/mimetypes/solid.png
/usr/share/icons/default.kde/48x48/mimetypes/solid.png
/usr/share/icons/default.kde/32x32/mimetypes/solid.png
/usr/share/applications/viewmol.desktop
/usr/share/applnk/Graphics/viewmol.desktop
