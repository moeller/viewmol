/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                                 M E N U . H                                  *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: menu.h,v 1.6 2003/11/07 12:49:39 jrh Exp $
* $Log: menu.h,v $
* Revision 1.6  2003/11/07 12:49:39  jrh
* Release 2.4
*
* Revision 1.5  2000/12/10 15:11:11  jrh
* Release 2.3
*
* Revision 1.4  1999/05/24 01:28:42  jrh
* Release 2.2.1
*
* Revision 1.3  1999/02/07 21:52:09  jrh
* Release 2.2
*
* Revision 1.2  1998/01/26 00:48:30  jrh
* Release 2.1
*
* Revision 1.1  1996/12/10  18:45:49  jrh
* Initial revision
*
*/
struct MenuItem
{
    char        *label;          /* the label for the item */
    WidgetClass *class;          /* pushbutton, label, separator... */
    void       (*callback)();    /* routine to call; NULL if none */
    XtPointer    callback_data;  /* client_data for callback() */
    Widget      widget;          /* the widget of the menu item */
    struct MenuItem *subitems;   /* pullright menu items, if not NULL */
};

struct MENUDATA
{
  struct MenuItem *submenu;
  int window;
  int which;
};
