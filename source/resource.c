/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                             R E S O U R C E . C                              *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: resource.c,v 1.6 2003/11/07 11:15:05 jrh Exp $
* $Log: resource.c,v $
* Revision 1.6  2003/11/07 11:15:05  jrh
* Release 2.4
*
* Revision 1.5  2000/12/10 15:16:06  jrh
* Release 2.3
*
* Revision 1.4  1999/05/24 01:27:30  jrh
* Release 2.2.1
*
* Revision 1.3  1999/02/07 21:56:26  jrh
* Release 2.2
*
* Revision 1.2  1998/01/26 00:49:25  jrh
* Release 2.1
*
* Revision 1.1  1996/12/10  18:43:46  jrh
* Initial revision
*
*/
#include<X11/Xlib.h>
#include<X11/Intrinsic.h>
#include<Xm/Xm.h>

int getIntResource(Widget widget, char *resource)   /* get an integer from the resources */
{
  int ret;
  XtResource resource_description[1];

  resource_description[0].resource_name=resource;
  resource_description[0].resource_class=XmCWidth;
  resource_description[0].resource_type=XmRInt;
  resource_description[0].resource_size=sizeof(int);
  resource_description[0].resource_offset=0;
  resource_description[0].default_type=XmRInt;
  resource_description[0].default_addr=NULL;

  XtGetApplicationResources(widget, &ret, resource_description, 1, NULL, 0);
  return(ret);
}

char *getStringResource(Widget widget, char *resource)  /* get a string from the resources */
{
  char *ret;
  XtResource resource_description[1];

  resource_description[0].resource_name=resource;
  resource_description[0].resource_class=XmCString;
  resource_description[0].resource_type=XmRString;
  resource_description[0].resource_size=sizeof(char *);
  resource_description[0].resource_offset=0;
  resource_description[0].default_type=XmRString;
  resource_description[0].default_addr=NULL;

  XtGetApplicationResources(widget, &ret, resource_description, 1, NULL, 0);
  return(ret);
}
