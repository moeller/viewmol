/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                              M E N U D E F . H                               *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: menudef.h,v 1.6 2003/11/07 12:49:33 jrh Exp $
* $Log: menudef.h,v $
* Revision 1.6  2003/11/07 12:49:33  jrh
* Release 2.4
*
* Revision 1.5  2000/12/10 15:11:16  jrh
* Release 2.3
*
* Revision 1.4  1999/05/24 01:26:24  jrh
* Release 2.2.1
*
* Revision 1.3  1999/02/07 21:52:16  jrh
* Release 2.2
*
* Revision 1.2  1998/01/26 00:48:32  jrh
* Release 2.1
*
* Revision 1.1  1996/12/10  18:45:53  jrh
* Initial revision
*
*/
extern void load(Widget, caddr_t, XmAnyCallbackStruct *);
extern void deleteMolecule(Widget, caddr_t, XmAnyCallbackStruct *);
extern void ende(Widget, caddr_t,  XmDrawingAreaCallbackStruct *);
extern void setModel(Widget, caddr_t, XmDrawingAreaCallbackStruct *);
extern void setDiagram(Widget, caddr_t, XmDrawingAreaCallbackStruct *);
extern void changeBoolean(Widget, int *, XmDrawingAreaCallbackStruct *);
extern void clearGeometry(Widget, caddr_t, XmAnyCallbackStruct *);
extern void undoGeometry(Widget, caddr_t, XmAnyCallbackStruct *);
extern void spectrumDialog(Widget, caddr_t, XmAnyCallbackStruct *);
extern void optimizationDialog(Widget, caddr_t, XmAnyCallbackStruct *);
extern void MODiagramDialog(Widget, caddr_t, XmAnyCallbackStruct *);
extern void wavefunctionDialog(Widget, caddr_t, XmAnyCallbackStruct *);
extern void drawingModeDialog(Widget, caddr_t, XmAnyCallbackStruct *);
extern void printDialog(Widget, caddr_t, XmAnyCallbackStruct *);
extern void colorEditor(Widget, caddr_t, XmAnyCallbackStruct *);
extern void readSpectrum(Widget, caddr_t, XmAnyCallbackStruct *);
extern void deleteSpectrum(Widget, caddr_t, XmAnyCallbackStruct *);
extern void quitSpectrum(Widget, caddr_t, XmAnyCallbackStruct *);
extern void quitHistory(Widget, caddr_t, XmAnyCallbackStruct *);
extern void quitMODiagram(Widget, caddr_t, XmAnyCallbackStruct *);
extern void zoomOut(Widget, caddr_t, caddr_t);
extern void manual(Widget, caddr_t, caddr_t);
extern void saveMoleculeDialog(Widget, caddr_t, XmAnyCallbackStruct *);
extern void configurationDialog(Widget, caddr_t, XmAnyCallbackStruct *);
extern void thermoDialog(Widget, caddr_t, XmAnyCallbackStruct *);
extern void raytrace(Widget, caddr_t, caddr_t);
extern void setTransition(Widget, caddr_t, XmToggleButtonCallbackStruct *);
extern void setAnnotation(Widget, int *, XmDrawingAreaCallbackStruct *);
extern void unitcellDialog(Widget, caddr_t, XmAnyCallbackStruct *);
extern void newMolecule(Widget, caddr_t, XmAnyCallbackStruct *);
extern void pseDialog(Widget, caddr_t, XmAnyCallbackStruct *);
extern void bondDialog(Widget, caddr_t, XmAnyCallbackStruct *);
extern void setHistoryAnimation(Widget, caddr_t, XmAnyCallbackStruct *);
extern void runScript(Widget, caddr_t, XmAnyCallbackStruct *);

#ifdef INIT
static struct MenuItem moleculeMenu[] = {
  { "loadMolecule",   &xmPushButtonGadgetClass, load,               (XtPointer)0,    NULL, NULL },
  { "saveMolecule",   &xmPushButtonGadgetClass, saveMoleculeDialog, (XtPointer)1,    NULL, NULL },
/*{ "saveSelected",   &xmPushButtonGadgetClass, saveMoleculeDialog, (XtPointer)0,    NULL, NULL },*/
  { "deleteMolecule", &xmPushButtonGadgetClass, deleteMolecule,     (XtPointer)TRUE, NULL, NULL },
  { "newMolecule",    &xmPushButtonGadgetClass, newMolecule,        (XtPointer)0,    NULL, NULL },
  { "buildMolecule",  &xmPushButtonGadgetClass, pseDialog,          (XtPointer)0,    NULL, NULL },
  { NULL }};
static struct MenuItem geometryMenu[] = {
  { "clear_all",  &xmPushButtonGadgetClass, clearGeometry, (XtPointer)ALL,  NULL, NULL },
  { "clear_last", &xmPushButtonGadgetClass, clearGeometry, (XtPointer)LAST, NULL, NULL },
  { "sep",        &xmSeparatorGadgetClass,  NULL,          (XtPointer)0,    NULL, NULL },
  { "undo",       &xmPushButtonGadgetClass, undoGeometry,  (XtPointer)0,    NULL, NULL },
  { NULL }};
static struct MenuItem viewerMenu[] = {
  { "molecule",                  &xmPushButtonGadgetClass,   NULL,               (XtPointer)0,          NULL, moleculeMenu },
  { "select_molecule",           &xmPushButtonGadgetClass,   NULL,               (XtPointer)0,          NULL, NULL },
  { "sep1",                      &xmSeparatorGadgetClass,    NULL,               (XtPointer)0,          NULL, NULL },
  { "wire_model",                &xmPushButtonGadgetClass,   setModel,           (XtPointer)WIREMODEL,  NULL, NULL },
  { "stick_model",               &xmPushButtonGadgetClass,   setModel,           (XtPointer)STICKMODEL, NULL, NULL },
  { "ball_and_stick_model",      &xmPushButtonGadgetClass,   setModel,           (XtPointer)BALLMODEL,  NULL, NULL },
  { "cpk_model",                 &xmPushButtonGadgetClass,   setModel,           (XtPointer)CUPMODEL,   NULL, NULL },
  { "sep2",                      &xmSeparatorGadgetClass,    NULL,               (XtPointer)0,          NULL, NULL },
  { "geometry_menu",             &xmPushButtonGadgetClass,   NULL,               (XtPointer)0,          NULL, geometryMenu },
  { "bondType_menu",             &xmPushButtonGadgetClass,   bondDialog,         (XtPointer)0,          NULL, NULL },
  { "wave_function",             &xmPushButtonGadgetClass,   wavefunctionDialog, (XtPointer)0,          NULL, NULL },
  { "energy_level_diagram",      &xmPushButtonGadgetClass,   initMODiagram,      (XtPointer)0,          NULL, NULL },
  { "optimization_history",      &xmPushButtonGadgetClass,   initHistory,        (XtPointer)0,          NULL, NULL },
  { "show_forces",               &xmToggleButtonGadgetClass, changeBoolean,      (XtPointer)&showForces, NULL, NULL },
  { "spectrum",                  &xmPushButtonGadgetClass,   initSpectrum,       (XtPointer)0,          NULL, NULL },
  { "thermodynamics",            &xmPushButtonGadgetClass,   thermoDialog,       (XtPointer)0,          NULL, NULL },
  { "show_unit_cell",            &xmPushButtonGadgetClass,   unitcellDialog,     (XtPointer)0,          NULL, NULL },
  { "show_ellipsoid_of_inertia", &xmToggleButtonGadgetClass, changeBoolean,      (XtPointer)&showInertia, NULL, NULL },
  { "sep3",                      &xmSeparatorGadgetClass,    NULL,               (XtPointer)0,          NULL, NULL },
  { "drawing_modes",             &xmPushButtonGadgetClass,   drawingModeDialog,  (XtPointer)0,          NULL, NULL },
  { "foreground_color",          &xmPushButtonGadgetClass,   colorEditor,        (XtPointer)(FOREGROUND*8+VIEWER), NULL, NULL},
  { "background_color",          &xmPushButtonGadgetClass,   colorEditor,        (XtPointer)(BACKGROUND*8+VIEWER), NULL, NULL},
  { "label_atoms",               &xmToggleButtonGadgetClass, changeBoolean,      (XtPointer)&label,     NULL, NULL },
  { "annotate",                  &xmPushButtonGadgetClass,   setAnnotation,      (XtPointer)0,          NULL, NULL },
  { "runScript",                 &xmPushButtonGadgetClass,   NULL,               (XtPointer)0,          NULL, NULL },
  { "sep4",                      &xmSeparatorGadgetClass,    NULL,               (XtPointer)0,          NULL, NULL },
  { "hardcopy",                  &xmPushButtonGadgetClass,   printDialog,        (XtPointer)VIEWER,     NULL, NULL },
  { "raytracing",                &xmPushButtonGadgetClass,   raytrace,           (XtPointer)0,          NULL, NULL },
  { "manual",                    &xmPushButtonGadgetClass,   manual,             (XtPointer)0,          NULL, NULL },
  { "sep5",                      &xmSeparatorGadgetClass,    NULL,               (XtPointer)0,          NULL, NULL },
  { "saveConfiguration",         &xmPushButtonGadgetClass,   configurationDialog,(XtPointer)0,          NULL, NULL },
  { "quit",                      &xmPushButtonGadgetClass,   ende,               (XtPointer)0,          NULL, NULL },
  { NULL } };
static struct MenuItem spectrumMenu[] = {
  { "settings_spectrum",      &xmPushButtonGadgetClass,   spectrumDialog,   (XtPointer)0,        NULL, NULL },
  { "select_molecule",        &xmPushButtonGadgetClass,   NULL,             (XtPointer)0,        NULL, NULL },
  { "imaginary_wave_numbers", &xmPushButtonGadgetClass,   NULL,             (XtPointer)0,        NULL, NULL },
  { "observed_spectrum",      &xmPushButtonGadgetClass,   readSpectrum,     (XtPointer)0,        NULL, NULL },
  { "delete_spectrum",        &xmPushButtonGadgetClass,   deleteSpectrum,   (XtPointer)0,        NULL, NULL },
  { "zoom_out",               &xmPushButtonGadgetClass,   zoomOut,          (XtPointer)SPECTRUM, NULL, NULL },
  { "hardcopy",               &xmPushButtonGadgetClass,   printDialog,      (XtPointer)SPECTRUM, NULL, NULL },
  { "sep",                    &xmSeparatorGadgetClass,    NULL,             (XtPointer)0,        NULL, NULL },
  { "foreground_color",       &xmPushButtonGadgetClass,   colorEditor,      (XtPointer)(FOREGROUND*8+SPECTRUM), NULL, NULL},
  { "background_color",       &xmPushButtonGadgetClass,   colorEditor,      (XtPointer)(BACKGROUND*8+SPECTRUM), NULL, NULL},
  { "sep",                    &xmSeparatorGadgetClass,    NULL,             (XtPointer)0,        NULL, NULL },
  { "quit_spectrum",          &xmPushButtonGadgetClass,   quitSpectrum,     (XtPointer)SPECTRUM, NULL, NULL },
  { NULL } };
/* #endif */
static struct MenuItem historyMenu[] = {
  { "settings_history", &xmPushButtonGadgetClass,   optimizationDialog,  (XtPointer)0,       NULL, NULL },
  { "select_molecule",  &xmPushButtonGadgetClass,   NULL,                (XtPointer)0,       NULL, NULL },
  { "animate_history",  &xmToggleButtonGadgetClass, setHistoryAnimation, (XtPointer)0,       NULL, NULL },
  { "hardcopy",         &xmPushButtonGadgetClass,   printDialog,         (XtPointer)HISTORY, NULL, NULL },
  { "sep",              &xmSeparatorGadgetClass,    NULL,                (XtPointer)0,       NULL, NULL },
  { "energy_color",     &xmPushButtonGadgetClass,   colorEditor,         (XtPointer)(ENERGY*8+HISTORY), NULL, NULL},
  { "gradient_color",   &xmPushButtonGadgetClass,   colorEditor,         (XtPointer)(GNORM*8+HISTORY), NULL, NULL},
  { "background_color", &xmPushButtonGadgetClass,   colorEditor,         (XtPointer)(BACKGROUND*8+HISTORY), NULL, NULL},
  { "sep",              &xmSeparatorGadgetClass,    NULL,                (XtPointer)0,       NULL, NULL },
  { "quit_history",     &xmPushButtonGadgetClass,   quitHistory,         (XtPointer)HISTORY, NULL, NULL },
  { NULL } };
/* #ifdef INIT */
static struct MenuItem MODiagramMenu[] = {
  { "settings_modiagram", &xmPushButtonGadgetClass,   MODiagramDialog,  (XtPointer)0,  NULL, NULL },
  { "select_molecule",    &xmPushButtonGadgetClass,   NULL,             (XtPointer)0,  NULL, NULL },
  { "transition",         &xmToggleButtonGadgetClass, setTransition,    (XtPointer)0,  NULL, NULL },
  { "zoom_out",           &xmPushButtonGadgetClass,   zoomOut,          (XtPointer)MO, NULL, NULL },
  { "hardcopy",           &xmPushButtonGadgetClass,   printDialog,      (XtPointer)MO, NULL, NULL },
  { "energy_levels",      &xmToggleButtonGadgetClass, setDiagram,       (XtPointer)0,  NULL, NULL },
  { "sep",                &xmSeparatorGadgetClass,    NULL,             (XtPointer)0,  NULL, NULL },
  { "foreground_color",   &xmPushButtonGadgetClass,   colorEditor,      (XtPointer)(FOREGROUND*8+MO), NULL, NULL},
  { "background_color",   &xmPushButtonGadgetClass,   colorEditor,      (XtPointer)(BACKGROUND*8+MO), NULL, NULL},
  { "sep",                &xmSeparatorGadgetClass,    NULL,             (XtPointer)0,  NULL, NULL },
  { "quit_modiagram",     &xmPushButtonGadgetClass,   quitMODiagram,    (XtPointer)MO, NULL, NULL },
  { NULL } };
#endif
