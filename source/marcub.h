/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                               M A R C U B . C                                *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: marcub.h,v 1.6 2003/11/07 12:49:25 jrh Exp $
* $Log: marcub.h,v $
* Revision 1.6  2003/11/07 12:49:25  jrh
* Release 2.4
*
* Revision 1.5  2000/12/10 15:10:57  jrh
* Release 2.3
*
* Revision 1.4  1999/05/24 01:26:18  jrh
* Release 2.2.1
*
* Revision 1.3  1999/02/07 21:51:20  jrh
* Release 2.2
*
* Revision 1.2  1998/01/26 00:48:28  jrh
* Release 2.1
*
* Revision 1.1  1996/12/10  18:45:43  jrh
* Initial revision
*
*/

#define IP_NONE   1
#define IP_LINEAR 2
#define IP_LOG    3
