/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                               O S N A M E . C                                *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: osname.c,v 1.6 2003/11/07 11:08:19 jrh Exp $
* $Log: osname.c,v $
* Revision 1.6  2003/11/07 11:08:19  jrh
* Release 2.4
*
* Revision 1.5  2000/12/10 15:13:38  jrh
* Release 2.3
*
* Revision 1.4  1999/05/24 01:26:53  jrh
* Release 2.2.1
*
* Revision 1.3  1999/02/07 21:54:13  jrh
* Release 2.2
*
* Revision 1.2  1998/01/26 00:49:00  jrh
* Release 2.1
*
* Revision 1.1  1996/12/10  18:42:54  jrh
* Initial revision
*
*/
#include<stdio.h>
#include<string.h>
#include<sys/utsname.h>

void osname(char *os)
{
  struct utsname sysname;
  FILE *pipe;

  uname(&sysname);

  strcpy(os, sysname.sysname);
  if (!strncmp(sysname.sysname, "AIX", 3))
  {
    strcat(os, "_");
    if (sysname.machine[8] == '7' && sysname.machine[9] == '0')
      strcat(os, "POWER2");
    else
      strcat(os, "POWER");
  }
  else if (strstr(sysname.sysname, "IRIX"))
  {
    strcat(os, "_");
    if ((pipe=popen("hinv | awk '/^CPU/ {print $3}' | cut -d/ -f2", "r")) != NULL)
    {
      fscanf(pipe, "%s", os+strlen(os));
      pclose(pipe);
    }
    else
      strcat(os, "R3000");
  }
}
