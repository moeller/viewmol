/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                               S H A D O W . C                                *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: shadow.c,v 1.1 2003/11/07 11:16:08 jrh Exp $
* $Log: shadow.c,v $
* Revision 1.1  2003/11/07 11:16:08  jrh
* Initial revision
*
*/

#include<GL/gl.h>
#include "viewmol.h"

int findShadowMapSupport(void)
{
  const GLubyte *extensions=NULL;
  int hasTextureEnvCombine=FALSE, hasNVRegisterCombiners=FALSE;

  extensions=glGetString(GL_EXTENSIONS);
  if (strstr((char *)extensions, "GL_EXT_texture_env_combine")) hasTextureEnvCombine=TRUE;
  if (strstr((char *)extensions, "GL_NV_register_combiners")) hasNVRegisterCombiners=TRUE;
  if (hasTextureEnvCombine || hasNVRegisterCombiners) return(TRUE);
  else                                                return(FALSE);

}
