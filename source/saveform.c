/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                             S A V E F O R M . C                              *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: saveform.c,v 1.5 2003/11/07 11:15:42 jrh Exp $
* $Log: saveform.c,v $
* Revision 1.5  2003/11/07 11:15:42  jrh
* Release 2.4
*
* Revision 1.4  2000/12/10 15:16:38  jrh
* Release 2.3
*
* Revision 1.3  1999/05/24 01:27:37  jrh
* Release 2.2.1
*
* Revision 1.2  1999/02/07 21:56:55  jrh
* Release 2.2
*
* Revision 1.1  1998/01/26 00:35:25  jrh
* Initial revision
*
*/
#include<X11/Intrinsic.h>
#include<Xm/Xm.h>
#include<Xm/BulletinB.h>
#include<Xm/Form.h>
#include<Xm/Label.h>
#include<Xm/LabelG.h>
#include<Xm/PanedW.h>
#include<Xm/PushB.h>
#include<Xm/PushBG.h>
#include<Xm/RowColumn.h>
#include<Xm/Separator.h>
#include<Xm/ToggleB.h>
#include "viewmol.h"
#include "dialog.h"

void saveMoleculeExit(Widget, caddr_t, XmPushButtonCallbackStruct *);
void GetOutputType(Widget, caddr_t, XmToggleButtonCallbackStruct *);

extern void MapBox(Widget, caddr_t, XmToggleButtonCallbackStruct *);
extern Widget CreatePushButtonRow(Widget, struct PushButtonRow *, int);
extern Widget CreateToggleBox(Widget, struct PushButtonRow *, int, int,
                              int, int, int);
extern void saveMolecule(char *);
extern void setMenuItem(int, int);
extern void *getmem(size_t, size_t);
extern void fremem(void **);
extern Widget initShell(Widget, char *, Widget *, Widget *);

extern struct WINDOW windows[];
extern struct OPTION *output;
extern Widget topShell;
extern int outputType, noutput, saveAll;
static Widget dialog;
static int outputType_save;

void saveMoleculeDialog(Widget widget, caddr_t all, XmAnyCallbackStruct *data)
{
  Widget form, board, form1, togglebox, sep;
  static struct PushButtonRow buttons[] = {
    { "ok", saveMoleculeExit, (XtPointer)TRUE, NULL },
    { "cancel", saveMoleculeExit, (XtPointer)FALSE, NULL },
  };
  struct PushButtonRow *togglebox_buttons;
  register int i;

  /* This function creates the dialog for the "Save molecule" option */

  if (all) saveAll=TRUE;
  else     saveAll=FALSE;
  switch (noutput)
  {
    case 0:  return;
    case 1:  saveMolecule(NULL);
             break;
    default: setMenuItem(VIEWER1_SAVE, False);
             outputType_save=outputType;

             togglebox_buttons=(struct PushButtonRow *)getmem((size_t)noutput,
                                sizeof(struct PushButtonRow));
             for (i=0; i<noutput; i++)
             {
               togglebox_buttons[i].label=&output[i].flag[0];
               togglebox_buttons[i].callback=(void (*)())GetOutputType;
               togglebox_buttons[i].client_data=(XtPointer)i;
               togglebox_buttons[i].widget=NULL;
             }

             dialog=initShell(windows[VIEWER].widget,
					"saveMoleculeForm", &board, &form);

             form1=XtVaCreateWidget("controlarea", xmFormWidgetClass, form,
                                    NULL);
             sep=XtVaCreateManagedWidget("sep", xmSeparatorWidgetClass, form,
                                         NULL);

             togglebox=CreateToggleBox(form1, togglebox_buttons, noutput,
                                       XmVERTICAL, 1, True, outputType);
             XtVaSetValues(togglebox, XmNleftAttachment, XmATTACH_FORM,
                                      XmNrightAttachment, XmATTACH_FORM,
                                      XmNtopAttachment, XmATTACH_FORM,
                                      NULL);
             XtManageChild(form1);

             CreatePushButtonRow(form, buttons, XtNumber(buttons));
             XtAddCallback(dialog, XmNpopupCallback, (XtCallbackProc)MapBox,
                           (XmAnyCallbackStruct *)NULL);
             XtManageChild(form);
             XtManageChild(board);
 
             fremem((void **)&togglebox_buttons);
             break;
  }
}

void saveMoleculeExit(Widget button, caddr_t which, XmPushButtonCallbackStruct *data)
{
  XtDestroyWidget(dialog);
  if (!(int)which) outputType=outputType_save;
  else             saveMolecule(NULL);
  setMenuItem(VIEWER1_SAVE, True);
}

void GetOutputType(Widget button, caddr_t which, XmToggleButtonCallbackStruct *data)
{
  if (data->set) outputType=(int)which;
}
