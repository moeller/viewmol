/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                                  E O F . C                                   *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: eof.c,v 1.4 2003/11/07 11:01:34 jrh Exp $
* $Log: eof.c,v $
* Revision 1.4  2003/11/07 11:01:34  jrh
* Release 2.4
*
* Revision 1.3  2000/12/10 15:06:50  jrh
* Release 2.3
*
* Revision 1.2  1999/05/24 01:25:22  jrh
* Release 2.2.1
*
* Revision 1.1  1999/02/07 21:48:36  jrh
* Initial revision
*
*/
#include<stdio.h>
#include<stdlib.h>

void eof(char *type, char *file, int terminate)
{
  printf("$error %s %d %s\n", type, terminate, file);
  printf("$end\n");
  exit(0);
}
