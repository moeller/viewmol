/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                             C O N F F O R M . C                              *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: confform.c,v 1.4 2003/11/07 10:59:04 jrh Exp $
* $Log: confform.c,v $
* Revision 1.4  2003/11/07 10:59:04  jrh
* Release 2.4
*
* Revision 1.3  2000/12/10 15:03:36  jrh
* Release 2.3
*
* Revision 1.2  1999/05/24 01:25:00  jrh
* Release 2.2.1
*
* Revision 1.1  1999/02/07 21:45:59  jrh
* Initial revision
*
*
*/
#include<sys/types.h>
#include<dirent.h>
#include<fnmatch.h>
#include<stdlib.h>
#include<sys/stat.h>
#include<X11/Intrinsic.h>
#include<Xm/Xm.h>
#include<Xm/Form.h>
#include<Xm/Label.h>
#include<Xm/LabelG.h>
#include<Xm/PanedW.h>
#include<Xm/PushB.h>
#include<Xm/PushBG.h>
#include<Xm/RowColumn.h>
#include<Xm/Scale.h>
#include<Xm/Separator.h>
#include<Xm/Text.h>
#include<Xm/ToggleB.h>
#include "viewmol.h"
#include "dialog.h"

void configurationDialog(Widget, caddr_t, XmAnyCallbackStruct *);
void configurationDialogExit(Widget, caddr_t, XmPushButtonCallbackStruct *);
void SetLanguage(Widget, caddr_t, XmToggleButtonCallbackStruct *);
void GetString(Widget, caddr_t, XmAnyCallbackStruct *);
#if defined LINUX || defined DARWIN
int matchXdefaults(const struct dirent *);
#else
int matchXdefaults(struct dirent *);
#endif

extern Widget makeViewerMenu(Widget);
extern void makeSpectrumMenu(void);
extern void makeHistoryMenu(void);
extern void makeMODiagramMenu(void);
extern void MapBox(Widget, caddr_t, XmAnyCallbackStruct *);
extern Widget CreateToggleBox(Widget, struct PushButtonRow *, int, int,
                              int, int, int);
extern void CreatePushButtonRow(Widget, struct PushButtonRow *, int);
extern char *getStringResource(Widget, char *);
extern void checkProgram(char *, char *);
extern void saveConfiguration(void);
extern void GetMessageBoxButton(Widget, XtPointer, caddr_t);
extern int  messgb(Widget, int, char *, struct PushButtonRow *, int);
extern Widget initShell(Widget, char *, Widget *, Widget *);
extern void getFormats(void);
extern void *getmem(size_t, size_t);
extern void fremem(void **);

extern struct WINDOW windows[];
extern Widget topShell;
extern int saveLanguage;
extern char language[], webbrowser[], moloch[], raytracer[], displayImage[];

static Widget dialog;
static XrmDatabase database;
static struct PushButtonRow *radiobox_buttons=NULL;
static char language_save[20];

void configurationDialog(Widget widget, caddr_t which, XmAnyCallbackStruct *data)
{
  Widget board, form, sep, b, m, r, d;
  Widget radiobox=(Widget)NULL;
  struct dirent **namelist;
  static struct PushButtonRow buttons[] = {
    { "ok", configurationDialogExit, (XtPointer)1, NULL },
    { "save", configurationDialogExit, (XtPointer)2, NULL },
    { "cancel", configurationDialogExit, (XtPointer)3, NULL }
  };
  int n, lang=0;
  char filename[FILENAME_MAX];
  char *p;
  register int i;

  /* This function creates the configuration dialog */

  strcpy(language_save, language);

  dialog=initShell(windows[VIEWER].widget, "configurationForm",
			 &board, &form);
  database=XrmGetDatabase(XtDisplay(windows[VIEWER].widget));
  strcpy(filename, getenv("VIEWMOLPATH"));
  strcat(filename, "/locale");
  if ((n=scandir(filename, &namelist, matchXdefaults, alphasort)) > 0)
  {
    radiobox_buttons=(struct PushButtonRow *)getmem((size_t)n,
                      sizeof(struct PushButtonRow));
    for (i=0; i<n; i++)
    {
      p=namelist[i]->d_name;
      radiobox_buttons[i].label=p;
      radiobox_buttons[i].callback=SetLanguage;
      radiobox_buttons[i].client_data=(XtPointer)p;
      radiobox_buttons[i].widget=NULL;
      if (!strcmp(language, p)) lang=i;
    }
    radiobox=CreateToggleBox(form, radiobox_buttons, n,
                             XmVERTICAL, 4, True, lang);
    sep=XtVaCreateManagedWidget("sep", xmSeparatorWidgetClass, form,
                                XmNorientation, XmHORIZONTAL,
                                XmNtraversalOn, False,
                                XmNtopAttachment, XmATTACH_WIDGET,
                                XmNtopWidget, radiobox,
                                XmNleftAttachment, XmATTACH_FORM,
                                XmNrightAttachment, XmATTACH_FORM,
                                NULL);
  }
  (void)XtVaCreateManagedWidget("browserLabel", xmLabelWidgetClass, form, NULL);
  b=XtVaCreateManagedWidget("browser", xmTextWidgetClass, form,
                            XmNvalue, webbrowser,
                            NULL);
  XtAddCallback(b, XmNvalueChangedCallback, (XtCallbackProc)GetString, webbrowser);
  (void)XtVaCreateManagedWidget("molochLabel", xmLabelWidgetClass, form, NULL);
  m=XtVaCreateManagedWidget("moloch", xmTextWidgetClass, form,
                            XmNvalue, moloch,
                            NULL);
  XtAddCallback(m, XmNvalueChangedCallback, (XtCallbackProc)GetString, moloch);
  (void)XtVaCreateManagedWidget("raytracerLabel", xmLabelWidgetClass, form, NULL);
  r=XtVaCreateManagedWidget("raytracer", xmTextWidgetClass, form,
                            XmNvalue, raytracer,
                            NULL);
  XtAddCallback(r, XmNvalueChangedCallback, (XtCallbackProc)GetString, raytracer);
  (void)XtVaCreateManagedWidget("displayImageLabel", xmLabelWidgetClass, form, NULL);
  d=XtVaCreateManagedWidget("displayImage", xmTextWidgetClass, form,
                            XmNvalue, displayImage,
                            NULL);
  XtAddCallback(d, XmNvalueChangedCallback, (XtCallbackProc)GetString, displayImage);
  sep=XtVaCreateManagedWidget("sep", xmSeparatorWidgetClass, form,
                              XmNorientation, XmHORIZONTAL,
                              XmNtraversalOn, False,
                              XmNtopAttachment, XmATTACH_WIDGET,
                              XmNtopWidget, radiobox,
                              XmNleftAttachment, XmATTACH_FORM,
                              XmNrightAttachment, XmATTACH_FORM,
                              NULL);

  CreatePushButtonRow(form, buttons, XtNumber(buttons));
  XtAddCallback(dialog, XmNpopupCallback, (XtCallbackProc)MapBox, (XmAnyCallbackStruct *)NULL);
  XtManageChild(form);
  XtManageChild(board);
}

void SetLanguage(Widget button, caddr_t which, XmToggleButtonCallbackStruct *data)
{
  char filename[FILENAME_MAX];

  if (data->set)
  {
    strcpy(filename, getenv("VIEWMOLPATH"));
/*  strcat(filename, "/Xdefaults.");*/
    strcat(filename, "/locale/");
    strcat(filename, (char *)which);
    strcat(filename, "/Viewmol");
    XrmCombineFileDatabase(filename, &database, True);
    windows[VIEWER].menu=makeViewerMenu(windows[VIEWER].widget);
    if (windows[SPECTRUM].widget != NULL) makeSpectrumMenu();
    if (windows[HISTORY].widget != NULL) makeHistoryMenu();
    if (windows[MO].widget != NULL) makeMODiagramMenu();
    if (strcmp(language, (char *)which)) saveLanguage=TRUE;
    strcpy(language, (char *)which);
    getFormats();
  }
}

void configurationDialogExit(Widget button, caddr_t which, XmPushButtonCallbackStruct *dummy)
{
  static struct PushButtonRow buttons[] = {{"ok", GetMessageBoxButton, (XtPointer)0, NULL}};
  XmToggleButtonCallbackStruct data;
  char *word, line[MAXLENLINE];

  switch ((int)which)
  {
    case 1: checkProgram(line, webbrowser);
            if (line[0] == '\0')
            {
              word=getStringResource(topShell, "wrongBrowser");
              messgb(topShell, 0, word, buttons, 1);
            }
            else
              strcpy(webbrowser, line);
            checkProgram(line, moloch);
            if (line[0] == '\0')
            {
              word=getStringResource(topShell, "wrongMoloch");
              messgb(topShell, 0, word, buttons, 1);
            }
            else
              strcpy(moloch, line);
            checkProgram(line, raytracer);
            if (line[0] == '\0')
            {
              if (raytracer[0] != '\0')
              {
                word=getStringResource(topShell, "noFile");
                sprintf(line, word, raytracer);
              }
              else
              {
                word=getStringResource(topShell, "noRayshade");
                strcpy(line, word);
              }
              messgb(topShell, 0, line, buttons, 1);
            }
            else
              strcpy(raytracer, line);
            checkProgram(line, displayImage);
            if (line[0] == '\0')
            {
              if (displayImage[0] == '\0')
              {
                word=getStringResource(topShell, "noFile");
                sprintf(line, word, displayImage);
              }
              else
              {
                word=getStringResource(topShell, "noDisplay");
                strcpy(line, word);
              }
              messgb(topShell, 0, line, buttons, 1);
            }
            else
              strcpy(displayImage, line);
            break;
    case 2: saveConfiguration();
            break;
    case 3: data.set=TRUE;
            SetLanguage((Widget)0, (caddr_t)language_save, &data);
            strcpy(language, language_save);
            break;
  }
  XtDestroyWidget(dialog);
  if (radiobox_buttons != NULL) fremem((void **)&radiobox_buttons);
}

void GetString(Widget w, caddr_t text, XmAnyCallbackStruct *data)
{
  char *str;

  str=XmTextGetString(w);
  strcpy((char *)text, str);
}

#if defined LINUX || defined DARWIN
int matchXdefaults(const struct dirent *d)
#else
int matchXdefaults(struct dirent *d)
#endif
{
/*return(!fnmatch("Xdefaults.*", d->d_name, 0));*/
  return(fnmatch(".*", d->d_name, 0));
}
