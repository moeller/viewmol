/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                            B A S I S F O R M . C                             *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: basisform.c,v 1.6 2003/11/07 10:57:42 jrh Exp $
* $Log: basisform.c,v $
* Revision 1.6  2003/11/07 10:57:42  jrh
* Release 2.4
*
* Revision 1.5  2000/12/10 15:01:42  jrh
* Release 2.3
*
* Revision 1.4  1999/05/24 01:24:33  jrh
* Release 2.2.1
*
* Revision 1.3  1999/02/07 21:44:05  jrh
* Release 2.2
*
* Revision 1.2  1998/01/26 00:46:57  jrh
* Release 2.1
*
* Revision 1.1  1996/12/10  18:39:41  jrh
* Initial revision
*
*/
#include<stdlib.h>
#include<X11/Intrinsic.h>
#include<Xm/Xm.h>
#include<Xm/Form.h>
#include<Xm/Label.h>
#include<Xm/LabelG.h>
#include<Xm/PanedW.h>
#include<Xm/PushB.h>
#include<Xm/PushBG.h>
#include<Xm/RowColumn.h>
#include<Xm/Scale.h>
#include<Xm/Separator.h>
#include<Xm/Text.h>
#include<Xm/ToggleB.h>
#include<Xm/VendorS.h>
#include "tiffio.h"
#include "viewmol.h"
#include "dialog.h"

void basisDialog(int);
void basisDialogExit(Widget, caddr_t, XmPushButtonCallbackStruct *);
void getBasisfunction(Widget, caddr_t, XmToggleButtonCallbackStruct *);

extern void MapBox(Widget, caddr_t, XmAnyCallbackStruct *);
extern Widget CreateToggleBox(Widget, struct PushButtonRow *, int, int,
                              int, int, int);
extern char *getStringResource(Widget, char *);
/* extern char *bfname(int);
extern void bfnamn(int, int *, int*);*/
extern void *getmem(size_t, size_t);
extern void CreatePushButtonRow(Widget, struct PushButtonRow *, int);
extern void calcmo(void);
extern void redraw(int);
extern void deleteAnnotation(int *);
extern void annotateWavefunction(void);
extern Widget initShell(Widget, char *, Widget *, Widget *);

extern struct WINDOW windows[];
extern struct MOLECULE *molecules;
extern Widget topShell;
extern int iwavef, selectAtom, selectAtomAnnotation;
static Widget dialog;

void basisDialog(int bfAtom)
{
  struct MOLECULE *mol;
  Widget board, form, form1, atomname, sep;
  Widget radiobox;
  XmString name;
  static struct PushButtonRow buttons[] = {
    { "ok", basisDialogExit, (XtPointer)TRUE, NULL },
    { "cancel", basisDialogExit, (XtPointer)FALSE, NULL }
  };
  struct PushButtonRow *radiobox_buttons;
/*int start, end;*/
  char *label, *word, aname[MAXLENLINE];
  register int i, j;

  /* This function creates the dialog for selecting a basis function */

  selectAtom=FALSE;
  deleteAnnotation(&selectAtomAnnotation);

  dialog=initShell(windows[VIEWER].widget, "basisForm", &board,
			 &form);
  atomname=XtVaCreateManagedWidget("atomname", xmLabelWidgetClass, form,
                                   XmNleftAttachment, XmATTACH_FORM,
                                   XmNrightAttachment, XmATTACH_FORM,
                                   XmNtopAttachment, XmATTACH_FORM,
                                   NULL);
  if (windows[VIEWER].set >= 0)
    mol=&molecules[windows[VIEWER].set];
  else
    mol=&molecules[0];
  word=getStringResource(atomname, "labelString");
  sprintf(aname, word, mol->atoms[bfAtom].name, bfAtom+1);
  name=XmStringCreateSimple(aname);
  XtVaSetValues(atomname, XmNlabelString, name, NULL);
  XmStringFree(name);
  form1=XtVaCreateWidget("controlarea", xmRowColumnWidgetClass, form,
                         XmNorientation, XmHORIZONTAL,
                         XmNleftAttachment, XmATTACH_FORM,
                         XmNrightAttachment, XmATTACH_FORM,
                         NULL);
/*bfnamn(bfAtom, &start, &end);
  radiobox_buttons=(struct PushButtonRow *)getmem(end-start+2, sizeof(struct PushButtonRow)); */
  radiobox_buttons=(struct PushButtonRow *)getmem(mol->atoms[bfAtom].nBasisFunctions+1, sizeof(struct PushButtonRow));
/*label=(char *)getmem(end-start+1, 19*sizeof(char));*/
  label=(char *)getmem(mol->atoms[bfAtom].nBasisFunctions, 19*sizeof(char));
  j=0;
/*for (i=start; i<=end; i++)*/
  for (i=0; i<mol->atoms[bfAtom].nBasisFunctions; i++)
  {
    if (iwavef == BASIS_IN_MO)
    {
/*    sprintf(&label[19*j], "%10.6f*%s", mol->orbitals[mol->imo].coeff[i], bfname(i));*/
      sprintf(&label[19*j], "%10.6f*%s", mol->orbitals[mol->imo].coeff[i], mol->atoms[bfAtom].basisFunctionNames[i]);
      radiobox_buttons[j].label=&(label[19*j]);
    }
    else
/*    radiobox_buttons[j].label=bfname(i);*/
      radiobox_buttons[j].label=mol->atoms[bfAtom].basisFunctionNames[i];
    radiobox_buttons[j].callback=getBasisfunction;
    radiobox_buttons[j].client_data=(XtPointer)i;
    radiobox_buttons[j++].widget=(Widget)NULL;
  }
  radiobox=CreateToggleBox(form1, radiobox_buttons, j, (int)XmVERTICAL, j/10+1,
                           True, 0);
/*mol->ibasfu=start;*/
  mol->ibasfu=0;
  sep=XtVaCreateManagedWidget("sep", xmSeparatorWidgetClass, form1,
                              XmNorientation, XmHORIZONTAL,
                              XmNtraversalOn, False,
                              XmNleftAttachment, XmATTACH_FORM,
                              XmNrightAttachment, XmATTACH_FORM,
                              XmNtopAttachment, XmATTACH_WIDGET,
                              XmNtopWidget, radiobox,
                              NULL);
  XtManageChild(form1);

  CreatePushButtonRow(form, buttons, 2);
  XtAddCallback(dialog, XmNpopupCallback, (XtCallbackProc)MapBox, (XmAnyCallbackStruct *)NULL);
  XtManageChild(form);
  XtManageChild(board);
}

void getBasisfunction(Widget widget, caddr_t which, XmToggleButtonCallbackStruct *data)
{
  if (data->set) molecules[windows[VIEWER].set].ibasfu=(int)which;
}

void basisDialogExit(Widget widget, caddr_t what, XmPushButtonCallbackStruct *data)
{
  XtDestroyWidget(dialog);
  if ((int)what)
  {
    calcmo();
    annotateWavefunction();
    redraw(VIEWER);
  }
  else
  {
    iwavef=ALL_OFF;
    molecules[windows[VIEWER].set].ibasfu=(-1);
  }
}
