/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                                X F O N T . C                                 *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: xfont.c,v 1.7 2004/08/29 15:02:57 jrh Exp $
* $Log: xfont.c,v $
* Revision 1.7  2004/08/29 15:02:57  jrh
* Release 2.4.1
*
* Revision 1.6  2003/11/07 11:17:38  jrh
* Release 2.4
*
* Revision 1.5  2000/12/10 15:19:30  jrh
* Release 2.3
*
* Revision 1.4  1999/05/24 01:28:08  jrh
* Release 2.2.1
*
* Revision 1.3  1999/02/07 22:00:25  jrh
* Release 2.2
*
* Revision 1.2  1998/01/26 00:49:52  jrh
* Release 2.1
*
* Revision 1.1  1996/12/10  18:44:32  jrh
* Initial revision
*
*/
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>
#include "viewmol.h"

extern void *getmem(size_t, size_t);
extern void fremem(void **);

extern int drawingDevice;
extern char *textPointer;

XFontStruct *makeRasterFont(Display *display, char *font, GLuint *base)
{
  XFontStruct *fontInfo, *f;
  Font id;
  unsigned int first, last;

  /* leaks memory */
  if ((fontInfo=XLoadQueryFont(display, font)) == NULL)
    return(fontInfo);

  id=fontInfo->fid;
  first=fontInfo->min_char_or_byte2;
  last=fontInfo->max_char_or_byte2;

  if ((*base=glGenLists((GLuint)last+1)) == 0)
  {
    XFreeFont(display, fontInfo);
    return((XFontStruct *)NULL);
  }

  /* leaks memory */
  f=(XFontStruct *)getmem((size_t)1, sizeof(XFontStruct));
  memcpy((void *)f, (void *)fontInfo, sizeof(XFontStruct));
  glXUseXFont(id, first, last-first+1, *base+first);
  return(f);
}

void printString(char *s, double x, double y, double z, double width,
                 GLuint base, char align)
{
  double xaligned=0.0;

  switch (align)
  {
    case 'l': xaligned=x;
              break;
    case 'r': xaligned=x-width;
              break;
    case 'c': xaligned=x-0.5*width;
              break;
  }
  glRasterPos3d(xaligned, y, z);
  glListBase(base);
  glCallLists(strlen(s), GL_UNSIGNED_BYTE, (GLubyte *)s);
}

int StringWidth(XFontStruct *font, char *string)
{
  if (font)
    return(XTextWidth(font, string, strlen(string)));
  else
    return(0);
}

int StringHeight(XFontStruct *font)
{
  if (font)
    return(font->ascent+font->descent);
  else
    return(0);
}

int StringAscent(XFontStruct *font)
{
  if (font)
    return(font->ascent);
  else
    return(0);
}

void deleteFontList(GLuint base, XFontStruct *font)
{
  if (font)
  {
    glDeleteLists(base, font->max_char_or_byte2-font->min_char_or_byte2+1);
    fremem((void **)&font);
  }
}
