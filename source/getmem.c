/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                               G E T M E M . C                                *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: getmem.c,v 1.6 2003/11/07 11:02:07 jrh Exp $
* $Log: getmem.c,v $
* Revision 1.6  2003/11/07 11:02:07  jrh
* Release 2.4
*
* Revision 1.5  2000/12/10 15:07:30  jrh
* Release 2.3
*
* Revision 1.4  1999/05/24 01:25:40  jrh
* Release 2.2.1
*
* Revision 1.3  1999/02/07 21:49:25  jrh
* Release 2.2
*
* Revision 1.2  1998/01/26 00:47:48  jrh
* Release 2.1
*
* Revision 1.1  1996/12/10  18:40:58  jrh
* Initial revision
*
*/
#include <stdio.h>
#include <stdlib.h>

void *getmem(size_t number, size_t size)
{
  void *pointer;

  pointer=calloc(number, size);
  if (pointer == NULL)
  {
    fprintf(stderr, "ERROR: Unable to allocate memory: ");
    perror(NULL);
    exit(1);
  }
  return(pointer);
}

void fremem(void **pointer)
{
  free(*pointer);
  *pointer=NULL;
}

void *expmem(void *pointer, size_t number, size_t size)
{
  pointer=realloc(pointer, number*size);
  if (pointer == NULL)
  {
    fprintf(stderr, "ERROR: Unable to change size of allocated memory: ");
    perror(NULL);
    exit(1);
  }
  return(pointer);
}
