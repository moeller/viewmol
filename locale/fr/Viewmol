!*******************************************************************************
!                                                                              *
!                                   Viewmol                                    *
!                                                                              *
!                   X D E F A U L T S . F R E N C H                            *
!                                                                              *
!        Copyright (c) Ludovic Douillard, Joerg-R. Hill, December 2000         *
!                                                                              *
!*******************************************************************************
!
! $Id: Viewmol,v 1.4 2003/11/08 15:18:05 jrh Exp $
! $Log: Viewmol,v $
! Revision 1.4  2003/11/08 15:18:05  jrh
! Release 2.4
!
! Revision 1.3  2000/12/10 14:58:42  jrh
! Release 2.3
!
! Revision 1.2  1999/05/24 01:24:24  jrh
! Release 2.2.1
!
! Revision 1.1  1999/02/07 21:43:15  jrh
! Initial revision
!
!
! Translation kindly provided by Ludovic Douillard <douillard@DRECAM.cea.fr>.
!
! Resources which have to be adapted for the installation of external programmes
!
Viewmol.webBrowser:                              mozilla %s
Viewmol.Moloch:                                  moloch
Viewmol.Raytracer:                               x-povray +I%s +O%s +W%d +H%d
Viewmol.DisplayImage:                            xv %s
!
! Resources which determine defaults
!
Viewmol.geometry:                                500x500+50+50
Viewmol.history.geometry:                        500x250+50+590
Viewmol.spectrum.geometry:                       500x250+50+590
Viewmol.MODiagram.geometry:                      250x500+565+50
Viewmol.model:                                   wire
Viewmol.drawingMode:                             surface
Viewmol.bondType:                                conjugated
Viewmol.sphereResolution:                        20
Viewmol.lineWidth:                               0
Viewmol.simplifyWhileRotating:                   True
Viewmol.interpolation:                           linear
Viewmol.bondLength:                              %7.4f Ang
Viewmol.bondAngle:                               %7.2f deg
Viewmol.torsionAngle:                            %7.2f deg
Viewmol.wavenumbers:                             0:5000
Viewmol.isosurface:                              0.05
Viewmol.densityResolution:                       0.01
Viewmol.reservedColors:                          0
Viewmol.hydrogenBondThreshold:                   2.0
Viewmol.automaticRecalculation:                  False
Viewmol.thermoUnits:                             joules
Viewmol*spectrumForm*amplitudeSlider.decimalPoints: 2
Viewmol*spectrumForm*amplitudeSlider.minimum:    -250
Viewmol*spectrumForm*amplitudeSlider.maximum:    250
Viewmol*spectrumForm*scaleSlider.decimalPoints:  2
Viewmol*spectrumForm*scaleSlider.minimum:        50
Viewmol*spectrumForm*scaleSlider.maximum:        150
Viewmol*thermoForm*pressureSlider.decimalPoints: 2
Viewmol*thermoForm*pressureSlider.minimum:       1
Viewmol*thermoForm*pressureSlider.maximum:       1000
Viewmol*wavefunctionForm*level.decimalPoints:    2
Viewmol*wavefunctionForm*level.minimum:          1
Viewmol*wavefunctionForm*level.maximum:          100
Viewmol*wavefunctionForm*grid.minimum:           4
Viewmol*wavefunctionForm*grid.maximum:           40
Viewmol*wavefunctionForm*grid.value:             20
Viewmol*MODiagramForm*resolution.minimum:        1
Viewmol*MODiagramForm*resolution.maximum:        1000
Viewmol*MODiagramForm*resolution.decimalPoints:  3
Viewmol*unitcellForm*avalue.minimum:             10
Viewmol*unitcellForm*avalue.maximum:             50
Viewmol*unitcellForm*avalue.decimalPoints:       1
Viewmol*unitcellForm*bvalue.minimum:             10
Viewmol*unitcellForm*bvalue.maximum:             50
Viewmol*unitcellForm*bvalue.decimalPoints:       1
Viewmol*unitcellForm*cvalue.minimum:             10
Viewmol*unitcellForm*cvalue.maximum:             50
Viewmol*unitcellForm*cvalue.decimalPoints:       1
Viewmol*unitcellForm*hvalue.minimum:             -5
Viewmol*unitcellForm*hvalue.maximum:             5
Viewmol*unitcellForm*kvalue.minimum:             -5
Viewmol*unitcellForm*kvalue.maximum:             5
Viewmol*unitcellForm*lvalue.minimum:             -5
Viewmol*unitcellForm*lvalue.maximum:             5 
Viewmol*bondForm*thresholdSlider.minimum:        100
Viewmol*bondForm*thresholdSlider.maximum:        250
Viewmol*bondForm*thresholdSlider.decimalPoints:  2
Viewmol*bondForm*scaleRadius.minimum:            1
Viewmol*bondForm*scaleRadius.maximum:            200
Viewmol*bondForm*scaleRadius.decimalPoints:      2 
Viewmol*infoForm*text*rows:                      6
Viewmol*infoForm*text*columns:                   80
Viewmol*annotation.highlightThickness:           0
Viewmol.paperSize:                               A4
Viewmol.elementSortOrder:                        C,H,O,N,S
Viewmol.viewer.font:                             -adobe-helvetica-bold-r-normal--12-120-75-75-P-70-iso8859-1
Viewmol.spectrum.spectrum.font:                  -adobe-helvetica-bold-r-normal--12-120-75-75-P-70-iso8859-1
Viewmol.history.history.font:                    -adobe-helvetica-bold-r-normal--12-120-75-75-P-70-iso8859-1
Viewmol.MODiagram.MODiagram.font:                -adobe-helvetica-bold-r-normal--12-120-75-75-P-70-iso8859-1
Viewmol.viewer.background:                       white
Viewmol.viewer.foreground:                       gray75
Viewmol.spectrum.spectrum.background:            white
Viewmol.spectrum.spectrum.foreground:            black
Viewmol.history.history.background:              white
Viewmol.history.history.foreground:              blue
Viewmol.MODiagram.MODiagram.background:          white
Viewmol.MODiagram.MODiagram.foreground:          black
Viewmol*foreground:                              black
!
! Resources which provide the Indigo Magic Desktop look-and-feel
! on SGIs
!
Viewmol*sgiMode:                                 True
Viewmol*useSchemes:                              all
Viewmol*SgNuseEnhancedFSB:                       True
!
! Resources which have to be changed for the translation of Viewmol into
! another language
!
Viewmol.language:                                fr
Viewmol.title:                                   Viewmol
Viewmol.by:                                      par
Viewmol.version:                                 Version
Viewmol.history.title:                           Historique d'optimisation (%s)
Viewmol.spectrum.title:                          Spectre (%s)
Viewmol.spectrum.title1:                         Tous les modes (%s)
Viewmol.spectrum.title2:                         Spectre IR (%s)
Viewmol.spectrum.title3:                         Spectre Raman (%s)
Viewmol.spectrum.title4:                         Spectre INS (%s)
Viewmol.MODiagram.title:                         Niveaux d'�nergie (%s)
Viewmol*_popup.molecule.labelString:             Mol�cule
Viewmol*loadMolecule.labelString:                Charger une mol�cule ...
Viewmol*saveMolecule.labelString:                Sauvegarder mol�cule ...
Viewmol*saveSelected.labelString:                Sauvegarder les atomes s�lectionn�s ...
Viewmol*deleteMolecule.labelString:              Supprimer une mol�cule
Viewmol*newMolecule.labelString:                 Nouvelle mol�cule ...
Viewmol*buildMolecule.labelString:               Modifier une mol�cule ...
Viewmol*_popup.wire_model.labelString:           Mod�le fils de fer
Viewmol*_popup.wire_model.mnemonic:              W
Viewmol*_popup.wire_model.accelerator:           Meta<Key>W
Viewmol*_popup.stick_model.labelString:          Mod�le batons
Viewmol*_popup.stick_model.mnemonic:             t
Viewmol*_popup.stick_model.accelerator:          Meta<Key>T
Viewmol*_popup.ball_and_stick_model.labelString: Mod�le sph�res et batons
Viewmol*_popup.ball_and_stick_model.mnemonic:    a
Viewmol*_popup.ball_and_stick_model.accelerator: Meta<Key>A
Viewmol*_popup.cpk_model.labelString:            Mod�le CPK
Viewmol*_popup.cpk_model.mnemonic:               C
Viewmol*_popup.cpk_model.accelerator:            Meta<Key>C
Viewmol*pseForm_popup*title:                     Modifier une mol�cule
Viewmol*change.labelString:                      Changer la g�om�trie
Viewmol*add.labelString:                         Ajouter un atome
Viewmol*delete.labelString:                      Supprimer un atome
Viewmol*replace.labelString:                     Remplacer un atome
Viewmol*create.labelString:                      Cr�er une liaison
Viewmol*remove.labelString:                      Supprimer une liaison
Viewmol*order.labelString:                       Modifier l'ordre de liaison
Viewmol*torsionDefault.labelString:              Angles de torsion par d�faut
Viewmol*trans.labelString:                       trans
Viewmol*cis.labelString:                         cis
Viewmol*gauche.labelString:                      gauche
Viewmol*-gauche.labelString:                     -gauche
Viewmol*bondOrderLabel.labelString:              Les liaisons sont
Viewmol*pseForm_popup*fractional.labelString:    Van der Waals
Viewmol*pseForm_popup*single.labelString:        simples
Viewmol*pseForm_popup*double.labelString:        doubles
Viewmol*pseForm_popup*triple.labelString:        triples
Viewmol*localGeometry.labelString:               Supprimer les atomes modifie la g�om�trie locale
Viewmol*_popup.geometry_menu.labelString:        G�om�trie ...
Viewmol*clear_all.labelString:                   Tout effacer
Viewmol*clear_all.mnemonic:                      a
Viewmol*clear_all.accelerator:                   Meta<Key>A
Viewmol*clear_last.labelString:                  Effacer derni�re op�ration
Viewmol*undo.labelString:                        Annuler la modification de g�om�trie
Viewmol*undo.accelerator:                        Ctrl<Key>U
Viewmol*undo.acceleratorText:                    Ctrl+U
Viewmol*bondForm_popup.title:                    Liaisons
Viewmol*_popup.bondType_menu.labelString:        Liaisons ...
Viewmol*bondForm*single.labelString:             Liaisons simples uniquement
Viewmol*bondForm*multiple.labelString:           Liaisons multiples
Viewmol*bondForm*conjugated.labelString:         Liaisons conjug�es
Viewmol*bondForm*select.labelString:             Multiplier les rayons
Viewmol*bondForm*all.labelString:                de tous les atomes
Viewmol*bondForm*atoms.labelString:              par
Viewmol*showHydrogenBonds.labelString:           Afficher les liaisons hydrog�ne
Viewmol*HydrogenBondLabel.labelString:           Seuil pour les liaisons hydrog�ne [�]
Viewmol*_popup.wave_function.labelString:        Fonction d'onde ...
Viewmol*_popup.wave_function.mnemonic:           v
Viewmol*_popup.wave_function.accelerator:        Meta<Key>V
Viewmol*_popup.energy_level_diagram.labelString: Niveaux d'�nergie
Viewmol*_popup.energy_level_diagram.mnemonic:    E
Viewmol*_popup.energy_level_diagram.accelerator: Meta<Key>E
Viewmol*_popup.optimization_history.labelString: Historique d'optimisation
Viewmol*_popup.optimization_history.mnemonic:    O
Viewmol*_popup.optimization_history.accelerator: Meta<Key>O
Viewmol*_popup.show_forces.labelString:          Afficher les forces
Viewmol*_popup.show_forces.mnemonic:             f
Viewmol*_popup.show_forces.accelerator:          Meta<Key>F
Viewmol*_popup.spectrum.labelString:             Spectre
Viewmol*_popup.spectrum.mnemonic:                S
Viewmol*_popup.spectrum.accelerator:             Meta<Key>S
Viewmol*_popup.thermodynamics.labelString:       Thermodynamique
Viewmol*_popup.thermodynamics.mnemonic:          y
Viewmol*_popup.thermodynamics.accelerator:       Meta<Key>Y
Viewmol*_popup.show_unit_cell.labelString:       Afficher la maille
Viewmol*_popup.show_unit_cell.mnemonic:          n
Viewmol*_popup.show_unit_cell.accelerator:       Meta<Key>N
Viewmol*_popup.show_ellipsoid_of_inertia.labelString: Afficher les ellipsoides d'inertie
Viewmol*_popup.show_ellipsoid_of_inertia.mnemonic: i
Viewmol*_popup.show_ellipsoid_of_inertia.accelerator: Meta<Key>I
Viewmol*_popup.drawing_modes.labelString:        Modes d'affichage ...
Viewmol*_popup.drawing_modes.mnemonic:           m
Viewmol*_popup.drawing_modes.accelerator:        Meta<Key>M
Viewmol*_popup.background_color.labelString:     Couleur arri�re-plan ...
Viewmol*_popup.background_color.mnemonic:        B
Viewmol*_popup.background_color.accelerator:     Meta<Key>B
Viewmol*_popup.foreground_color.labelString:     Couleur d'affichage ...
Viewmol*_popup.foreground_color.mnemonic:        G
Viewmol*_popup.foreground_color.accelerator:     Meta<Key>G
Viewmol*_popup.label_atoms.labelString:          Noms des atomes
Viewmol*_popup.label_atoms.mnemonic:             L
Viewmol*_popup.label_atoms.accelerator:          Meta<Key>L
Viewmol*_popup.annotate.labelString:             Annoter
Viewmol*_popup.annotate.accelerator:             Ctrl<Key>N
Viewmol*_popup.annotate.acceleratorText:         Ctrl+N
Viewmol*_popup.runScript.labelString:            Ex�cuter le scripte
Viewmol*select.labelString:                      S�lectionner ...
Viewmol*select.accelerator:                      Ctrl<Key>R
Viewmol*select.acceleratorText:                  Ctrl+R
Viewmol*_popup.hardcopy.labelString:             Sauvegarder dessin ...
Viewmol*_popup.hardcopy.mnemonic:                d
Viewmol*_popup.hardcopy.accelerator:             Meta<Key>D
Viewmol*_popup.raytracing.labelString:           Ray tracing
Viewmol*_popup.raytracing.mnemonic:              R
Viewmol*_popup.raytracing.accelerator:           Meta<Key>R
Viewmol*_popup.manual.labelString:               Aide/Manuel
Viewmol*_popup.manual.mnemonic:                  H
Viewmol*_popup.manual.accelerator:               Meta<Key>H
Viewmol*_popup.saveConfiguration.labelString:    Configuration ...
Viewmol*_popup.quit.labelString:                 Sortie
Viewmol*_popup.quit.mnemonic:                    Q
Viewmol*_popup.quit.accelerator:                 Meta<Key>Q
Viewmol*_popup.select_molecule.labelString:      S�lectionner une mol�cule
Viewmol*all.labelString:                         Tout
Viewmol*spectrumForm_popup.title:                Param�tres spectraux
Viewmol.spectrum*_popup.settings_spectrum.labelString: Param�tres spectraux ...
Viewmol.spectrum*_popup.settings_spectrum.mnemonic: S
Viewmol.spectrum*_popup.settings_spectrum.accelerator: Meta<Key>S
Viewmol.spectrum*_popup.observed_spectrum.labelString: Lecture du spectre ...
Viewmol.spectrum*_popup.observed_spectrum.mnemonic: R
Viewmol.spectrum*_popup.observed_spectrum.accelerator: Meta<Key>R
Viewmol.spectrum*_popup.delete_spectrum.labelString: D�truire le spectre
Viewmol.spectrum*_popup.delete_spectrum.mnemonic: e
Viewmol.spectrum*_popup.delete_spectrum.accelerator: Meta<Key>E
Viewmol.spectrum*_popup.imaginary_wave_numbers.labelString: Nombres d'ondes imaginaires ...
Viewmol.spectrum*_popup.zoom_out.labelString:    Zoom arri�re
Viewmol.spectrum*_popup.zoom_out.mnemonic:       Z
Viewmol.spectrum*_popup.zoom_out.accelerator:    Meta<Key>Z
Viewmol.spectrum*_popup.hardcopy.labelString:    Sauvegarder dessin ...
Viewmol.spectrum*_popup.hardcopy.mnemonic:       d
Viewmol.spectrum*_popup.hardcopy.accelerator:    Meta<Key>D
Viewmol.spectrum*_popup.foreground_color.labelString: Couleur premier-plan ...
Viewmol.spectrum*_popup.foreground_color.mnemonic: F
Viewmol.spectrum*_popup.foreground_color.accelerator: Meta<Key>F
Viewmol.spectrum*_popup.background_color.labelString: Couleur arri�re-plan ...
Viewmol.spectrum*_popup.background_color.mnemonic: B
Viewmol.spectrum*_popup.background_color.accelerator: Meta<Key>B
Viewmol.spectrum*_popup.quit_spectrum.labelString: Quitter spectre
Viewmol.spectrum*_popup.quit_spectrum.mnemonic:  Q
Viewmol.spectrum*_popup.quit_spectrum.accelerator: Meta<Key>Q
Viewmol*thermoForm_popup.title:                  Thermodynamique
Viewmol*thermoForm*molecules.labelString:        Mol�cules
Viewmol*thermoForm*reactions.labelString:        R�actions
Viewmol*thermoForm*moleculeMass:                 Masse molaire %.2f g/mol
Viewmol*thermoForm*solidDensity:                 Densit� %.2f g/cm^3
Viewmol*thermoForm*symmetryNumber:               Nombre de sym�trie %d
Viewmol*thermoForm*rotationalConstants:          Constantes rotationnelles %.3f %.3f %.3f 1/cm
Viewmol*joules:                                  J
Viewmol*calories:                                cal
Viewmol*format:                                  %.3f
Viewmol*thermoForm*enthalphy.labelString:        H/[k%s/mol]
Viewmol*thermoForm*entropy.labelString:          S/[%s/(mol K)]
Viewmol*thermoForm*gibbsEnergy.labelString:      G/[k%s/mol]
Viewmol*thermoForm*heatCapacity.labelString:     C_v/[%s/(mol K)]
Viewmol*thermoForm*reactant.labelString:         R�actif
Viewmol*thermoForm*notInvolved.labelString:      Non impliqu� dans la r�action
Viewmol*thermoForm*product.labelString:          Produit
Viewmol*thermoForm*allReactions.labelString:     Toutes les r�actions
Viewmol*thermoForm*noReaction:                   Aucune r�action d�finie.
Viewmol*thermoForm*missingAtoms:                 (Aucune r�action de ce type possible)
Viewmol*thermoForm*cantBalance:                  (Impossible d'�quilibrer la r�action)
Viewmol*thermoForm*inconsistentType:             \"R�actif/Produit\" et \"Toutes r�actions\" sont incompatibles.
Viewmol*thermoForm*translation.labelString:      Translation
Viewmol*thermoForm*pv.labelString:               pV
Viewmol*thermoForm*rotation.labelString:         Rotation
Viewmol*thermoForm*vibration.labelString:        Vibration
Viewmol*thermoForm*total.labelString:            Total
Viewmol*thermoForm*electronicEnergy.labelString: Energie �lectronique de r�action
Viewmol*thermoForm*statisticalEnergy.labelString: Energie statistique de r�action
Viewmol*thermoForm*reactionEnergy.labelString:   Energie totale de r�action
Viewmol*thermoForm*reactionEntropy.labelString:  Entropie de r�action
Viewmol*thermoForm*reactionGibbsEnergy.labelString: Energie de Gibbs de r�action (enthalpie libre)
Viewmol*thermoForm*reactionHeatCapacity.labelString: Chaleur sp�cifique de r�action
Viewmol*thermoForm*equilibriumConstant.labelString: Logarithme de la constante d'�quilibre (log K)
Viewmol*thermoForm*previous.labelString:         R�action pr�c�dente
Viewmol*thermoForm*next.labelString:             R�action suivante
Viewmol*kiloperMole:                             % 15.2f k%s/mol
Viewmol*perMoleandK:                             % 15.2f %s/(mol K)
Viewmol*noUnit:                                  % 15.2f
Viewmol*thermoForm*unitlabel.labelString:        Utilise
Viewmol*thermoForm*joules.labelString:           Joules
Viewmol*thermoForm*calories.labelString:         Calories
Viewmol*thermoForm*thermocalories.labelString:   Calories thermochimiques
Viewmol*thermoForm*temperature.labelString:      Temp�rature
Viewmol*thermoForm*pressure.labelString:         Pression/[atm]
Viewmol*balanceForm_popup.title:                 Equilibrer la r�action manuellement
Viewmol*historyForm_popup.title:                 Param�tres de l'historique
Viewmol.history*_popup.settings_history.labelString: Param�tres de l'historique ...
Viewmol.history*_popup.settings_history.mnemonic: S
Viewmol.history*_popup.settings_history.accelerator: Meta<Key>S
Viewmol.history*_popup.animate_history.labelString: Animer
Viewmol.history*_popup.animate_history.mnemonic:    A
Viewmol.history*_popup.animate_history.accelerator: Meta<Key>A
Viewmol.history*_popup.hardcopy.labelString:     Sauvegarder dessin ...
Viewmol.history*_popup.hardcopy.mnemonic:        d
Viewmol.history*_popup.hardcopy.accelerator:     Meta<Key>D
Viewmol.history*_popup.energy_color.labelString: S�lection de la couleur pour �nergie ...
Viewmol.history*_popup.energy_color.mnemonic:    e
Viewmol.history*_popup.energy_color.accelerator: Meta<Key>E
Viewmol.history*_popup.gradient_color.labelString: S�lection de la couleur pour gradient ...
Viewmol.history*_popup.gradient_color.mnemonic:  g
Viewmol.history*_popup.gradient_color.accelerator: Meta<Key>G
Viewmol.history*_popup.background_color.labelString: Couleur d'arri�re-plan ...
Viewmol.history*_popup.background_color.mnemonic: B
Viewmol.history*_popup.background_color.accelerator: Meta<Key>B
Viewmol.history*_popup.quit_history.labelString: Quitter historique d'optimization
Viewmol.history*_popup.quit_history.mnemonic:    Q
Viewmol.history*_popup.quit_history.accelerator: Meta<Key>Q
Viewmol.MODiagram*_popup.settings_modiagram.labelString: Param�tres pour diagramme d'�nergies ...
Viewmol.MODiagram*_popup.settings_modiagram.mnemonic: S
Viewmol.MODiagram*_popup.settings_modiagram.accelerator: Meta<Key>S
Viewmol.MODiagram*_popup.transition.labelString: Transition
Viewmol.MODiagram*_popup.transition.mnemonic:    T
Viewmol.MODiagram*_popup.transition.accelerator: Meta<Key>T
Viewmol.MODiagram*_popup.zoom_out.labelString:   Zoom arri�re
Viewmol.MODiagram*_popup.zoom_out.mnemonic:      Z
Viewmol.MODiagram*_popup.zoom_out.accelerator:   Meta<Key>Z
Viewmol.MODiagram*_popup.hardcopy.labelString:   Sauvegarder dessin ...
Viewmol.MODiagram*_popup.hardcopy.mnemonic:      d
Viewmol.MODiagram*_popup.hardcopy.accelerator:   Meta<Key>D
Viewmol.MODiagram*_popup.energy_levels.labelString: Afficher densit� d'�tats
Viewmol.MODiagram*_popup.energy_levels.mnemonic: e
Viewmol.MODiagram*_popup.energy_levels.accelerator: Meta<Key>E
Viewmol.MODiagram*_popup.foreground_color.labelString: Couleur premier-plan ...
Viewmol.MODiagram*_popup.foreground_color.mnemonic: F
Viewmol.MODiagram*_popup.foreground_color.accelerator: Meta<Key>F
Viewmol.MODiagram*_popup.background_color.labelString: Couleur arri�re-plan ...
Viewmol.MODiagram*_popup.background_color.mnemonic: B
Viewmol.MODiagram*_popup.background_color.accelerator: Meta<Key>B
Viewmol.MODiagram*_popup.quit_modiagram.labelString: Quitter diagramme d'�nergies
Viewmol.MODiagram*_popup.quit_modiagram.mnemonic: Q
Viewmol.MODiagram*_popup.quit_modiagram.accelerator: Meta<Key>Q
Viewmol*messageForm_popup*exit.labelString:      Sortie
Viewmol*messageForm_popup*title:                 Note
Viewmol*infoForm_popup.title:                    Python
Viewmol*basisForm_popup.title:                   Base d'orbitales atomiques
Viewmol*basisForm_popup.basisForm.rowcolumn.atomname.labelString: Base d'orbitales atomiques pour l'atome %s%d
Viewmol.fileSelectionBox_popup.title:            S�lection fichier
Viewmol*fileSelectionBox.dirListLabelString:     R�pertoires
Viewmol*fileSelectionBox.fileListLabelString:    Fichiers
Viewmol*fileSelectionBox.filterLabelString:      Chemin
Viewmol*fileSelectionBox.applyLabelString:       Filtre
Viewmol*fileSelectionBox.okLabelString:          Oui
Viewmol*fileSelectionBox.cancelLabelString:      Annuler
Viewmol*fileSelectionBox.selectionLabelString:   S�lection
Viewmol*ok.labelString:                          Oui
Viewmol*cancel.labelString:                      Annuler
Viewmol*continue.labelString:                    Continuer
Viewmol*save.labelString:                        Sauvegarder
Viewmol*optimizationForm_popup*title:            Historique d'optimisation
Viewmol*optimizationForm*energies.labelString:   Energie
Viewmol*optimizationForm*norms.labelString:      Norme du gradient
Viewmol*optimizationForm*scales.labelString:     Echelles
Viewmol*spectrumForm*all_modes.labelString:      Tous les modes
Viewmol*spectrumForm*ir_modes.labelString:       Modes IR actifs
Viewmol*spectrumForm*raman_modes.labelString:    Modes Raman actifs
Viewmol*spectrumForm*ins_modes.labelString:      Diffusion in�lastique de neutrons
Viewmol*spectrumForm*animate.labelString:        Animer
Viewmol*spectrumForm*draw_arrows.labelString:    Dessiner fl�ches
Viewmol*spectrumForm*distort.labelString:        Distordre
Viewmol*spectrumForm*line_spectrum.labelString:  Spectre mode ligne
Viewmol*spectrumForm*gaussian_spectrum.labelString: Spectre mode gaussienne
Viewmol*spectrumForm*setins.labelString:         Saisie des poids pour la diffusion in�lastique de neutrons
Viewmol*spectrumForm*axisTop.labelString:        Nombres d'ondes affich�s au sommet 
Viewmol*spectrumForm*showGrid.labelString:       Afficher la grille
Viewmol*spectrumForm*lineWidthLabel.labelString: Epaisseur de trait
Viewmol*spectrumForm*temperature.labelString:    Temp�rature
Viewmol*spectrumForm*amplitude.labelString:      Amplitude
Viewmol*spectrumForm*scale.labelString:          Echelle\nnombre d'ondes
Viewmol*wavefunctionForm_popup.title:            Fonction d'onde
Viewmol*wavefunctionForm*all_off.labelString:    Aucun
Viewmol*wavefunctionForm*basis_function.labelString: Base d'orbitales atomiques
Viewmol*wavefunctionForm*basis_in_mo.labelString: Base d'orbitales atomiques pour OM
Viewmol*wavefunctionForm*molecular_orbital.labelString: Orbitale Mol�culaire (OM)
Viewmol*wavefunctionForm*electron_density.labelString: Densit� �lectronique
Viewmol*wavefunctionForm*interpolationLabel.labelString: Interpolation
Viewmol*wavefunctionForm*none.labelString:       Aucune
Viewmol*wavefunctionForm*linear.labelString:     Lin�aire
Viewmol*wavefunctionForm*logarithmic.labelString: Logarithmique
Viewmol*wavefunctionForm*levelLabel.labelString: Isosurface
Viewmol*wavefunctionForm*gridLabel.labelString:  R�solution de la grille
Viewmol*wavefunctionForm*automatic.labelString:  Recalcul automatique
Viewmol*MODiagramForm_popup.title:               Param�tres
Viewmol*MODiagramForm*hartrees.labelString:      Hartrees
Viewmol*MODiagramForm*kj_mol.labelString:        kJ/mol
Viewmol*MODiagramForm*ev.labelString:            eV
Viewmol*MODiagramForm*cm.labelString:            cm^-1
Viewmol*MODiagramForm*resolutionlabel.labelString: R�solution
Viewmol*printForm_popup.title:                   Sauvegarder dessin
Viewmol*printForm*hpgl.labelString:              HPGL
Viewmol*printForm*postscript.labelString:        PostScript
Viewmol*printForm*raytracer.labelString:         Povray
Viewmol*printForm*tiff.labelString:              TIFF
Viewmol*printForm*png.labelString:               PNG
Viewmol*printForm*landscape.labelString:         Paysage
Viewmol*printForm*portrait.labelString:          Portrait
Viewmol*printForm*papersize.labelString:         Format document
Viewmol*printForm*a5.labelString:                A5
Viewmol*printForm*a4.labelString:                A4
Viewmol*printForm*a3.labelString:                A3
Viewmol*printForm*letter.labelString:            Letter
Viewmol*printForm*legal.labelString:             Legal
Viewmol*printForm*userdefined.labelString:       D�fini par l'utilisateur
Viewmol*printForm*lzw.labelString:               LZW
Viewmol*printForm*mac.labelString:               Macintosh
Viewmol*printForm*none.labelString:              Aucune
Viewmol*printForm*compressionlabel.labelString:  Compression TIFF
Viewmol*printForm*transparent.labelString:       Arri�re-plan transparent
Viewmol*printForm*widthlabel.labelString:        Largeur de la page en mm
Viewmol*printForm*heightlabel.labelString:       Hauteur de la page en mm
Viewmol*printForm*file.labelString:              Fichier
Viewmol*printForm*select.labelString:            S�lectionner
Viewmol*drawingModeForm_popup.title:             Modes d'affichage
Viewmol*drawingModeForm*dots.labelString:        Points
Viewmol*drawingModeForm*lines.labelString:       Lignes
Viewmol*drawingModeForm*surfaces.labelString:    Surfaces
Viewmol*drawingModeForm*simplify.labelString:    Mode lignes durant rotation
Viewmol*drawingModeForm*projectionLabel.labelString: Projection
Viewmol*drawingModeForm*orthographic.labelString: Orthogonale
Viewmol*drawingModeForm*perspective.labelString: Perspective
Viewmol*drawingModeForm*onOffLabel.labelString:  Lumi�re oui/non
Viewmol*drawingModeForm*molecule.labelString:    D�placer mol�cule
Viewmol*drawingModeForm*viewpoint.labelString:   D�placer point de vue
Viewmol*drawingModeForm*light0.labelString:      D�placer source de lumi�re 1
Viewmol*drawingModeForm*light1.labelString:      D�placer source de lumi�re 2
Viewmol*drawingModeForm*light0OnOff.labelString: Source de lumi�re 1
Viewmol*drawingModeForm*light1OnOff.labelString: Source de lumi�re 2
Viewmol*drawingModeForm*sphereResolutionLabel.labelString: R�solution du trac� des sph�res
Viewmol*drawingModeForm*lineWidthLabel.labelString: Epaisseur du trait
Viewmol*colorEditor_popup.title:                 Editeur de couleurs
Viewmol*colorEditor*smoothRed.labelString:       Lissage
Viewmol*colorEditor*smoothGreen.labelString:     Lissage
Viewmol*colorEditor*smoothBlue.labelString:      Lissage
Viewmol*doRaytracing.labelString:                Commencer raytracing
Viewmol*stopRaytracing.labelString:              Ne pas commencer raytracing
Viewmol*saveMoleculeForm_popup*title:            Sauvegarder mol�cule
Viewmol*saveMoleculeForm*car.labelString:        Fichier MSI car
Viewmol*saveMoleculeForm*arc.labelString:        Fichier MSI arc
Viewmol*saveMoleculeForm*gauss.labelString:      Gaussian 98 input file
Viewmol*saveMoleculeForm*mol.labelString:        Fichier MDL mol
Viewmol*saveMoleculeForm*tm.labelString:         Fichier Turbomole
Viewmol*unitcellForm_popup.title:                Maille
Viewmol*unitcellForm*visible.labelString:        visible
Viewmol*unitcellForm*avalue.titleString:         a
Viewmol*unitcellForm*bvalue.titleString:         b
Viewmol*unitcellForm*cvalue.titleString:         c
Viewmol*unitcellForm*miller.labelString:         Afficher le plan de Miller
Viewmol*unitcellForm*hvalue.titleString:         h
Viewmol*unitcellForm*kvalue.titleString:         k
Viewmol*unitcellForm*lvalue.titleString:         l
Viewmol*configurationForm_popup*title:           Configuration
Viewmol*configurationForm*de.labelString:        Allemand
Viewmol*configurationForm*en_US.labelString:     Anglais
Viewmol*configurationForm*es.labelString:        Espagnol
Viewmol*configurationForm*fr.labelString:        Fran�ais
Viewmol*configurationForm*hu.labelString:        Hongrois
Viewmol*configurationForm*pl.labelString:        Polonais
Viewmol*configurationForm*ru.labelString:        Russe
Viewmol*configurationForm*tr.labelString:        Turc
Viewmol*configurationForm*browserLabel.labelString: Adresse du navigateur Web
Viewmol*configurationForm*molochLabel.labelString:  Adresse de Moloch
Viewmol*configurationForm*raytracerLabel.labelString: Adresse de Povray
Viewmol*configurationForm*displayImage:          Location of display program for images
Viewmol.unknownParameter:                        Param�tre(s) de ligne de commande inconnu(s): %s.
Viewmol.selectFormat:                            S�lectionnez un format svp
Viewmol.selectCompression:                       S�lectionnez la m�thode de compression svp.
Viewmol.TIFFSaved:                               Sauvegarder dessin au format TIFF fichier %s.
Viewmol.PNGSaved:                                Sauvegarder dessin au format PNG fichier %s.
Viewmol.HPGLSaved:                               Sauvegarder dessin au format HPGL fichier %s.
Viewmol.PostscriptSaved:                         Sauvegarder dessin au format Postscript fichier %s.
Viewmol.RaytracerSaved:                          Sauvegarder dessin au format Rayshade fichier %s.
Viewmol.MoleculeSaved:                           Sauvegarder mol�cule fichier %s.
Viewmol.ConfigurationSaved:                      Sauvegarder la configuration dans le fichier $HOME/.Xdefaults.
Viewmol.noControlFile:                           Fichier de controle absent du r�pertoire courant.
Viewmol.unableToOpen:                            Ouverture du fichier %s impossible.
Viewmol.molochFailed:                            Echec de Moloch.
Viewmol.noMolochOutput:                          Fichiers de sortie de Moloch inexistants.
Viewmol.errorOnLine:                             Erreur ligne %d de %s.
Viewmol.noColors:                                Impossible d'allouer suffisament de couleurs
Viewmol.noInputFilter:                           Aucun filtre d'importation sp�cifi� pour %s.
Viewmol.noDefaultFilter:                         Filtre d'importation par d�faut absent.
Viewmol.noFile:                                  Fichier %s non trouv�.
Viewmol.cannotOpen:                              Impossible d'ouvrir le fichier %s.
Viewmol.FileExists:                              Fichier %s existant.
Viewmol.cannotExecute:                           Ex�cution impossible de %s.
Viewmol.notConverged:                            Les OMs du fichier %s sont non converg�es.
Viewmol.noBrowser:                               Navigateur Web non trouv� pour l'affichage du manuel.\n%s inconnu. Saisir la ligne\n'Viewmol.webBrowser: <votre navigateur Web>'\n dans votre fichier $HOME/.Xdefaults.
Viewmol.noManual:                                Fichier d'aide %s\ninexistant.
Viewmol.cannotDisplay:                           Impossible d'afficher le manuel.\nWeb Le navigateur Web ne d�marre pas.
Viewmol.noVisual:                                Impossible de trouver une fenetre adapt�e au trac� OpenGL.\nInstallation de OpenGL probablement incorrecte\nou serveur X d�marr� avec des\nextensions incorrectes.
Viewmol.noRayshade:                              Aucune adresse sp�cifi�e pour Rayshade dans le fichier des ressources.
Viewmol.noDisplay:                               Aucun afficheur d'images sp�cifi� dans les ressources.
Viewmol.unableToWriteFeedback:                   M�moire insuffisante pour la sauvegarde du dessin.
Viewmol.wavenumberTitle:                         %s mode, %.6g cm-1
Viewmol.selectAtomTitle:                         S�lectionnez un atome � l'aide de la souris svp.
Viewmol.selectINSTitle:                          Cliquez sur un atome pour valider son poids INS � %f.
Viewmol.basisfunctionTitle:                      Base d'orbitales atomiques %d: %s%d %s
Viewmol.basisfunctionInMOTitle:                  Base d'orbitales atomiques %d pour OM %d: %s%d %.7f*%s
Viewmol.molecularOrbitalTitle:                   Orbitale mol�culaire %d: %s, �nergie %f Hartree
Viewmol.electronDensityTitle:                    Densit� �lectronique
Viewmol.isosurfaceLabel:                         Isosurface:
Viewmol.isosurfaceLevel:                         %.3f
Viewmol.historyTitle:                            Cycle %d �nergie %18.10f Hartrees, |dE/dxyz| %10.6f
Viewmol.wavenumber:                              Nombre d'ondes (cm**-1)
Viewmol.intensity:                               Intensit� (%)
Viewmol.energy:                                  Energie
Viewmol.gradientNorm:                            Norme du gradient
Viewmol.animateSave:                             Vous ne pouvez afficher une mol�cule anim�e.
Viewmol.noNormalModes:                           Les modes normaux n'ont pas �t� lus dans 
Viewmol.GaussianProblem:                         La sortie Gaussienne contient les fonctions %c.\nCette combinaison est actuellement non suport�e.
Viewmol.unknownResource:                         La valeur %s est non permise pour \nla ressource %s.
Viewmol.unsupportedVersion:                      Les fichiers de version %s sont non support�s.
Viewmol.wrongFiletype:                           Format du fichier %s incorrect. 
Viewmol.wrongReference:                          Le fichier r�f�renc� dans %s comme 'type=car' poss�de un format incorrect.
Viewmol.onlySymmetricBasis:                      Un fichier d'entr�e contient seulement des bases\\nd'atomes non �quivalents. Il est pr�sum� qu'une base\\nest identique pour tous les atomes d'un m�me �l�ment.
Viewmol.unknownErrorMessage:                     Filtre d'importation message d'erreur inconnu:\n%s.
Viewmol.noCoordinates:                           Coordonn�es introuvables dans %s.
Viewmol.noEnergy:                                Energie introuvable dans %s.
Viewmol.noMOselected:                            Aucune OM s�lectionn�e pour %s. S�lectionner\nune OM� partir du diagramme d'�nergies svp,\npuis r�essayer.
Viewmol.notChangeable:                           Cette coordonn�e ne peut pas �tre modifi�e\n, elle appartient � un cycle.
Viewmol.notDefined:                              Cette coordonn�e interne est non d�finie.
Viewmol.formatNotRecognized:                     Format du fichier %s inconnu.
Viewmol.wrongBrowser:                            Le navigateur Web ne peut pas �tre trouv� � l'adresse sp�cifi�e.
Viewmol.wrongMoloch:                             Moloch ne peut pas �tre trouv� � l'adresse sp�cifi�e.
Viewmol.differentMolecules:                      Mesures des coordonn�es internes impossibles entre diff�rentes mol�cules.
Viewmol.BusError:                                Erreur de bus. Viewmol\nne peut pas poursuivre.
Viewmol.FloatingPointException:                  Erreur d'exception sur nombres r�els.\nViewmol ne peut pas poursuivre.
Viewmol.SegmentationViolation:                   Erreur de segmentation.\nViewmol ne peut pas poursuivre.
Viewmol.deleteAll:                               Voulez-vous vraiment supprimer\ntoutes les mol�cules ?
Viewmol.deleteOne:                               Voulez-vous vraiment supprimer\n%s ?
Viewmol.unknownElement:                          El�ment %s inconnu.
Viewmol.elementMenuPrefix:                       des atomes
Viewmol.wrongPNGversion:                         La version %s de la librairie PNG est non support�e. Version 1.4 ou sup�rieure requise.
