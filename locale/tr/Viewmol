!*******************************************************************************
!                                                                              *
!                                   Viewmol                                    *
!                                                                              *
!                      X D E F A U L T S . T U R K I S H                       *
!                                                                              *
!                 Copyright (c) Joerg-R. Hill, December 2000                   *
!                                                                              *
!*******************************************************************************
!
! $Id: Viewmol,v 1.6 2003/11/08 15:16:31 jrh Exp $
! $Log: Viewmol,v $
! Revision 1.6  2003/11/08 15:16:31  jrh
! Release 2.4
!
! Revision 1.5  2000/12/10 14:57:57  jrh
! Release 2.3
!
! Revision 1.4  1999/05/24 01:24:21  jrh
! Release 2.2.1
!
! Revision 1.3  1999/02/07 21:43:04  jrh
! Release 2.2
!
! Revision 1.2  1998/01/26 00:46:32  jrh
! Release 2.1
!
! Revision 1.1  1996/12/10  18:44:47  jrh
! Initial revision
!
! Resources which have to be adapted for the installation of external programmes
!
Viewmol.webBrowser:                              mozilla %s
Viewmol.Moloch:                                  moloch
Viewmol.Raytracer:                               x-povray +I%s +O%s +W%d +H%d
Viewmol.DisplayImage:                            xv %s
!
! Resources which determine defaults
!
Viewmol.geometry:                                500x500+50+50
Viewmol.history.geometry:                        500x250+50+590
Viewmol.spectrum.geometry:                       500x250+50+590
Viewmol.MODiagram.geometry:                      250x500+565+50
Viewmol.model:                                   wire
Viewmol.drawingMode:                             surface
Viewmol.bondType:                                conjugated
Viewmol.sphereResolution:                        20
Viewmol.lineWidth:                               0
Viewmol.simplifyWhileRotating:                   True
Viewmol.interpolation:                           linear
Viewmol.bondLength:                              %7.4f Ang
Viewmol.bondAngle:                               %7.2f deg
Viewmol.torsionAngle:                            %7.2f deg
Viewmol.wavenumbers:                             0:5000
Viewmol.isosurface:                              0.05
Viewmol.densityResolution:                       0.01
Viewmol.reservedColors:                          0
Viewmol.hydrogenBondThreshold:                   2.0
Viewmol.automaticRecalculation:                  False
Viewmol.thermoUnits:                             joules
Viewmol*spectrumForm*amplitudeSlider.decimalPoints: 2
Viewmol*spectrumForm*amplitudeSlider.minimum:    -250
Viewmol*spectrumForm*amplitudeSlider.maximum:    250
Viewmol*spectrumForm*scaleSlider.decimalPoints:  2
Viewmol*spectrumForm*scaleSlider.minimum:        50
Viewmol*spectrumForm*scaleSlider.maximum:        150
Viewmol*thermoForm*pressureSlider.decimalPoints: 2
Viewmol*thermoForm*pressureSlider.minimum:       1
Viewmol*thermoForm*pressureSlider.maximum:       1000
Viewmol*wavefunctionForm*level.decimalPoints:    3
Viewmol*wavefunctionForm*level.minimum:          1
Viewmol*wavefunctionForm*level.maximum:          100
Viewmol*wavefunctionForm*grid.minimum:           4
Viewmol*wavefunctionForm*grid.maximum:           40
Viewmol*wavefunctionForm*grid.value:             20
Viewmol*MODiagramForm*resolution.minimum:        1
Viewmol*MODiagramForm*resolution.maximum:        1000
Viewmol*MODiagramForm*resolution.decimalPoints:  3
Viewmol*unitcellForm*avalue.minimum:             10
Viewmol*unitcellForm*avalue.maximum:             50
Viewmol*unitcellForm*avalue.decimalPoints:       1
Viewmol*unitcellForm*bvalue.minimum:             10
Viewmol*unitcellForm*bvalue.maximum:             50
Viewmol*unitcellForm*bvalue.decimalPoints:       1
Viewmol*unitcellForm*cvalue.minimum:             10
Viewmol*unitcellForm*cvalue.maximum:             50
Viewmol*unitcellForm*cvalue.decimalPoints:       1
Viewmol*unitcellForm*hvalue.minimum:             -5
Viewmol*unitcellForm*hvalue.maximum:             5
Viewmol*unitcellForm*kvalue.minimum:             -5
Viewmol*unitcellForm*kvalue.maximum:             5
Viewmol*unitcellForm*lvalue.minimum:             -5
Viewmol*unitcellForm*lvalue.maximum:             5 
Viewmol*bondForm*thresholdSlider.minimum:        100
Viewmol*bondForm*thresholdSlider.maximum:        250
Viewmol*bondForm*thresholdSlider.decimalPoints:  2
Viewmol*bondForm*scaleRadius.minimum:            1
Viewmol*bondForm*scaleRadius.maximum:            200
Viewmol*bondForm*scaleRadius.decimalPoints:      2 
Viewmol*_popup.infoForm*rows:                    6
Viewmol*_popup.infoForm*columns:                 80
Viewmol*annotation.highlightThickness:           0
Viewmol.paperSize:                               A4
Viewmol.elementSortOrder:                        C,H,O,N,S
Viewmol.viewer.font:                             -adobe-courier-medium-r-normal--0-0-0-0-m-0-iso8859-9
Viewmol.spectrum.spectrum.font:                  -adobe-courier-medium-r-normal--0-0-0-0-m-0-iso8859-9
Viewmol.history.history.font:                    -adobe-courier-medium-r-normal--0-0-0-0-m-0-iso8859-9
Viewmol.MODiagram.MODiagram.font:                -adobe-courier-medium-r-normal--0-0-0-0-m-0-iso8859-9
Viewmol.viewer.background:                       white
Viewmol.viewer.foreground:                       gray75
Viewmol.spectrum.spectrum.background:            white
Viewmol.spectrum.spectrum.foreground:            black
Viewmol.history.history.background:              white
Viewmol.history.history.foreground:              blue
Viewmol.MODiagram.MODiagram.background:          white
Viewmol.MODiagram.MODiagram.foreground:          black
Viewmol*foreground:                              black
!
! Resources which provide the Indigo Magic Desktop look-and-feel
! on SGIs
!
Viewmol*sgiMode:                                 True
Viewmol*useSchemes:                              all
Viewmol*SgNuseEnhancedFSB:                       True
!
! Resources which have to be changed for the translation of Viewmol into
! another language
!
Viewmol.language:                                tr_eu
Viewmol.title:                                   Molek�lleri G�ster
Viewmol.by:                                      Taraf�ndan
Viewmol.version:                                 Versiyon
Viewmol.history.title:                           Ge�mi�i optimize etme(%s)
Viewmol.spectrum.title:                          Spectrum (%s)
Viewmol.spectrum.title1:                         B�t�m modlar (%s)
Viewmol.spectrum.title2:                         IR spektrumu (%s)
Viewmol.spectrum.title3:                         Raman spectrum (%s)
Viewmol.spectrum.title4:                         INS spectrum (%s)
Viewmol.MODiagram.title:                         Energy level diagram (%s)
Viewmol*_popup.molecule.labelString:             Molek�l
Viewmol*loadMolecule.labelString:                Molek�l Y�kle...
Viewmol*saveMolecule.labelString:                Molek�l Kaydet.....
Viewmol*saveSelected.labelString:                Se�ili Atomlar� Kaydet...
Viewmol*deleteMolecule.labelString:              Molek�lleri Sil
Viewmol*newMolecule.labelString:                 Yeni Molek�l
Viewmol*buildMolecule.labelString:               Molek�l� De�i�tir...
Viewmol*_popup.wire_model.labelString:           Modeli Ba�la
Viewmol*_popup.wire_model.mnemonic:              W
Viewmol*_popup.wire_model.accelerator:           Meta<Key>W
Viewmol*_popup.stick_model.labelString:          Modeli Yap��t�r..
Viewmol*_popup.stick_model.mnemonic:             t
Viewmol*_popup.stick_model.accelerator:          Meta<Key>T
Viewmol*_popup.ball_and_stick_model.labelString: Ball and stick model
Viewmol*_popup.ball_and_stick_model.mnemonic:    a
Viewmol*_popup.ball_and_stick_model.accelerator: Meta<Key>A
Viewmol*_popup.cpk_model.labelString:            CPK model
Viewmol*_popup.cpk_model.mnemonic:               C
Viewmol*_popup.cpk_model.accelerator:            Meta<Key>C
Viewmol*pseForm_popup*title:                     Molek�l� De�i�tir
Viewmol*change.labelString:                      Geometriyi De�i�tir
Viewmol*add.labelString:                         Atom Ekle
Viewmol*delete.labelString:                      Atom Sil
Viewmol*replace.labelString:                     Atom De�i�ti
Viewmol*create.labelString:                      Ba� Olu�tur
Viewmol*remove.labelString:                      Ba�� Kald�r
Viewmol*order.labelString:                       Ba� D�zenini De�i�tir
Viewmol*torsionDefault.labelString:              A��lar� (varsay�lanlar�) de�i�tir
Viewmol*trans.labelString:                       Bitir
Viewmol*cis.labelString:                         cis
Viewmol*gauche.labelString:                      Yapamad�n�z
Viewmol*-gauche.labelString:                     -Yapamad�n�z
Viewmol*bondOrderLabel.labelString:              A��lar�
Viewmol*pseForm_popup*fractional.labelString:    Van der Waals
Viewmol*pseForm_popup*single.labelString:        tek
Viewmol*pseForm_popup*double.labelString:        �ift
Viewmol*pseForm_popup*triple.labelString:        ��misli
Viewmol*localGeometry.labelString:               lokal geometrideki atomlar� temizle..
Viewmol*_popup.geometry_menu.labelString:        Geometri ...
Viewmol*clear_all.labelString:                   T�m�n� temizle..
Viewmol*clear_all.accelerator:                   Ctrl<Key>A
Viewmol*clear_all.acceleratorText:               Ctrl+A
Viewmol*clear_last.labelString:                  Son de�i�ikli�i geri al
Viewmol*clear_last.accelerator:                  Ctrl<Key>L
Viewmol*clear_last.acceleratorText:              Ctrl+L
Viewmol*undo.labelString:                        Geometri de�i�ikli�ini geri al
Viewmol*undo.accelerator:                        Ctrl<Key>U
Viewmol*undo.acceleratorText:                    Ctrl+U
Viewmol*bondForm_popup.title:                    Ba�lar
Viewmol*_popup.bondType_menu.labelString:        Ba�lar ...
Viewmol*bondForm*single.labelString:             Sadece Tekil
Viewmol*bondForm*multiple.labelString:           �oklu
Viewmol*bondForm*conjugated.labelString:         �ektir
Viewmol*bondForm*select.labelString:             A��lar� ayarla
Viewmol*bondForm*all.labelString:                T�m�
Viewmol*bondForm*atoms.labelString:              Atomlar taraf�ndan..
Viewmol*showHydrogenBonds.labelString:           Hidrojen Ba�lar�n� G�ster
Viewmol*HydrogenBondLabel.labelString:           Hidrojen Ba�lar� i�in e�ik de�eri [Ang]
Viewmol*_popup.wave_function.labelString:        Dalga Fonksiyonu ...
Viewmol*_popup.wave_function.mnemonic:           v
Viewmol*_popup.wave_function.accelerator:        Meta<Key>V
Viewmol*_popup.energy_level_diagram.labelString: Aktif diagram�n enerjisi
Viewmol*_popup.energy_level_diagram.mnemonic:    E
Viewmol*_popup.energy_level_diagram.accelerator: Meta<Key>E
Viewmol*_popup.optimization_history.labelString: Ge�mi�i Optimize Et
Viewmol*_popup.optimization_history.mnemonic:    O
Viewmol*_popup.optimization_history.accelerator: Meta<Key>O
Viewmol*_popup.show_forces.labelString:          G�c� g�ster
Viewmol*_popup.show_forces.mnemonic:             f
Viewmol*_popup.show_forces.accelerator:          Meta<Key>F
Viewmol*_popup.spectrum.labelString:             Tayf
Viewmol*_popup.spectrum.mnemonic:                S
Viewmol*_popup.spectrum.accelerator:             Meta<Key>S
Viewmol*_popup.thermodynamics.labelString:       Thermodinamik
Viewmol*_popup.thermodynamics.mnemonic:          y
Viewmol*_popup.thermodynamics.accelerator:       Meta<Key>Y
Viewmol*_popup.show_unit_cell.labelString:       Birim H�cre...
Viewmol*_popup.show_unit_cell.mnemonic:          n
Viewmol*_popup.show_unit_cell.accelerator:       Meta<Key>N
Viewmol*_popup.show_ellipsoid_of_inertia.labelString: Show ellipsoid of inertia
Viewmol*_popup.show_ellipsoid_of_inertia.mnemonic: i
Viewmol*_popup.show_ellipsoid_of_inertia.accelerator: Meta<Key>I
Viewmol*_popup.drawing_modes.labelString:        �izim Modu..
Viewmol*_popup.drawing_modes.mnemonic:           m
Viewmol*_popup.drawing_modes.accelerator:        Meta<Key>M
Viewmol*_popup.background_color.labelString:     ArkaPlan Rengi
Viewmol*_popup.background_color.mnemonic:        B
Viewmol*_popup.background_color.accelerator:     Meta<Key>B
Viewmol*_popup.foreground_color.labelString:     Alan Rengi...
Viewmol*_popup.foreground_color.mnemonic:        G
Viewmol*_popup.foreground_color.accelerator:     Meta<Key>G
Viewmol*_popup.label_atoms.labelString:          Atom Etiketi
Viewmol*_popup.label_atoms.mnemonic:             L
Viewmol*_popup.label_atoms.accelerator:          Meta<Key>L
Viewmol*_popup.annotate.labelString:             A��klay�c� Not
Viewmol*_popup.annotate.accelerator:             Ctrl<Key>N
Viewmol*_popup.annotate.acceleratorText:         Ctrl+N
Viewmol*_popup.runScript.labelString:            Script �al��t�r
Viewmol*select.labelString:                      Se�...
Viewmol*select.accelerator:                      Ctrl<Key>R
Viewmol*select.acceleratorText:                  Ctrl+R
Viewmol*_popup.hardcopy.labelString:             �izimi Kaydet...
Viewmol*_popup.hardcopy.mnemonic:                d
Viewmol*_popup.hardcopy.accelerator:             Meta<Key>D
Viewmol*_popup.raytracing.labelString:           I��n izleme
Viewmol*_popup.raytracing.mnemonic:              R
Viewmol*_popup.raytracing.accelerator:           Meta<Key>R
Viewmol*_popup.manual.labelString:               Yard�m / El Kitab�
Viewmol*_popup.manual.mnemonic:                  H
Viewmol*_popup.manual.accelerator:               Meta<Key>H
Viewmol*_popup.saveConfiguration.labelString:    Ayarlar ...
Viewmol*_popup.quit.labelString:                 ��k��
Viewmol*_popup.quit.mnemonic:                    Q
Viewmol*_popup.quit.accelerator:                 Meta<Key>Q
Viewmol*_popup.select_molecule.labelString:      Molek�l� Se�
Viewmol*all.labelString:                         T�m�
Viewmol*spectrumForm_popup.title:                Spektrum i�in Ayarlar
Viewmol.spectrum*_popup.settings_spectrum.labelString: Spektrum i�in Ayarlar...
Viewmol.spectrum*_popup.settings_spectrum.mnemonic: S
Viewmol.spectrum*_popup.settings_spectrum.accelerator: Meta<Key>S
Viewmol.spectrum*_popup.observed_spectrum.labelString: Observerd Spektrumu oku ...
Viewmol.spectrum*_popup.observed_spectrum.mnemonic: R
Viewmol.spectrum*_popup.observed_spectrum.accelerator: Meta<Key>R
Viewmol.spectrum*_popup.delete_spectrum.labelString: Observerd Spektrumu sil..
Viewmol.spectrum*_popup.delete_spectrum.mnemonic: e
Viewmol.spectrum*_popup.delete_spectrum.accelerator: Meta<Key>E
Viewmol.spectrum*_popup.imaginary_wave_numbers.labelString: Ger�ek olmayan dalga numaralar� ...
Viewmol.spectrum*_popup.zoom_out.labelString:    Uzakla�t�r
Viewmol.spectrum*_popup.zoom_out.mnemonic:       Z
Viewmol.spectrum*_popup.zoom_out.accelerator:    Meta<Key>Z
Viewmol.spectrum*_popup.hardcopy.labelString:    �izimi Kaydet...
Viewmol.spectrum*_popup.hardcopy.mnemonic:       d
Viewmol.spectrum*_popup.hardcopy.accelerator:    Meta<Key>D
Viewmol.spectrum*_popup.foreground_color.labelString: �nalan Rengi ...
Viewmol.spectrum*_popup.foreground_color.mnemonic: F
Viewmol.spectrum*_popup.foreground_color.accelerator: Meta<Key>F
Viewmol.spectrum*_popup.background_color.labelString: Arkaplan rengi ...
Viewmol.spectrum*_popup.background_color.mnemonic: B
Viewmol.spectrum*_popup.background_color.accelerator: Meta<Key>B
Viewmol.spectrum*_popup.quit_spectrum.labelString: Spektrumdan ��k
Viewmol.spectrum*_popup.quit_spectrum.mnemonic:  Q
Viewmol.spectrum*_popup.quit_spectrum.accelerator: Meta<Key>Q
Viewmol*thermoForm_popup.title:                  Termo Dinamik
Viewmol*thermoForm*molecules.labelString:        Molek�ller
Viewmol*thermoForm*reactions.labelString:        Reaksiyonlar
Viewmol*thermoForm*moleculeMass:                 K�me %.2f g/mol
Viewmol*thermoForm*solidDensity:                 Yo�unluk %.2f g/cm^3
Viewmol*thermoForm*symmetryNumber:               Simetri Numaras� %d
Viewmol*thermoForm*rotationalConstants:          Rotasyonel Dire�imleri %.3f %.3f %.3f 1/cm
Viewmol*joules:                                  J
Viewmol*calories:                                cal
Viewmol*format:                                  %.3f
Viewmol*thermoForm*enthalphy.labelString:        H/[k%s/mol]
Viewmol*thermoForm*entropy.labelString:          S/[%s/(mol K)]
Viewmol*thermoForm*gibbsEnergy.labelString:      G/[k%s/mol]
Viewmol*thermoForm*heatCapacity.labelString:     C_v/[%s/(mol K)]
Viewmol*thermoForm*reactant.labelString:         Reactant
Viewmol*thermoForm*notInvolved.labelString:      Reaksiyon i�in par�a yok ...
Viewmol*thermoForm*product.labelString:          �r�n
Viewmol*thermoForm*allReactions.labelString:     B�t�n Reaksiyonlar
Viewmol*thermoForm*noReaction:                   Reaksiyon tan�mlanmad�
Viewmol*thermoForm*missingAtoms:                 (Reaksiyon ger�ekle�tirilemedi..)
Viewmol*thermoForm*cantBalance:                  (Denge reaksiyonu ger�ekle�tirilemedi)
Viewmol*thermoForm*inconsistentType:             \"Reactant/Product\" and \"All Reactions\" Kar��t�r�lamad�.
Viewmol*thermoForm*translation.labelString:      �evirme
Viewmol*thermoForm*pv.labelString:               pV
Viewmol*thermoForm*rotation.labelString:         Rotasyon
Viewmol*thermoForm*vibration.labelString:        Titre�im
Viewmol*thermoForm*total.labelString:            Total
Viewmol*thermoForm*electronicEnergy.labelString: Elektronik reaksiyon enerjisi
Viewmol*thermoForm*statisticalEnergy.labelString: �statistiksel mekanik reaksiyon enerjisi
Viewmol*thermoForm*reactionEnergy.labelString:   Toplam reaksiyon enerjisi
Viewmol*thermoForm*reactionEntropy.labelString:  Reaksiyon Entropisi
Viewmol*thermoForm*reactionGibbsEnergy.labelString: Reaksiyon Enerjisi Girdilari
Viewmol*thermoForm*reactionHeatCapacity.labelString:Reaksiyon Is�s�
Viewmol*thermoForm*equilibriumConstant.labelString: Denge Logaritmas� (log K)
Viewmol*thermoForm*previous.labelString:         �nceki Reaksiyon
Viewmol*thermoForm*next.labelString:             Sonraki Reaksiyon
Viewmol*kiloperMole:                             % 15.2f k%s/mol
Viewmol*perMoleandK:                             % 15.2f %s/(mol K)
Viewmol*noUnit:                                  % 15.2f
Viewmol*thermoForm*unitlabel.labelString:        Kullan
Viewmol*thermoForm*joules.labelString:           Joules
Viewmol*thermoForm*calories.labelString:         Kaloriler
Viewmol*thermoForm*thermocalories.labelString:   Termodinamik Kalori
Viewmol*thermoForm*temperature.labelString:      S�cakl�k
Viewmol*thermoForm*pressure.labelString:         Bas�n� /[atm]
Viewmol*balanceForm_popup.title:                 Reaksiyonu manuel olarak dengele ...
Viewmol*historyForm_popup.title:                 Ge�mi� Ayarlar
Viewmol.history*_popup.settings_history.labelString: Ge�mi� Ayarlar ..
Viewmol.history*_popup.settings_history.mnemonic: S
Viewmol.history*_popup.settings_history.accelerator: Meta<Key>S
Viewmol.history*_popup.animate_history.labelString: Animasyon
Viewmol.history*_popup.animate_history.mnemonic:    A
Viewmol.history*_popup.animate_history.accelerator: Meta<Key>A
Viewmol.history*_popup.hardcopy.labelString:     �izimi Kaydet...
Viewmol.history*_popup.hardcopy.mnemonic:        d
Viewmol.history*_popup.hardcopy.accelerator:     Meta<Key>D
Viewmol.history*_popup.energy_color.labelString: Enerji i�in renk ...
Viewmol.history*_popup.energy_color.mnemonic:    e
Viewmol.history*_popup.energy_color.accelerator: Meta<Key>E
Viewmol.history*_popup.gradient_color.labelString: Meyil i�in renk...
Viewmol.history*_popup.gradient_color.mnemonic:  g
Viewmol.history*_popup.gradient_color.accelerator: Meta<Key>G
Viewmol.history*_popup.background_color.labelString: Arka plan rengi...
Viewmol.history*_popup.background_color.mnemonic: B
Viewmol.history*_popup.background_color.accelerator: Meta<Key>B
Viewmol.history*_popup.quit_history.labelString: Optimizal Se�eneklerden ��k
Viewmol.history*_popup.quit_history.mnemonic:    Q
Viewmol.history*_popup.quit_history.accelerator: Meta<Key>Q
Viewmol.MODiagram*_popup.settings_modiagram.labelString: Enerji leveli i�in ayarlar ...
Viewmol.MODiagram*_popup.settings_modiagram.mnemonic: S
Viewmol.MODiagram*_popup.settings_modiagram.accelerator: Meta<Key>S
Viewmol.MODiagram*_popup.transition.labelString: Ge�i�
Viewmol.MODiagram*_popup.transition.mnemonic:    T
Viewmol.MODiagram*_popup.transition.accelerator: Meta<Key>T
Viewmol.MODiagram*_popup.zoom_out.labelString:   Uzakla�t�r
Viewmol.MODiagram*_popup.zoom_out.mnemonic:      Z
Viewmol.MODiagram*_popup.zoom_out.accelerator:   Meta<Key>Z
Viewmol.MODiagram*_popup.hardcopy.labelString:   �izimi Kaydet...
Viewmol.MODiagram*_popup.hardcopy.mnemonic:      d
Viewmol.MODiagram*_popup.hardcopy.accelerator:   Meta<Key>D
Viewmol.MODiagram*_popup.energy_levels.labelString: Yo�unluk Durumunu De�i�tir ...
Viewmol.MODiagram*_popup.energy_levels.mnemonic: e
Viewmol.MODiagram*_popup.energy_levels.accelerator: Meta<Key>E
Viewmol.MODiagram*_popup.foreground_color.labelString: �nalan rengi ..
Viewmol.MODiagram*_popup.foreground_color.mnemonic: F
Viewmol.MODiagram*_popup.foreground_color.accelerator: Meta<Key>F
Viewmol.MODiagram*_popup.background_color.labelString: Arka Plan Rengi...
Viewmol.MODiagram*_popup.background_color.mnemonic: B
Viewmol.MODiagram*_popup.background_color.accelerator: Meta<Key>B
Viewmol.MODiagram*_popup.quit_modiagram.labelString: Enerji leveli diagram�ndan ��k ...
Viewmol.MODiagram*_popup.quit_modiagram.mnemonic: Q
Viewmol.MODiagram*_popup.quit_modiagram.accelerator: Meta<Key>Q
Viewmol*messageForm_popup*exit.labelString:      ��k��
Viewmol*messageForm_popup*title:                 Not
Viewmol*infoForm_popup.title:                    Python
Viewmol*basisForm_popup.title:                   Temel Fonksiyonlar
Viewmol*basisForm_popup.basisForm.rowcolumn.atomname.labelString: Atomarlar i�in temel funksiyonlar %s%d
Viewmol.fileSelectionBox_popup.title:            Dosya Se�imi
Viewmol*fileSelectionBox.dirListLabelString:     Dizinler
Viewmol*fileSelectionBox.fileListLabelString:    Dosyalar
Viewmol*fileSelectionBox.filterLabelString:      Yol
Viewmol*fileSelectionBox.applyLabelString:       S�zge�
Viewmol*fileSelectionBox.okLabelString:          Tamam
Viewmol*fileSelectionBox.cancelLabelString:      �ptal
Viewmol*fileSelectionBox.selectionLabelString:   Se�im
Viewmol*ok.labelString:                          Tamam
Viewmol*cancel.labelString:                      �ptal
Viewmol*continue.labelString:                    Devam et
Viewmol*save.labelString:                        Kaydet
Viewmol*optimizationForm_popup*title:            Ge�mi�i optimize et .
Viewmol*optimizationForm*energies.labelString:   Enerjiler
Viewmol*optimizationForm*norms.labelString:      Norm e�ilimleri
Viewmol*optimizationForm*scales.labelString:     �l��ler
Viewmol*spectrumForm*all_modes.labelString:      T�m Modlar
Viewmol*spectrumForm*ir_modes.labelString:       IR Aktif Modlar�
Viewmol*spectrumForm*raman_modes.labelString:    Raman Aktif Modlar�
Viewmol*spectrumForm*ins_modes.labelString:      Esnemez n�tron sa�
Viewmol*spectrumForm*axisTop.labelString:        �st�nde dalga numaralar�n� g�ster
Viewmol*spectrumForm*showGrid.labelString:       Klavuz �izgileri
Viewmol*spectrumForm*lineWidthLabel.labelString: �izgi Kal�nl���
Viewmol*spectrumForm*animate.labelString:        Animasyon
Viewmol*spectrumForm*draw_arrows.labelString:    Ok �iz
Viewmol*spectrumForm*distort.labelString:        �arp�t
Viewmol*spectrumForm*line_spectrum.labelString:  �izgi Spektrumu
Viewmol*spectrumForm*gaussian_spectrum.labelString: Gaussian spektrumu
Viewmol*spectrumForm*setins.labelString:         Esnemez n�tron sa�ma a��rl���
Viewmol*spectrumForm*temperature.labelString:    S�cakl�k
Viewmol*spectrumForm*amplitude.labelString:      Geni�lik
Viewmol*spectrumForm*scale.labelString:          Dalga boyunu ayarlar\nnumbers
Viewmol*wavefunctionForm_popup.title:            Dalga ayar�
Viewmol*wavefunctionForm*all_off.labelString:    T�m�n� kapat
Viewmol*wavefunctionForm*basis_function.labelString: Temel fonksiyonlar
Viewmol*wavefunctionForm*basis_in_mo.labelString: Basis  MO daki funksiyonlar
Viewmol*wavefunctionForm*molecular_orbital.labelString: Molek�l orbitalleri
Viewmol*wavefunctionForm*electron_density.labelString: Elektron Yo�unlu�u
Viewmol*wavefunctionForm*interpolationLabel.labelString: Enterpolasyon
Viewmol*wavefunctionForm*none.labelString:       Hi�biri
Viewmol*wavefunctionForm*linear.labelString:     Do�rusal
Viewmol*wavefunctionForm*logarithmic.labelString: Logaritmik
Viewmol*wavefunctionForm*levelLabel.labelString: Isosurface
Viewmol*wavefunctionForm*gridLabel.labelString:  Izgaralar�n Kararl�l���
Viewmol*wavefunctionForm*automatic.labelString:  Otomatik Hesapla
Viewmol*MODiagramForm_popup.title:               Ayarlar
Viewmol*MODiagramForm*hartrees.labelString:      Hartrees
Viewmol*MODiagramForm*kj_mol.labelString:        kJ/mol
Viewmol*MODiagramForm*ev.labelString:            eV
Viewmol*MODiagramForm*cm.labelString:            cm^-1
Viewmol*MODiagramForm*resolutionlabel.labelString: Kararl�l�k
Viewmol*printForm_popup.title:                   �izimi Kaydet
Viewmol*printForm*hpgl.labelString:              HPGL
Viewmol*printForm*postscript.labelString:        PostScript
Viewmol*printForm*raytracer.labelString:         Povray
Viewmol*printForm*tiff.labelString:              TIFF
Viewmol*printForm*png.labelString:               PNG
Viewmol*printForm*landscape.labelString:         �evresi
Viewmol*printForm*portrait.labelString:          Portre
Viewmol*printForm*papersize.labelString:         Ka��t Boyutu
Viewmol*printForm*a5.labelString:                A5
Viewmol*printForm*a4.labelString:                A4
Viewmol*printForm*a3.labelString:                A3
Viewmol*printForm*letter.labelString:            Mektup
Viewmol*printForm*legal.labelString:             Yasal
Viewmol*printForm*userdefined.labelString:       Kullan�c� Tan�ml�
Viewmol*printForm*lzw.labelString:               LZW
Viewmol*printForm*mac.labelString:               Macintosh
Viewmol*printForm*none.labelString:              Hi�biri
Viewmol*printForm*compressionlabel.labelString:  TIFF S�k��t�rma
Viewmol*printForm*transparent.labelString:       Transparan Arkaplan
Viewmol*printForm*widthlabel.labelString:        Sayfa Geni�li�i (mm)
Viewmol*printForm*heightlabel.labelString:       Sayfa Y�ksekli�i(mm)
Viewmol*printForm*file.labelString:              Dosya
Viewmol*printForm*select.labelString:            Se�
Viewmol*drawingModeForm_popup.title:             �izim Modu
Viewmol*drawingModeForm*dots.labelString:        Noktalar ile ...
Viewmol*drawingModeForm*lines.labelString:       �izgiler ile ...
Viewmol*drawingModeForm*surfaces.labelString:    Y�zey ile ...
Viewmol*drawingModeForm*simplify.labelString:    �evirirken �izgiler
Viewmol*drawingModeForm*projectionLabel.labelString: G�sterim
Viewmol*drawingModeForm*orthographic.labelString: Ge�erli Grafik
Viewmol*drawingModeForm*perspective.labelString: Perspektif
Viewmol*drawingModeForm*onOffLabel.labelString:  I��klar� A� / Kapat
Viewmol*drawingModeForm*molecule.labelString:    Molek�lleri Ta��
Viewmol*drawingModeForm*viewpoint.labelString:   Noktalar� Ta��
Viewmol*drawingModeForm*light0.labelString:      1 nolu I���� Ta��  
Viewmol*drawingModeForm*light1.labelString:      2 nolu I���� Ta��
Viewmol*drawingModeForm*light0OnOff.labelString: I��k 1
Viewmol*drawingModeForm*light1OnOff.labelString: I��k 2
Viewmol*drawingModeForm*sphereResolutionLabel.labelString: K�renin Kararl�l���
Viewmol*drawingModeForm*lineWidthLabel.labelString: �izgi Kal�nl���
Viewmol*colorEditor_popup.title:                 Renk Edit�r�
Viewmol*colorEditor*smoothRed.labelString:       D�zle�tir
Viewmol*colorEditor*smoothGreen.labelString:     D�zle�tir
Viewmol*colorEditor*smoothBlue.labelString:      D�zle�tir
Viewmol*doRaytracing.labelString:                I��k Zerresini Ba�lat
Viewmol*stopRaytracing.labelString:              I��k Zerresini Ba�latma
Viewmol*saveMoleculeForm_popup*title:            Molek�l� Kaydet.
Viewmol*saveMoleculeForm*car.labelString:        MSI car-Dosyas�
Viewmol*saveMoleculeForm*arc.labelString:        MSI arc-Dosyas�
Viewmol*saveMoleculeForm*gauss.labelString:      Gaussian 98 Giri� Dosyas�
Viewmol*saveMoleculeForm*mol.labelString:        MDL mol-Dosyas�
Viewmol*saveMoleculeForm*tm.labelString:         Turbomole Dosyas�
Viewmol*unitcellForm_popup.title:                H�cre Birimi
Viewmol*unitcellForm*visible.labelString:        Gizle
Viewmol*unitcellForm*avalue.titleString:         a
Viewmol*unitcellForm*bvalue.titleString:         b
Viewmol*unitcellForm*cvalue.titleString:         c
Viewmol*unitcellForm*miller.labelString:         Show Miller plane
Viewmol*unitcellForm*hvalue.titleString:         h
Viewmol*unitcellForm*kvalue.titleString:         k
Viewmol*unitcellForm*lvalue.titleString:         l
Viewmol*configurationForm_popup*title:           Ayarlar
Viewmol*configurationForm*en_US.labelString:     English
Viewmol*configurationForm*fr.labelString:        French
Viewmol*configurationForm*de.labelString:        German
Viewmol*configurationForm*hu.labelString:        Hungarian
Viewmol*configurationForm*pl.labelString:        Polish
Viewmol*configurationForm*ru.labelString:        Russian
Viewmol*configurationForm*es.labelString:        Spanish
Viewmol*configurationForm*tr_eu.labelString:      T�rk�e
Viewmol*configurationForm*browserLabel.labelString: Web Taray�c�s�n�n Lokasyonu
Viewmol*configurationForm*molochLabel.labelString:    Moloch un Lokasyonu
Viewmol*configurationForm*raytracerLabel.labelString:  Povray �n lokasyonu
Viewmol*configurationForm*displayImageLabel.labelString:  Resimler i�in program g�r�nt�s�n�n lokasyonu
Viewmol.unknownParameter:                        Bilinmeyen komut: %s
Viewmol.selectFormat:                            L�tfen format se�in.
Viewmol.selectCompression:                       L�tfen s�k��t�rma methodunu se�in.
Viewmol.TIFFSaved:                               �izimi TIFF dosyas� olarak kaydet %s.
Viewmol.PNGSaved:                                �izimi PNG dosyas� olarak kaydet %s.
Viewmol.HPGLSaved:                               �izimi HPGL dosyas� olarak kaydet %s.
Viewmol.PostscriptSaved:                         �izimi Postscript dosyas� olarak kaydet %s.
Viewmol.RaytracerSaved:                          �izimi Rayshade dosyas� olarak kaydet %s.
Viewmol.MoleculeSaved:                           Molek�l� dosyaya kaydet %s.
Viewmol.ConfigurationSaved:                      Ayarlar� dosyay� kaydet $HOME/.Xdefaults.
Viewmol.noControlFile:                           Mevcut klas�rde ayarlar dosyas� yok
Viewmol.unableToOpen:                            Dosya a��lamad� %s.
Viewmol.molochFailed:                            Moloch hatal�.
Viewmol.noMolochOutput:                          Moloch ��kt�s� mevcut de�il.
Viewmol.errorOnLine:                             %s .sat�rda %d karakterde hata var
Viewmol.noColors:                                Renk numaralar� i�in ay�rma yetersiz.
Viewmol.noInputFilter:                           %s de giri� de�erleri belirtilmemi�.
Viewmol.noDefaultFilter:                         Varsay�lan giri� filtresi bulunamad�
Viewmol.noFile:                                  %s Dosyas� bulunamad�.
Viewmol.cannotOpen:                              %s Dosyas�n� a�ma ba�ar�s�z
Viewmol.FileExists:                              %s Dosyas� mevcut
Viewmol.cannotExecute:                           �al��t�r�lamad�: %s.
Viewmol.notConverged:                            %s de MOs bir noktada birle�tirilemedi.
Viewmol.noBrowser:                               Manuel olarak taray�c� bulunamad�.\n%s Mevcut de�il.$HOME/.Xdefaults dosyas�na su sat�r� girin \n'Viewmol.webBrowser: <Taray�c�n�z�n ad�>'\n  
Viewmol.noManual:                                Manuel dosyas� mevcut de�il %s\n
Viewmol.cannotDisplay:                           G�r�n�m manuel dosyas� bulunamad�.\n Taray�c�n�z mevcut de�il
Viewmol.noVisual:                                Unable to find a window suitable for OpenGL drawing.\nThere might be an installation problem with OpenGL\nor the X server has not been started with the\ncorrect extensions.
Viewmol.noRayshade:                              No location for Rayshade specified in resources.
Viewmol.noDisplay:                               No display program for images specified in resources.
Viewmol.unableToWriteFeedback:                   Cannot allocate enough memory to write drawing to file.
Viewmol.wavenumberTitle:                         %s mode, %.6g cm-1
Viewmol.selectAtomTitle:                         Please select atom by clicking on it.
Viewmol.selectINSTitle:                          Click on atom to set INS weight to %f.
Viewmol.basisfunctionTitle:                      Basis function %d: %s%d %s
Viewmol.basisfunctionInMOTitle:                  Basis function %d in MO %d: %s%d %.7f*%s
Viewmol.molecularOrbitalTitle:                   Molecular orbital %d: %s, energy %f Hartree
Viewmol.electronDensityTitle:                    Electron density
Viewmol.isosurfaceLabel:                         isosurface: 
Viewmol.isosurfaceLevel:                         %.3f 
Viewmol.historyTitle:                            Cycle %d Energy %18.10f Hartree, |dE/dxyz| %10.6f
Viewmol.wavenumber:                              Wave numbers (cm**-1)
Viewmol.intensity:                               Intensity (%)
Viewmol.energy:                                  Energy
Viewmol.gradientNorm:                            Gradient norm
Viewmol.animateSave:                             You cannot plot an animated molecule.
Viewmol.noNormalModes:                           Normal modes have not been read in.
Viewmol.GaussianProblem:                         The Gaussian output contains %c functions.\nThis combination is currently not supported.
Viewmol.unknownResource:                         The value %s is not allowed for\nthe resource %s.
Viewmol.unsupportedVersion:                      Files of version %s are not supported.
Viewmol.wrongFiletype:                           File %s is of wrong file type.
Viewmol.wrongReference:                          File referenced in %s as 'type=car' is of wrong file type.
Viewmol.onlySymmetricBasis:                      Input file contains basis sets only for non-symmetry\\nequivalent atoms. Assuming that the basis set is the\\nsame for all atoms of the same element.
Viewmol.unknownErrorMessage:                     Input filter sent unknown error message:\n%s.
Viewmol.noCoordinates:                           Cannot find coordinates in file %s.
Viewmol.noEnergy:                                Cannot find energy in file %s.
Viewmol.noMOselected:                            There is no MO selected for %s. Please\nselect a MO in the energy level\ndiagram, then try again.
Viewmol.notChangeable:                           This coordinate cannot be changed\nsince it is part of a ring.
Viewmol.notDefined:                              This internal coordinate is not defined.
Viewmol.formatNotRecognized:                     Cannot recognize format of file %s.
Viewmol.wrongBrowser:                            Web browser cannot be found at specified location.
Viewmol.wrongMoloch:                             Moloch cannot be found at specified location.
Viewmol.differentMolecules:                      Internal coordinates cannot be measured between different molecules.
Viewmol.BusError:                                A bus error occured. Viewmol\ncannot continue.
Viewmol.FloatingPointException:                  A floating point exception occured.\nViewmol cannot continue.
Viewmol.SegmentationViolation:                   A segmentation fault occured.\nViewmol cannot continue.
Viewmol.deleteAll:                               Do you really want to delete\nall molecules ?
Viewmol.deleteOne:                               Do you really want to delete\n%s ?
Viewmol.unknownElement:                          Element %s is not known.
Viewmol.elementMenuPrefix:
Viewmol.wrongPNGversion:                         Version %s of the PNG library is not supported. At least version 1.4 is needed.
