!*******************************************************************************
!                                                                              *
!                                   Viewmol                                    *
!                                                                              *
!                       X D E F A U L T S . G E R M A N                        *
!                                                                              *
!                 Copyright (c) Joerg-R. Hill, December 2000                   *
!                                                                              *
!*******************************************************************************
!
! $Id: Viewmol,v 1.6 2003/11/08 15:14:53 jrh Exp $
! $Log: Viewmol,v $
! Revision 1.6  2003/11/08 15:14:53  jrh
! Release 2.4
!
! Revision 1.5  2000/12/10 14:58:59  jrh
! Release 2.3
!
! Revision 1.4  1999/05/24 01:24:26  jrh
! Release 2.2.1
!
! Revision 1.3  1999/02/07 21:43:26  jrh
! Release 2.2
!
! Revision 1.2  1998/01/26 00:46:40  jrh
! Release 2.1
!
! Revision 1.1  1996/12/10  18:45:01  jrh
! Initial revision
!
! Resourcen, die f�r die Installation externer Programme angepa�t werden m�ssen
!
Viewmol.webBrowser:                              mozilla %s
Viewmol.Moloch:                                  moloch
Viewmol.Raytracer:                               x-povray +I%s +O%s +W%d +H%d
Viewmol.DisplayImage:                            xv %s
!
! Resourcen, die die Standardeinstellungen bestimmen
!
Viewmol.geometry:                                500x500+50+50
Viewmol.history.geometry:                        500x250+50+590
Viewmol.spectrum.geometry:                       500x250+50+590
Viewmol.MODiagram.geometry:                      250x500+565+50
Viewmol.model:                                   wire
Viewmol.drawingMode:                             surface
Viewmol.bondType:                                conjugated
Viewmol.sphereResolution:                        20
Viewmol.lineWidth:                               0
Viewmol.simplifyWhileRotating:                   True
Viewmol.interpolation:                           linear
Viewmol.bondLength:                              %7.4f �
Viewmol.bondAngle:                               %7.2f�
Viewmol.torsionAngle:                            %7.2f�
Viewmol.wavenumbers:                             0:5000
Viewmol.isosurface:                              0.05
Viewmol.densityResolution:                       0.01
Viewmol.reservedColors:                          0
Viewmol.hydrogenBondThreshold:                   2.0
Viewmol.automaticRecalculation:                  False
Viewmol.thermoUnits:                             joules
Viewmol*spectrumForm*amplitudeSlider.decimalPoints: 2
Viewmol*spectrumForm*amplitudeSlider.minimum:    -250
Viewmol*spectrumForm*amplitudeSlider.maximum:    250
Viewmol*spectrumForm*scaleSlider.decimalPoints:  2
Viewmol*spectrumForm*scaleSlider.minimum:        50
Viewmol*spectrumForm*scaleSlider.maximum:        150
Viewmol*thermoForm*pressureSlider.decimalPoints: 2
Viewmol*thermoForm*pressureSlider.minimum:       1
Viewmol*thermoForm*pressureSlider.maximum:       1000
Viewmol*wavefunctionForm*level.decimalPoints:    2
Viewmol*wavefunctionForm*level.minimum:          1
Viewmol*wavefunctionForm*level.maximum:          100
Viewmol*wavefunctionForm*grid.minimum:           4
Viewmol*wavefunctionForm*grid.maximum:           40
Viewmol*wavefunctionForm*grid.value:             20
Viewmol*MODiagramForm*resolution.minimum:        1
Viewmol*MODiagramForm*resolution.maximum:        1000
Viewmol*MODiagramForm*resolution.decimalPoints:  3
Viewmol*unitcellForm*avalue.minimum:             10
Viewmol*unitcellForm*avalue.maximum:             50
Viewmol*unitcellForm*avalue.decimalPoints:       1
Viewmol*unitcellForm*bvalue.minimum:             10
Viewmol*unitcellForm*bvalue.maximum:             50
Viewmol*unitcellForm*bvalue.decimalPoints:       1
Viewmol*unitcellForm*cvalue.minimum:             10
Viewmol*unitcellForm*cvalue.maximum:             50
Viewmol*unitcellForm*cvalue.decimalPoints:       1
Viewmol*unitcellForm*hvalue.minimum:             -5
Viewmol*unitcellForm*hvalue.maximum:             5
Viewmol*unitcellForm*kvalue.minimum:             -5
Viewmol*unitcellForm*kvalue.maximum:             5
Viewmol*unitcellForm*lvalue.minimum:             -5
Viewmol*unitcellForm*lvalue.maximum:             5 
Viewmol*bondForm*thresholdSlider.minimum:        100
Viewmol*bondForm*thresholdSlider.maximum:        250
Viewmol*bondForm*thresholdSlider.decimalPoints:  2
Viewmol*bondForm*scaleRadius.minimum:            1
Viewmol*bondForm*scaleRadius.maximum:            200
Viewmol*bondForm*scaleRadius.decimalPoints:      2 
Viewmol*infoForm*text*rows:                      6
Viewmol*infoForm*text*columns:                   80
Viewmol*annotation.highlightThickness:           0
Viewmol.paperSize:                               A4
Viewmol.elementSortOrder:                        C,H,O,N,S
Viewmol.viewer.font:                             -adobe-helvetica-bold-r-normal--12-120-75-75-P-70-iso8859-1
Viewmol.spectrum.spectrum.font:                  -adobe-helvetica-bold-r-normal--12-120-75-75-P-70-iso8859-1
Viewmol.history.history.font:                    -adobe-helvetica-bold-r-normal--12-120-75-75-P-70-iso8859-1
Viewmol.MODiagram.MODiagram.font:                -adobe-helvetica-bold-r-normal--12-120-75-75-P-70-iso8859-1
Viewmol.viewer.background:                       white
Viewmol.viewer.foreground:                       gray75
Viewmol.spectrum.spectrum.background:            white
Viewmol.spectrum.spectrum.foreground:            black
Viewmol.history.history.background:              white
Viewmol.history.history.foreground:              blue
Viewmol.MODiagram.MODiagram.background:          white
Viewmol.MODiagram.MODiagram.foreground:          black
Viewmol*foreground:                              black
!
! Resourcen, die das Indigo Magic Desktop look-and-feel
! auf SGIs einschalten
!
Viewmol*sgiMode:                                 True
Viewmol*useSchemes:                              all
Viewmol*SgNuseEnhancedFSB:                       True
!
! Resourcen, die f�r die �bersetzung von Viewmol in eine andere Sprache ge�ndert werden m�ssen
!
Viewmol.language:                                de
Viewmol.title:                                   Viewmol
Viewmol.by:                                      von
Viewmol.version:                                 Version
Viewmol.history.title:                           Optimierungsverlauf (%s)
Viewmol.spectrum.title:                          Spektrum (%s)
Viewmol.spectrum.title1:                         Alle Modi (%s)
Viewmol.spectrum.title2:                         IR-Spektrum (%s)
Viewmol.spectrum.title3:                         Raman-Spektrum (%s)
Viewmol.spectrum.title4:                         INS-Spektrum (%s)
Viewmol.MODiagram.title:                         Energieniveauschema (%s)
Viewmol*_popup.molecule.labelString:             Molek�l
Viewmol*loadMolecule.labelString:                Molek�l laden ...
Viewmol*saveMolecule.labelString:                Molek�l speichern ...
Viewmol*saveSelected.labelString:                Ausgew�hlte Atome speichern ...
Viewmol*deleteMolecule.labelString:              Molek�l l�schen
Viewmol*newMolecule.labelString:                 Neues Molek�l ...
Viewmol*buildMolecule.labelString:               Molek�l ver�ndern ...
Viewmol*_popup.wire_model.labelString:           Drahtmodell
Viewmol*_popup.wire_model.mnemonic:              D
Viewmol*_popup.wire_model.accelerator:           Meta<Key>D
Viewmol*_popup.stick_model.labelString:          Stabmodell
Viewmol*_popup.stick_model.mnemonic:             a
Viewmol*_popup.stick_model.accelerator:          Meta<Key>A
Viewmol*_popup.ball_and_stick_model.labelString: Kugel-Stab-Modell
Viewmol*_popup.ball_and_stick_model.mnemonic:    u
Viewmol*_popup.ball_and_stick_model.accelerator: Meta<Key>U
Viewmol*_popup.cpk_model.labelString:            Kalottenmodell
Viewmol*_popup.cpk_model.mnemonic:               K
Viewmol*_popup.cpk_model.accelerator:            Meta<Key>K
Viewmol*pseForm_popup*title:                     Molek�l ver�ndern
Viewmol*change.labelString:                      Geometrie �ndern
Viewmol*add.labelString:                         Atom hinzuf�gen
Viewmol*delete.labelString:                      Atom l�schen
Viewmol*replace.labelString:                     Atom ersetzen
Viewmol*create.labelString:                      Bindung erzeugen
Viewmol*remove.labelString:                      Bindung l�schen
Viewmol*order.labelString:                       Bindungsordnung �ndern
Viewmol*torsionDefault.labelString:              Torsionswinkel sind automatisch
Viewmol*trans.labelString:                       trans
Viewmol*cis.labelString:                         cis
Viewmol*gauche.labelString:                      gauche
Viewmol*-gauche.labelString:                     -gauche
Viewmol*bondOrderLabel.labelString:              Bindungen sind
Viewmol*pseForm_popup*fractional.labelString:    Van der Waals
Viewmol*pseForm_popup*single.labelString:        einfach
Viewmol*pseForm_popup*double.labelString:        doppelt
Viewmol*pseForm_popup*triple.labelString:        dreifach
Viewmol*localGeometry.labelString:               L�schen von Atomen ver�ndert lokale Geometrie
Viewmol*_popup.geometry_menu.labelString:        Geometrie ...
Viewmol*clear_all.labelString:                   Alles l�schen
Viewmol*clear_all.acceleratorText:               Ctrl+A
Viewmol*clear_all.accelerator:                   Ctrl<Key>A
Viewmol*clear_last.labelString:                  Letzte Koordinate l�schen
Viewmol*clear_last.acceleratorText:              Ctrl+L
Viewmol*clear_last.accelerator:                  Ctrl<Key>L
Viewmol*undo.labelString:                        Geometrie�nderung r�ckg�ngigmachen
Viewmol*undo.accelerator:                        Ctrl<Key>R
Viewmol*undo.acceleratorText:                    Ctrl+R
Viewmol*bondForm_popup.title:                    Bindungen
Viewmol*_popup.bondType_menu.labelString:        Bindungen ...
Viewmol*bondForm*single.labelString:             nur einfach
Viewmol*bondForm*multiple.labelString:           mehrfach
Viewmol*bondForm*conjugated.labelString:         konjugiert
Viewmol*bondForm*select.labelString:             Skaliere Radius f�r
Viewmol*bondForm*all.labelString:                alle
Viewmol*bondForm*atoms.labelString:              Atome mit
Viewmol*showHydrogenBonds.labelString:           Wasserstoffbr�ckenbindungen zeigen
Viewmol*HydrogenBondLabel.labelString:           Grenzwert f�r Wasserstoffbr�ckenbindungen [�]
Viewmol*_popup.wave_function.labelString:        Wellenfunktion ...
Viewmol*_popup.wave_function.mnemonic:           W
Viewmol*_popup.wave_function.accelerator:        Meta<Key>W
Viewmol*_popup.energy_level_diagram.labelString: Energieniveauschema
Viewmol*_popup.energy_level_diagram.mnemonic:    E
Viewmol*_popup.energy_level_diagram.accelerator: Meta<Key>E
Viewmol*_popup.optimization_history.labelString: Optimierungsverlauf
Viewmol*_popup.optimization_history.mnemonic:    O
Viewmol*_popup.optimization_history.accelerator: Meta<Key>O
Viewmol*_popup.show_forces.labelString:          Kr�fte zeigen
Viewmol*_popup.show_forces.mnemonic:             f
Viewmol*_popup.show_forces.accelerator:          Meta<Key>F
Viewmol*_popup.spectrum.labelString:             Spektrum
Viewmol*_popup.spectrum.mnemonic:                S
Viewmol*_popup.spectrum.accelerator:             Meta<Key>S
Viewmol*_popup.thermodynamics.labelString:       Thermodynamik
Viewmol*_popup.thermodynamics.mnemonic:          y
Viewmol*_popup.thermodynamics.accelerator:       Meta<Key>Y
Viewmol*_popup.show_unit_cell.labelString:       Elementarzelle ...
Viewmol*_popup.show_unit_cell.mnemonic:          z
Viewmol*_popup.show_unit_cell.accelerator:       Meta<Key>Z
Viewmol*_popup.show_ellipsoid_of_inertia.labelString: Tr�gheitsellipsoid zeigen
Viewmol*_popup.show_ellipsoid_of_inertia.mnemonic: T
Viewmol*_popup.show_ellipsoid_of_inertia.accelerator: Meta<Key>T
Viewmol*_popup.drawing_modes.labelString:        Zeichenmodi ...
Viewmol*_popup.drawing_modes.mnemonic:           m
Viewmol*_popup.drawing_modes.accelerator:        Meta<Key>M
Viewmol*_popup.background_color.labelString:     Hintergrundfarbe ...
Viewmol*_popup.background_color.mnemonic:        H
Viewmol*_popup.background_color.accelerator:     Meta<Key>H
Viewmol*_popup.foreground_color.labelString:     Untergrundfarbe ...
Viewmol*_popup.foreground_color.mnemonic:        g
Viewmol*_popup.foreground_color.accelerator:     Meta<Key>g
Viewmol*_popup.label_atoms.labelString:          Atomnamen
Viewmol*_popup.label_atoms.mnemonic:             n
Viewmol*_popup.label_atoms.accelerator:          Meta<Key>N
Viewmol*_popup.annotate.labelString:             Beschriften
Viewmol*_popup.annotate.accelerator:             Ctrl<Key>B
Viewmol*_popup.annotate.acceleratorText:         Ctrl+B
Viewmol*_popup.runScript.labelString:            Skript starten
Viewmol*select.labelString:                      Ausw�hlen ...
Viewmol*select.accelerator:                      Ctrl<Key>R
Viewmol*select.acceleratorText:                  Ctrl+R
Viewmol*_popup.hardcopy.labelString:             Zeichnung speichern ...
Viewmol*_popup.hardcopy.mnemonic:                p
Viewmol*_popup.hardcopy.accelerator:             Meta<Key>P
Viewmol*_popup.raytracing.labelString:           Raytracing
Viewmol*_popup.raytracing.mnemonic:              R
Viewmol*_popup.raytracing.accelerator:           Meta<Key>R
Viewmol*_popup.manual.labelString:               Manual
Viewmol*_popup.manual.mnemonic:                  M
Viewmol*_popup.manual.accelerator:               Meta<Key>M
Viewmol*_popup.saveConfiguration.labelString:    Konfiguration ...
Viewmol*_popup.quit.labelString:                 Beenden
Viewmol*_popup.quit.mnemonic:                    B
Viewmol*_popup.quit.accelerator:                 Meta<Key>B
Viewmol*_popup.select_molecule.labelString:      Molek�l ausw�hlen
Viewmol*all.labelString:                         Alle
Viewmol*spectrumForm_popup.title:                Einstellungen f�r Spektrum
Viewmol.spectrum*_popup.settings_spectrum.labelString: Einstellungen f�r Spektrum ...
Viewmol.spectrum*_popup.settings_spectrum.mnemonic: E
Viewmol.spectrum*_popup.settings_spectrum.accelerator: Meta<Key>E
Viewmol.spectrum*_popup.observed_spectrum.labelString: Beobachtetes Spektrum einlesen...
Viewmol.spectrum*_popup.observed_spectrum.mnemonic: B
Viewmol.spectrum*_popup.observed_spectrum.accelerator: Meta<Key>B
Viewmol.spectrum*_popup.delete_spectrum.labelString: Beobachtetes Spektrum l�schen
Viewmol.spectrum*_popup.delete_spectrum.mnemonic: l
Viewmol.spectrum*_popup.delete_spectrum.accelerator: Meta<Key>L
Viewmol.spectrum*_popup.imaginary_wave_numbers.labelString: Imagin�re Wellenzahlen ...
Viewmol.spectrum*_popup.zoom_out.labelString:    Vergr��erung zur�cksetzen
Viewmol.spectrum*_popup.zoom_out.mnemonic:       z
Viewmol.spectrum*_popup.zoom_out.accelerator:    Meta<Key>Z
Viewmol.spectrum*_popup.hardcopy.labelString:    Zeichnung speichern ...
Viewmol.spectrum*_popup.hardcopy.mnemonic:       p
Viewmol.spectrum*_popup.hardcopy.accelerator:    Meta<Key>P
Viewmol.spectrum*_popup.foreground_color.labelString: Vordergrundfarbe ...
Viewmol.spectrum*_popup.foreground_color.mnemonic: V
Viewmol.spectrum*_popup.foreground_color.accelerator: Meta<Key>V
Viewmol.spectrum*_popup.background_color.labelString: Hintergrundfarbe ...
Viewmol.spectrum*_popup.background_color.mnemonic: H
Viewmol.spectrum*_popup.background_color.accelerator: Meta<Key>H
Viewmol.spectrum*_popup.quit_spectrum.labelString: Spektrum beenden
Viewmol.spectrum*_popup.quit_spectrum.mnemonic:  b
Viewmol.spectrum*_popup.quit_spectrum.accelerator: Meta<Key>B
Viewmol*thermoForm_popup.title:                  Thermodynamik
Viewmol*thermoForm*molecules.labelString:        Molek�le
Viewmol*thermoForm*reactions.labelString:        Reaktionen
Viewmol*thermoForm*moleculeMass:                 Masse %.2f g/mol
Viewmol*thermoForm*solidDensity:                 Dichte %.2f g/cm^3
Viewmol*thermoForm*symmetryNumber:               Symmetriezahl %d
Viewmol*thermoForm*rotationalConstants:          Rotationskonstanten %.3f %.3f %.3f 1/cm
Viewmol*joules:                                  J
Viewmol*calories:                                cal
Viewmol*format:                                  %.3f
Viewmol*thermoForm*enthalphy.labelString:        H/[k%s/mol]
Viewmol*thermoForm*entropy.labelString:          S/[%s/(mol K)]
Viewmol*thermoForm*gibbsEnergy.labelString:      G/[k%s/mol]
Viewmol*thermoForm*heatCapacity.labelString:     C_v/[%s/(mol K)]
Viewmol*thermoForm*reactant.labelString:         Edukt
Viewmol*thermoForm*notInvolved.labelString:      an Reaktion nicht beteiligt
Viewmol*thermoForm*product.labelString:          Produkt
Viewmol*thermoForm*allReactions.labelString:     Alle Reaktionen
Viewmol*thermoForm*noReaction:                   Keine Reaktion definiert.
Viewmol*thermoForm*missingAtoms:                 (Eine solche Reaktion ist nicht m�glich)
Viewmol*thermoForm*cantBalance:                  (Kann Reaktionsgleichung nicht ausgleichen)
Viewmol*thermoForm*inconsistentType:             \"Edukt/Produkt\" und \"Alle Reaktionen\" k�nnen nicht zusammen verwendet werden.
Viewmol*thermoForm*translation.labelString:      Translation
Viewmol*thermoForm*pv.labelString:               pV
Viewmol*thermoForm*rotation.labelString:         Rotation
Viewmol*thermoForm*vibration.labelString:        Schwingung
Viewmol*thermoForm*total.labelString:            Gesamt
Viewmol*thermoForm*electronicEnergy.labelString: Elektronische Reaktionsenergie
Viewmol*thermoForm*statisticalEnergy.labelString: Statistisch-mechanische Reaktionsenergie
Viewmol*thermoForm*reactionEnergy.labelString:   Gesamtreaktionsenergie
Viewmol*thermoForm*reactionEntropy.labelString:  Reaktionsentropie
Viewmol*thermoForm*reactionGibbsEnergy.labelString: Freie Reaktionsenergie
Viewmol*thermoForm*reactionHeatCapacity.labelString: Reaktionsw�rmekapazit�t
Viewmol*thermoForm*equilibriumConstant.labelString: Logarithmus der Gleichgewichtskonstante (log K)
Viewmol*thermoForm*previous.labelString:         Vorherige Reaktion
Viewmol*thermoForm*next.labelString:             N�chste Reaktion
Viewmol*kiloperMole:                             % 15.2f k%s/mol
Viewmol*perMoleandK:                             % 15.2f %s/(mol K)
Viewmol*noUnit:                                  % 15.2f
Viewmol*thermoForm*unitlabel.labelString:        Verwende
Viewmol*thermoForm*joules.labelString:           Joule
Viewmol*thermoForm*calories.labelString:         Kalorien
Viewmol*thermoForm*thermocalories.labelString:   thermochemische Kalorien
Viewmol*thermoForm*temperature.labelString:      Temperatur
Viewmol*thermoForm*pressure.labelString:         Druck/[atm]
Viewmol*balanceForm_popup.title:                 Reaktionsgleichung manuell ausgleichen
Viewmol*historyForm_popup.title:                 Einstellungen f�r Optimierungsverlauf
Viewmol.history*_popup.settings_history.labelString: Einstellungen f�r Optimierungsverlauf ...
Viewmol.history*_popup.settings_history.mnemonic: E
Viewmol.history*_popup.settings_history.accelerator: Meta<Key>E
Viewmol.history*_popup.animate_history.labelString: Animieren
Viewmol.history*_popup.animate_history.mnemonic:    A
Viewmol.history*_popup.animate_history.accelerator: Meta<Key>A
Viewmol.history*_popup.hardcopy.labelString:     Zeichnung speichern ...
Viewmol.history*_popup.hardcopy.mnemonic:        p
Viewmol.history*_popup.hardcopy.accelerator:     Meta<Key>P
Viewmol.history*_popup.energy_color.labelString: Farbe f�r Energie ...
Viewmol.history*_popup.energy_color.mnemonic:    F
Viewmol.history*_popup.energy_color.accelerator: Meta<Key>F
Viewmol.history*_popup.gradient_color.labelString: Farbe f�r Gradienten ...
Viewmol.history*_popup.gradient_color.mnemonic:  G
Viewmol.history*_popup.gradient_color.accelerator: Meta<Key>G
Viewmol.history*_popup.background_color.labelString: Hintergrundfarbe ...
Viewmol.history*_popup.background_color.mnemonic: H
Viewmol.history*_popup.background_color.accelerator: Meta<Key>H
Viewmol.history*_popup.quit_history.labelString: Optimierungsverlauf beenden
Viewmol.history*_popup.quit_history.mnemonic:    b
Viewmol.history*_popup.quit_history.accelerator: Meta<Key>B
Viewmol.MODiagram*_popup.settings_modiagram.labelString: Einstellungen f�r Energieniveauschema ...
Viewmol.MODiagram*_popup.settings_modiagram.mnemonic: E
Viewmol.MODiagram*_popup.settings_modiagram.accelerator: Meta<Key>E
Viewmol.MODiagram*_popup.transition.labelString: �bergang
Viewmol.MODiagram*_popup.transition.acceleratorText: Alt+U
Viewmol.MODiagram*_popup.transition.accelerator: Meta<Key>U
Viewmol.MODiagram*_popup.zoom_out.labelString:   Vergr��erung zur�cksetzen
Viewmol.MODiagram*_popup.zoom_out.mnemonic:      z
Viewmol.MODiagram*_popup.zoom_out.accelerator:   Meta<Key>Z
Viewmol.MODiagram*_popup.hardcopy.labelString:   Zeichnug speichern ...
Viewmol.MODiagram*_popup.hardcopy.mnemonic:      p
Viewmol.MODiagram*_popup.hardcopy.accelerator:   Meta<Key>P
Viewmol.MODiagram*_popup.energy_levels.labelString: Zustandsdichte zeichnen
Viewmol.MODiagram*_popup.energy_levels.mnemonic: n
Viewmol.MODiagram*_popup.energy_levels.accelerator: Meta<Key>N
Viewmol.MODiagram*_popup.foreground_color.labelString: Vordergrundfarbe ...
Viewmol.MODiagram*_popup.foreground_color.mnemonic: V
Viewmol.MODiagram*_popup.foreground_color.accelerator: Meta<Key>V
Viewmol.MODiagram*_popup.background_color.labelString: Hintergrundfarbe ...
Viewmol.MODiagram*_popup.background_color.mnemonic: H
Viewmol.MODiagram*_popup.background_color.accelerator: Meta<Key>H
Viewmol.MODiagram*_popup.quit_modiagram.labelString: Energieniveauschema beenden
Viewmol.MODiagram*_popup.quit_modiagram.mnemonic: b
Viewmol.MODiagram*_popup.quit_modiagram.accelerator: Meta<Key>B
Viewmol*messageForm_popup*exit.labelString:      Beenden
Viewmol*messageForm_popup*title:                 Beachten Sie
Viewmol*infoForm_popup.title:                    Python
Viewmol*basisForm_popup.title:                   Basisfunktionen
Viewmol*basisForm_popup.basisForm.rowcolumn.atomname.labelString: Basisfunktionen am Atom %s%d
Viewmol.fileSelectionBox_popup.title:            Dateiauswahl
Viewmol*fileSelectionBox.dirListLabelString:     Verzeichnisse
Viewmol*fileSelectionBox.fileListLabelString:    Dateien
Viewmol*fileSelectionBox.filterLabelString:      Pfad
Viewmol*fileSelectionBox.applyLabelString:       Filter
Viewmol*fileSelectionBox.okLabelString:          OK
Viewmol*fileSelectionBox.cancelLabelString:      Abbrechen
Viewmol*fileSelectionBox.selectionLabelString:   Auswahl
Viewmol*ok.labelString:                          OK
Viewmol*cancel.labelString:                      Abbrechen
Viewmol*continue.labelString:                    Weiter
Viewmol*save.labelString:                        Speichern
Viewmol*optimizationForm_popup*title:            Optimierungsverlauf
Viewmol*optimizationForm*energies.labelString:   Energien
Viewmol*optimizationForm*norms.labelString:      Gradientennorm
Viewmol*optimizationForm*scales.labelString:     Achsenbeschriftung
Viewmol*spectrumForm*all_modes.labelString:      Alle Schwingungen
Viewmol*spectrumForm*ir_modes.labelString:       IR-aktive Schwingungen
Viewmol*spectrumForm*raman_modes.labelString:    Raman-aktive Schwingungen
Viewmol*spectrumForm*ins_modes.labelString:      Inelastische Neutronenstreuung
Viewmol*spectrumForm*animate.labelString:        Animieren
Viewmol*spectrumForm*draw_arrows.labelString:    Pfeile zeichnen
Viewmol*spectrumForm*distort.labelString:        Auslenken
Viewmol*spectrumForm*line_spectrum.labelString:  Linienspektrum
Viewmol*spectrumForm*gaussian_spectrum.labelString: Spektrum mit Gau�-Funktionen
Viewmol*spectrumForm*setins.labelString:         Gewichte f�r inelastische Neutronenstreuung setzen
Viewmol*spectrumForm*axisTop.labelString:        Wellenzahlen oben
Viewmol*spectrumForm*showGrid.labelString:       Gitter zeigen
Viewmol*spectrumForm*lineWidthLabel.labelString: Linienst�rke
Viewmol*spectrumForm*temperature.labelString:    Temperatur
Viewmol*spectrumForm*amplitude.labelString:      Amplitude
Viewmol*spectrumForm*scale.labelString:          Wellenzahlen\nskalieren
Viewmol*wavefunctionForm_popup.title:            Wellenfunktion
Viewmol*wavefunctionForm*all_off.labelString:    Alles aus
Viewmol*wavefunctionForm*basis_function.labelString: Basisfunktion
Viewmol*wavefunctionForm*basis_in_mo.labelString: Basisfunktion in MO
Viewmol*wavefunctionForm*molecular_orbital.labelString: Molek�lorbital
Viewmol*wavefunctionForm*electron_density.labelString: Elektronendichte
Viewmol*wavefunctionForm*interpolationLabel.labelString: Interpolation
Viewmol*wavefunctionForm*none.labelString:       Keine
Viewmol*wavefunctionForm*linear.labelString:     Linear
Viewmol*wavefunctionForm*logarithmic.labelString: Logarithmisch
Viewmol*wavefunctionForm*levelLabel.labelString: Isofl�che
Viewmol*wavefunctionForm*gridLabel.labelString:  Gitteraufl�sung
Viewmol*wavefunctionForm*automatic.labelString:  Automatische Wiederberechnung
Viewmol*MODiagramForm_popup.title:               Einstellungen
Viewmol*MODiagramForm*hartrees.labelString:      Hartrees
Viewmol*MODiagramForm*kj_mol.labelString:        kJ/mol
Viewmol*MODiagramForm*ev.labelString:            eV
Viewmol*MODiagramForm*cm.labelString:            cm^-1
Viewmol*MODiagramForm*resolutionlabel.labelString: Aufl�sung
Viewmol*printForm_popup.title:                   Zeichnung speichern
Viewmol*printForm*hpgl.labelString:              HPGL
Viewmol*printForm*postscript.labelString:        PostScript
Viewmol*printForm*raytracer.labelString:         Povray
Viewmol*printForm*tiff.labelString:              TIFF
Viewmol*printForm*png.labelString:               PNG
Viewmol*printForm*landscape.labelString:         Querformat
Viewmol*printForm*portrait.labelString:          Hochformat
Viewmol*printForm*papersize.labelString:         Papiergr��e
Viewmol*printForm*a5.labelString:                A5
Viewmol*printForm*a4.labelString:                A4
Viewmol*printForm*a3.labelString:                A3
Viewmol*printForm*letter.labelString:            Letter
Viewmol*printForm*legal.labelString:             Legal
Viewmol*printForm*userdefined.labelString:       Benutzer definiert
Viewmol*printForm*lzw.labelString:               LZW
Viewmol*printForm*mac.labelString:               Macintosh
Viewmol*printForm*none.labelString:              Keine
Viewmol*printForm*compressionlabel.labelString:  TIFF-Komprimierung
Viewmol*printForm*transparent.labelString:       Hintergrund durchsichtig
Viewmol*printForm*widthlabel.labelString:        Papierbreite in mm
Viewmol*printForm*heightlabel.labelString:       Papierh�he in mm
Viewmol*printForm*file.labelString:              Datei
Viewmol*printForm*select.labelString:            Ausw�hlen
Viewmol*drawingModeForm_popup.title:             Zeichenmodi
Viewmol*drawingModeForm*dots.labelString:        Punkte
Viewmol*drawingModeForm*lines.labelString:       Linien
Viewmol*drawingModeForm*surfaces.labelString:    Oberfl�che
Viewmol*drawingModeForm*simplify.labelString:    Linien bei Drehung
Viewmol*drawingModeForm*projectionLabel.labelString: Projektion
Viewmol*drawingModeForm*orthographic.labelString: Orthografisch
Viewmol*drawingModeForm*perspective.labelString: Perspektivisch
Viewmol*drawingModeForm*onOffLabel.labelString:  Beleuchtung an/aus
Viewmol*drawingModeForm*molecule.labelString:    Molek�l bewegen
Viewmol*drawingModeForm*viewpoint.labelString:   Betrachterstandpunkt bewegen
Viewmol*drawingModeForm*light0.labelString:      Lichtquelle 1 bewegen
Viewmol*drawingModeForm*light1.labelString:      Lichtquelle 2 bewegen
Viewmol*drawingModeForm*light0OnOff.labelString: Lichtquelle 1
Viewmol*drawingModeForm*light1OnOff.labelString: Lichtquelle 2
Viewmol*drawingModeForm*sphereResolutionLabel.labelString: Aufl�sung f�r Kugeln
Viewmol*drawingModeForm*lineWidthLabel.labelString: Linienbreite
Viewmol*colorEditor_popup.title:                 Farb-Editor
Viewmol*colorEditor*smoothRed.labelString:       �bergang
Viewmol*colorEditor*smoothGreen.labelString:     �bergang
Viewmol*colorEditor*smoothBlue.labelString:      �bergang
Viewmol*doRaytracing.labelString:                Raytracing beginnen
Viewmol*stopRaytracing.labelString:              Raytracing nicht beginnen
Viewmol*saveMoleculeForm_popup*title:            Molek�l speichern
Viewmol*saveMoleculeForm*car.labelString:        MSI car-file
Viewmol*saveMoleculeForm*arc.labelString:        MSI arc-file
Viewmol*saveMoleculeForm*gauss.labelString:      Gaussian98-File 
Viewmol*saveMoleculeForm*mol.labelString:        MDL-Mol-File
Viewmol*saveMoleculeForm*tm.labelString:         Turbomole-File
Viewmol*unitcellForm_popup.title:                Elementarzelle
Viewmol*unitcellForm*visible.labelString:        sichtbar
Viewmol*unitcellForm*avalue.titleString:         a
Viewmol*unitcellForm*bvalue.titleString:         b
Viewmol*unitcellForm*cvalue.titleString:         c
Viewmol*unitcellForm*miller.labelString:         Miller-Ebene zeigen
Viewmol*unitcellForm*hvalue.titleString:         h
Viewmol*unitcellForm*kvalue.titleString:         k
Viewmol*unitcellForm*lvalue.titleString:         l
Viewmol*configurationForm_popup*title:           Konfiguration
Viewmol*configurationForm*de.labelString:        Deutsch
Viewmol*configurationForm*en_US.labelString:     Englisch
Viewmol*configurationForm*fr.labelString:        Franz�sisch
Viewmol*configurationForm*pl.labelString:        Polnisch
Viewmol*configurationForm*ru.labelString:        Russisch
Viewmol*configurationForm*es.labelString:        Spanisch
Viewmol*configurationForm*hu.labelString:        Ungarisch
Viewmol*configurationForm*tr.labelString:        T�rkisch
Viewmol*configurationForm*browserLabel.labelString: Pfad zum Web-Browser
Viewmol*configurationForm*molochLabel.labelString:    Pfad zu Moloch
Viewmol*configurationForm*raytracerLabel.labelString:  Pfad zu Povray
Viewmol*configurationForm*displayImageLabel.labelString:  Pfad zum Anzeigeprogramm f�r Bilder
Viewmol.unknownParameter:                        Unbekannter Parameter in der Kommandozeile: %s
Viewmol.selectFormat:                            Bitte ein Format w�hlen.
Viewmol.selectCompression:                       Bitte eine Komprimierungsmethode w�hlen.
Viewmol.TIFFSaved:                               Zeichnung in TIFF-Datei %s gespeichert.
Viewmol.PNGSaved:                                Zeichnung in PNG-Datei %s gespeichert.
Viewmol.HPGLSaved:                               Zeichnung in HPGL-Datei %s gespeichert.
Viewmol.PostscriptSaved:                         Zeichnung in Postscript-Datei %s gespeichert.
Viewmol.RaytracerSaved:                          Zeichnung in Rayshade-Datei %s gespeichert.
Viewmol.MoleculeSaved:                           Molek�l in Datei %s gespeichert.
Viewmol.ConfigurationSaved:                      Konfiguration in Datei $HOME/.Xdefaults gespeichert.
Viewmol.noControlFile:                           Eine Datei 'control' ist in diesem Verzeichnis\nnicht vorhanden.
Viewmol.unableToOpen:                            Kann Datei %s nicht �ffnen.
Viewmol.molochFailed:                            Moloch ist nicht erfolgreich gelaufen.
Viewmol.noMolochOutput:                          Moloch-Ausgabedatei ist nicht vorhanden.
Viewmol.errorOnLine:                             Fehler in Zeile %d von %s.
Viewmol.noColors:                                Kann nicht die notwendige Anzahl von Farben reservieren.
Viewmol.noInputFilter:                           Keine Einlesefilter in %s angegeben.
Viewmol.noDefaultFilter:                         Keinen Default-Einlesefilter gefunden.
Viewmol.noFile:                                  Datei %s existiert nicht.
Viewmol.cannotOpen:                              Kann Datei %s nicht �ffnen.
Viewmol.FileExists:                              Datei %s existiert bereits.
Viewmol.cannotExecute:                           Kann %s nicht ausf�hren.
Viewmol.notConverged:                            MOs in %s sind nicht konvergiert.
Viewmol.noBrowser:                               Kann keinen Web-Browser f�r das Manual finden.\n%s existiert nicht. F�gen Sie eine Zeile\n'Viewmol.webBrowser: <Ihr Web-Browser>'\nin Ihre $HOME/.Xdefaults Datei ein.
Viewmol.noManual:                                Manual-Datei %s\nexistiert nicht.
Viewmol.cannotDisplay:                           Manual kann nicht angezeigt werden.\nWeb-Browser startet nicht.
Viewmol.noVisual:                                Kann kein f�r OpenGL-Zeichnungen geeignetes Fenster finden.\nEntweder ist OpenGL nicht richtig installiert\noder der X-Server ist nicht mit den richtigen Extensions\ngestartet worden.
Viewmol.noRayshade:                              Kein Pfad zu Rayshade in den Resourcen angegeben.
Viewmol.noDisplay:                               Kein Anzeigeprogramm f�r Bilder in den Resourcen angegeben.
Viewmol.unableToWriteFeedback:                   Kann nicht gen�gend Speicher allokieren, um Zeichnung abzuspeichern.
Viewmol.wavenumberTitle:                         %s Schwingung, %.6g cm-1
Viewmol.selectAtomTitle:                         Bitte ein Atom ausw�hlen in dem Sie darauf klicken.
Viewmol.selectINSTitle:                          Bitte ein Atom anklicken, um das INS-Gewicht auf %f zu setzen.
Viewmol.basisfunctionTitle:                      Basisfunktion %d: %s%d %s
Viewmol.basisfunctionInMOTitle:                  Basisfunktion %d in MO %d: %s%d %.7f*%s
Viewmol.molecularOrbitalTitle:                   Molek�lorbital %d: %s, Energie %f Hartree
Viewmol.electronDensityTitle:                    Elektronendichte
Viewmol.isosurfaceLabel:                         Isofl�che:
Viewmol.isosurfaceLevel:                         %.3f
Viewmol.historyTitle:                            Iteration %d Energie %18.10f Hartree, |dE/dxyz| %10.6f
Viewmol.wavenumber:                              Wellenzahl (cm**-1)
Viewmol.intensity:                               Intensit�t (%)
Viewmol.energy:                                  Energie
Viewmol.gradientNorm:                            Gradientennorm
Viewmol.animateSave:                             Sie k�nnen kein animiertes Molek�l zeichnen.
Viewmol.noNormalModes:                           Normalschwingungen wurden nicht eingelesen.
Viewmol.GaussianProblem:                         Der Gaussian-Output enth�lt %c-Funktionen.\nDiese Kombination wird zur Zeit nicht unterst�tzt.
Viewmol.unknownResource:                         Der Wert %s ist f�r die Resource\n%s nicht erlaubt.
Viewmol.unsupportedVersion:                      Dateien der Version %s werden nicht unterst�tzt.
Viewmol.wrongFiletype:                           Die Datei %s hat den falschen Dateityp.
Viewmol.wrongReference:                          Die Datei, auf die in %s als 'type=car'\nverwiesen wird, hat den falschen Dateityp.
Viewmol.onlySymmetricBasis:                      Die Eingabedatei enth�lt nur Basisfunktionen f�r nicht-symmetrie�quivalente\\Atome. Es wird angenommen, da� der Basissatz der gleiche f�r alle Atome des\\gleichen Elements ist.
Viewmol.unknownErrorMessage:                     Der Input-Filter hat eine unbekannte Fehlermeldung\ngeschickt: %s.
Viewmol.noCoordinates:                           Kann keine Koordinaten in der Datei %s finden.
Viewmol.noEnergy:                                Kann keine Energie in der Datei %s finden.
Viewmol.noMOselected:                            Es ist kein MO f�r %s ausgew�hlt. Bitte\nw�hlen Sie ein MO im Energieniveauschema bevor sie diesen\nPunkt erneut anklicken.
Viewmol.notChangeable:                           Diese Koordinate kann nicht ge�ndert werden,\nda sie Bestandteil eines Ringes ist.
Viewmol.notDefined:                              Diese innere Koordinate ist nicht definiert.
Viewmol.formatNotRecognized:                     Kann das Format der Datei %s nicht erkennen.
Viewmol.wrongBrowser:                            Der Web-Browser kann an der angegebenen Stelle nicht gefunden werden.
Viewmol.wrongMoloch:                             Moloch kann an der angegebenen Stelle nicht gefunden werden.
Viewmol.differentMolecules:                      Innere Koordinaten zwischen zwei verschiedenen Molek�len k�nnen nicht gemessen werden.
Viewmol.BusError:                                Ein Bus-Fehler ist aufgetreten. Viewmol\nkann nicht weiterlaufen.
Viewmol.FloatingPointException:                  Ein Gleitkomma-Rechenfehler ist aufgetreten.\nViewmol kann nicht weiterlaufen.
Viewmol.SegmentationViolation:                   Der Speicherschutz wurde verletzt.\nViewmol kann nicht weiterlaufen.
Viewmol.deleteAll:                               Wollen Sie wirklich alle Molek�le\n l�schen ?
Viewmol.deleteOne:                               Wollen Sie wirklich %s\nl�schen ?
Viewmol.unknownElement:                          Das Element %s ist unbekannt.
Viewmol.elementMenuPrefix:
Viewmol.wrongPNGversion:                         Die Version %s der PNG-Bibliothek wird nicht unterst�tzt. Die Version 1.4 wird mindestens ben�tigt.
